package com.three.config;

import com.three.config.common.IConfig;
import com.three.db.DemoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.ws.Action;

/**
 * @Author mathua
 * @Date 2017/5/24 17:52
 */
@SpringBootApplication
public class ConfigTest {
    public static void main(String[] args) {
        SpringApplication.run(com.three.CommonMain.class, args);
        System.out.println(IConfig.chess.log_conf_path);
    }
}
