package com.three.config.game;

/**
 * @Author mathua
 * @Date 2017/5/24 17:51
 */
import com.three.exception.GameException;
import com.three.utils.ConfigUtils;

/**
 * @Author mathua
 * @Date 2017/5/19 17:56
 */
public class ConfigTestExe {

    public static void main(String[] args) {
        ConfigUtils.loadAllConfig("config", "com.three.config.game");
    }
}
