package com.three.config.game;

import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import com.three.utils.JsonUtils;

import java.util.List;
import java.util.Map;

/**
 * @Author mathua
 * @Date 2017/5/18 20:32
 */
public class TestConfig {
    private long longValue;
    private int intValue;
    private short shortValue;
    private byte byteValue;
    private float floatValue;
    private double doubleValue;
    private String stringValue;
    private boolean booleanValue;
    private List<Double> doubleList;
    private List<TmpConfig> tmpConfigList;

    public static void main(String[] args) {
        ConfigUtils.loadAllConfig("config", "com.three.gameconfig");
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        Map<Long, TestConfig> configs = ConfigUtils.getConfigs(TestConfig.class);
        if (configs == null)
            return false;
        for (TestConfig config : configs.values()) {
            System.out.println(JsonUtils.toJson(config));
        }
        return true;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        System.out.println("配置表检查!");
        return true;
    }

    public long getLongValue() {
        return longValue;
    }

    public int getIntValue() {
        return intValue;
    }

    public short getShortValue() {
        return shortValue;
    }

    public byte getByteValue() {
        return byteValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public boolean isBooleanValue() {
        return booleanValue;
    }

    public List<Double> getDoubleList() {
        return doubleList;
    }

    public List<TmpConfig> getTmpConfigList() {
        return tmpConfigList;
    }

    public void setLongValue(long longValue) {
        this.longValue = longValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public void setShortValue(short shortValue) {
        this.shortValue = shortValue;
    }

    public void setByteValue(byte byteValue) {
        this.byteValue = byteValue;
    }

    public void setFloatValue(float floatValue) {
        this.floatValue = floatValue;
    }

    public void setDoubleValue(double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public void setBooleanValue(boolean booleanValue) {
        this.booleanValue = booleanValue;
    }

    public void setDoubleList(List<Double> doubleList) {
        this.doubleList = doubleList;
    }

    public void setTmpConfigList(List<TmpConfig> tmpConfigList) {
        this.tmpConfigList = tmpConfigList;
    }
}
