package com.three.config.game;

/**
 * @Author mathua
 * @Date 2017/5/18 20:33
 */
public class TmpConfig {
    private int tmpInt;
    private String tmpString;
    private boolean tmpBoolean;

    public int getTmpInt() {
        return tmpInt;
    }

    public String getTmpString() {
        return tmpString;
    }

    public boolean isTmpBoolean() {
        return tmpBoolean;
    }
}
