package com.three.db;

import com.three.db.annotation.Sharding;
import com.three.db.annotation.Shardkey;
import com.three.db.shard.base.ShardingType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ziqingwang on 2017/6/1 0001.
 */
@Entity
@Table(name = "t_demo")
@Sharding(
        db_name = "demo",
        shard_db_num = 2,
        shard_db_type = ShardingType.MOLD,
        shard_tb_num = 2,
        shard_tb_type = ShardingType.MOLD)
public class DemoPO {
    @Id
    @Shardkey
    private long id;

    @Column(name = "des")
    private String des;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
