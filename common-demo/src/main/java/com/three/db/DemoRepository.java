package com.three.db;

import com.three.db.annotation.DemoDbRepo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@DemoDbRepo
@Component
public interface DemoRepository extends JpaRepository<DemoPO,Long> {

}
