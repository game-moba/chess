package com.three.db;

import com.three.CommonMain;
import com.three.db.config.DataSourceConfig;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Created by ziqingwang on 2017/6/1 0001.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CommonMain.class)
public class ShardingDbTest {
    @Autowired
    private DemoRepository demoRepository;
    @Autowired
    private TimeRepository timeRepository;
    @Autowired
    private DataSourceConfig dataSourceConfig;
    @Test
    public void test() throws Exception {
        DemoPO demoPO=new DemoPO();
        demoPO.setId(3L);
        demoPO.setDes("预计分配（db_1——t_demo_1）");
        demoRepository.save(demoPO);
        Assert.assertTrue(demoRepository.getOne(3L).getDes().equals("预计分配（db_1——t_demo_1）"));
    }
    @Test
    public void timeShardingTest(){
        TimePO timePO=new TimePO();
        timePO.setId(1);
        timePO.setTime(new Date());
        //预计分配 demo_db_1——t_time_201706
        timeRepository.save(timePO);
    }

}
