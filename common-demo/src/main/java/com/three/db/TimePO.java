package com.three.db;

import com.three.db.annotation.Sharding;
import com.three.db.annotation.Shardkey;
import com.three.db.shard.base.ShardingType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * 示例
 * Created by ziqingwang on 2017/6/1 0001.
 */
@Entity
@Table(name = "t_time")
@Sharding(
        db_name = "demo",
        shard_db_num = 2,
        shard_db_type = ShardingType.MOLD,
        shard_tb_type = ShardingType.TIME)
public class TimePO {
    @Id
    @Shardkey(type = Shardkey.TYPE.DB)//ID 取模分库
    private int id;

    @Shardkey(type = Shardkey.TYPE.TB)//time 按月分表
    @Column(name = "time")
    private Date time;
    @Column(name = "des")
    private String des;

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
