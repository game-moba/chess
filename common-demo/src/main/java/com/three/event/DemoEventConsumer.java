package com.three.event;

import com.google.common.eventbus.Subscribe;
import org.springframework.stereotype.Component;

/**
 * Created by mathua on 2017/5/28.
 */
@Component
public final class DemoEventConsumer extends EventConsumer {
    @Subscribe
    void on(OneDemoEvent event) {
        System.out.println("event post:-----------------------------------------------------------" + event.eventName);
    }
}
