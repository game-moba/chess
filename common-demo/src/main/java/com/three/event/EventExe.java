package com.three.event;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by mathua on 2017/5/28.
 */
@SpringBootApplication
public class EventExe {

    public static void main(String[] args) {
        SpringApplication.run(com.three.CommonMain.class, args);
        EventBus.I.post(new OneDemoEvent("post a demo event"));
    }
}
