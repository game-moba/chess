package com.three.event;

import com.three.api.event.Event;

/**
 * Created by mathua on 2017/5/28.
 */
public final class OneDemoEvent implements Event {

    public final String eventName;

    public OneDemoEvent(String eventName) {
        this.eventName = eventName;
    }
}
