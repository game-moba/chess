package com.three.lock;

import com.three.base.AbstractEntity;

/**
 * @Author mathua
 * @Date 2017/5/18 17:08
 */
public class Entity extends AbstractEntity {
    private final long id;
    private int count;

    public Entity(long id) {
        this.id = id;
    }

    @Override
    public long getEntityId() {
        return id;
    }

    public void doTask() {
        ++count;
    }

    public int getCount() {
        return count;
    }

    public void print() {
        System.out.println(id + "-" + count);
    }
}
