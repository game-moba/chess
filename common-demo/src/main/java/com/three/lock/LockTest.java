package com.three.lock;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/18 16:08
 */
public class LockTest {

    public static void main(String[] args) {
        List<Entity> list = new ArrayList<>();
        list.add(new Entity(1));
        list.add(new Entity(2));
        list.add(new Entity(3));

        List<Entity> list2 = new ArrayList<>();
        list2.add(new Entity(4));
        list2.add(new Entity(5));
        list2.add(new Entity(6));

        LockThread tl1 = new LockThread(list2);
        LockThread tl2 = new LockThread(list2);
        LockThread tl3 = new LockThread(list2);

        MyThread t1 = new MyThread(list);
        MyThread t2 = new MyThread(list);
        MyThread t3 = new MyThread(list);

        t1.start();
        t2.start();
        t3.start();

        tl1.start();
        tl2.start();
        tl3.start();

        try {
            Thread.sleep(1000 * 10);
            for (Entity entity : list) {
                entity.print();
            }

            for (Entity entity : list2) {
                entity.print();
            }

            System.out.println("\n\n");
            Thread.sleep(1000 * 10);
            for (Entity entity : list) {
                entity.print();
            }

            for (Entity entity : list2) {
                entity.print();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
