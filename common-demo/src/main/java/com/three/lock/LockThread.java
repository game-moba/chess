package com.three.lock;

import com.three.utils.LockUtils;

import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/19 19:13
 */
public class LockThread extends Thread {

    List<Entity> list;

    public LockThread(List<Entity> list) {
        this.list = list;
    }

    @Override
    public void run() {
        ChainLock chainLock = LockUtils.getLock(list);
        chainLock.lock();
        try {
            int count = 0;
            for (Entity entity : list) {
                for (int i = 0; i < 10000 * 3000; ++i) {
                    entity.doTask();
                    ++count;
                }
            }
            System.out.println("结束:" + count);
        } finally {
            chainLock.unlock();
        }
    }
}