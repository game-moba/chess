package com.three.lock;

import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/19 19:13
 */
public class MyThread extends Thread {

    List<Entity> list;

    public MyThread(List<Entity> list) {
        this.list = list;
    }

    @Override
    public void run() {
        int count = 0;
        for (Entity entity : list) {
            for (int i = 0; i < 10000 * 3000; ++i) {
                entity.doTask();
                ++count;
            }
        }
        System.out.println("结束:" + count);
    }
}
