package com.three.redis;

import com.three.cache.redis.manager.RedisManager;
import com.three.cache.redis.mq.ListenerDispatcher;
import com.three.server.ServerTestMain;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.locks.LockSupport;

/**
 * Created by mathua on 2017/5/30.
 */
public class PubSubTest {

    @Before
    public void init() {
        ServerTestMain.start();
    }

    @Test
    public void subpubTest() {
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/123");
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/124");
        RedisManager.I.publish("/hello/123", "123");
        RedisManager.I.publish("/hello/124", "124");
    }

    @Test
    public void pubsubTest() {
        RedisManager.I.publish("/hello/123", "123");
        RedisManager.I.publish("/hello/124", "124");
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/123");
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/124");
    }

    @Test
    public void pubTest() {
        RedisManager.I.publish("/hello/123", "123");
        RedisManager.I.publish("/hello/124", "124");
    }

    @Test
    public void subTest() {
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/123");
        RedisManager.I.subscribe(ListenerDispatcher.I().getSubscriber(), "/hello/124");
        LockSupport.park();
    }

}
