package com.three.redis;

/**
 * Created by mathua on 2017/5/30.
 */
import com.google.common.collect.Lists;
//import com.three.cache.redis.client.RedisClient;
import com.three.cache.redis.RedisServer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RedisUtilTest {

    RedisServer node = new RedisServer("127.0.0.1", 6379);
    List<RedisServer> nodeList = Lists.newArrayList(node);

//    @Test
//    public void testAddAndGetAndDelete() {
//        Jedis jedis = RedisClient.getClient(node);
//        jedis.set("hi", "huang");
//
//        String ret = jedis.get("hi");
//        System.out.println(ret);
//
//        jedis.del("hi");
//        ret = jedis.get("hi");
//        if (ret == null) {
//            System.out.println("ret is null");
//        } else {
//            System.out.println("ret is not null:" + ret);
//        }
//
//    }
//
//    @Test
//    public void testJedisPool() {
//        // 最大连接数是8，因此，获取10个链接会抛错误
//        List<Jedis> jedisList = Lists.newArrayList();
//        for (int i = 0; i < 10; i++) {
//            Jedis jedis = RedisClient.getClient(node);
//            jedisList.add(jedis);
//        }
//    }
//
//    @Test
//    public void testJedisPool2() {
//        // 最大连接数是8，因此，获取10个链接会抛错误
//        List<Jedis> jedisList = Lists.newArrayList();
//        for (int i = 1; i <= 8; i++) {
//            Jedis jedis = RedisClient.getClient(node);
//            jedisList.add(jedis);
//        }
//
//        System.out.println(jedisList.size());
//
//        try {
//            Jedis jedis = RedisClient.getClient(node);
//            jedisList.add(jedis);
//            System.out.println("first get jedis success");
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//
//        // 关闭一个链接
//        RedisClient.close(jedisList.get(0));
//
//        try {
//            Jedis jedis = RedisClient.getClient(node);
//            jedisList.add(jedis);
//            System.out.println("second get jedis success");
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//
//        System.out.println(jedisList.size());
//    }
//
//    @Test
//    public void testKV() {
//        Player player = new Player("huang", 18, new Date());
//        RedisClient.set(nodeList, "test", player);
//
//        Player nowPlayer = RedisClient.get(node, "test", Player.class);
//        System.out.println("node1:" + ToStringBuilder.reflectionToString(nowPlayer));
//
//        nowPlayer = RedisClient.get(node, "test", Player.class);
//        System.out.println("node2:" + ToStringBuilder.reflectionToString(nowPlayer));
//
//        RedisClient.del(nodeList, "test");
//
//        nowPlayer = RedisClient.get(node, "test", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node2 nowPlayer is null");
//        } else {
//            System.out.println("node2:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//        nowPlayer = RedisClient.get(node, "test", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node nowPlayer is null");
//        } else {
//            System.out.println("node:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//        RedisClient.set(nodeList, "test", player, 10);
//
//        try {
//            Thread.sleep(12000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        nowPlayer = RedisClient.get(node, "test", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node2 nowPlayer is null");
//        } else {
//            System.out.println("node2:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//        nowPlayer = RedisClient.get(node, "test", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node nowPlayer is null");
//        } else {
//            System.out.println("node:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//    }
//
//    @Test
//    public void hashTest() {
//
//        Player player = new Player("huang", 18, new Date());
//
//        RedisClient.hset(nodeList, "hashhuang", "hi", player);
//
//        Player nowPlayer = RedisClient.hget(node, "hashhuang", "hi", Player.class);
//        System.out.println("node1:" + ToStringBuilder.reflectionToString(nowPlayer));
//
//        nowPlayer = RedisClient.hget(node, "hashhuang", "hi", Player.class);
//        System.out.println("node2:" + ToStringBuilder.reflectionToString(nowPlayer));
//
//        Map<String, Player> ret = RedisClient.hgetAll(node, "hashhuang", Player.class);
//        Iterator<Map.Entry<String, Player>> iterator = ret.entrySet().iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Player> entry = iterator.next();
//            String key = entry.getKey();
//            Player val = entry.getValue();
//            System.out.println("all:" + key + "," + ToStringBuilder.reflectionToString(val));
//        }
//
//        RedisClient.hdel(nodeList, "hashhuang", "hi");
//
//        nowPlayer = RedisClient.hget(node, "hashhuang", "hi", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node2 nowPlayer is null");
//        } else {
//            System.out.println("node2:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//        nowPlayer = RedisClient.hget(node, "hashhuang", "hi", Player.class);
//        if (nowPlayer == null) {
//            System.out.println("node nowPlayer is null");
//        } else {
//            System.out.println("node:" + ToStringBuilder.reflectionToString(nowPlayer));
//        }
//
//    }
//
//    @Test
//    public void testSet() {
//		System.out.println(RedisClient.sCard(node, RedisKey.getOnlinePlayerListKey()));
//
//		List<String> onlinePlayerIdList = RedisClient.sScan(node, RedisKey.getOnlinePlayerListKey(), String.class, 0);
//		System.out.println(onlinePlayerIdList.size());
//
//    }
//
//    @Test
//    public void testlist() {
//		RedisClient.del(nodeList, RedisKey.getPlayerOfflineKey());
//    }
//
//    @Test
//    public void testsortedset() {
//		RedisClient.zAdd(nodeList, RedisKey.getOnlinePlayerListKey(), "doctor1test");
//
//		long len =RedisClient.zCard(node, RedisKey.getOnlinePlayerListKey());
//		System.out.println(len);
//    }

}
