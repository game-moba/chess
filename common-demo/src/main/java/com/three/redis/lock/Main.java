package com.three.redis.lock;

import com.three.cache.redis.lock.RedisLock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mathua on 2017/7/16.
 */
public class Main {

    public static void main(String[] args) {
        String key = "redis:dist:lock";
        final MesSkill mesSkill = MesSkill.build(20);
        final RedisLock redisLock = new RedisLock(key,2000,2000);

        int threads = 10;

        ExecutorService pool = Executors.newFixedThreadPool(threads);
        for (; ; ) {
            pool.submit(new Runnable() {
                public void run() {
                    try {
                        if (redisLock.lock()) {
                            mesSkill.skill();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        redisLock.unlock();
                    }

                }
            });
        }
    }
}