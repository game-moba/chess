package com.three.redis.lock;

/**
 * Created by mathua on 2017/7/16.
 */
public class MesSkill {

    private long total = 50;

    public static MesSkill build(long total) {
        MesSkill mesSkill = new MesSkill();
        mesSkill.total = total;
        return mesSkill;
    }

    public void skill() throws Exception {
        if (total <= 0) {
            throw new Exception("秒杀完成,未抢到商品 ...");
        }
        total -= 1;
        System.out.println(Thread.currentThread().getName() + ",秒到商品,剩余:" + total);
    }
}
