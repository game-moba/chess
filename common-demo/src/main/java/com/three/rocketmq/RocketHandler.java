package com.three.rocketmq;

import com.three.message.hall.CreateFriendRoomReqMessage;
import com.three.common.receiver.ReceiverType;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import org.springframework.stereotype.Service;

/**
 * Created by Mathua on 2017/6/28.
 */
@Service
@PacketHandlerClass
public class RocketHandler {

    /**
     * 跨服协议接收者:ReceiverType.ROCKET_MQ_RECEIVER
     * @param message
     * @throws Exception
     */
    @PacketHandlerMethod(msgId = CommandEnum.Command.CREATE_FRIEND_ROOM, describe = "创建房间", receivers = {ReceiverType.ROCKET_MQ_RECEIVER})
    public void createRoom(CreateFriendRoomReqMessage message) throws Exception {
        System.out.println("*************************************创建房间***************************************************");
        System.out.println(message);
    }

}
