package com.three.rocketmq;

import com.three.api.protocol.Packet;
import com.three.netty.rocketmq.MQProducerService;
import com.three.protocol.CommandEnum;
import com.three.protocol.HallModule;
import org.junit.Test;

/**
 * Created by Mathua on 2017/6/28.
 */
public class RocketTest {

    @Test
    public void rocketMQTest() {
        long playerId = 1000;
        HallModule.CreateFriendRoomReq_1000.Builder builder = HallModule.CreateFriendRoomReq_1000.newBuilder().setGameType(11).setPayType(22).setMaxRounds(33);
        MQProducerService.I.sendToPlayer(/*player.getPlayerId()*/playerId, Packet.build(CommandEnum.Command.CREATE_FRIEND_ROOM, builder.build().toByteArray()));
    }
}
