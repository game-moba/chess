package com.three.server;

import org.junit.Test;
import com.three.netty.bootstrap.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.locks.LockSupport;

/**
 * Created by mathua on 2017/5/30.
 */
@SpringBootApplication
public class ServerTestMain {

    public static void main(String[] args) {
        SpringApplication.run(ServerTestMain.class, args);
        start();
    }

    @Test
    public void testServer() {
        start();
        LockSupport.park();
    }

    public static void start() {
        System.setProperty("io.netty.leakDetection.level", "PARANOID");
        System.setProperty("io.netty.noKeySetOptimization", "false");
        Main.main(null);
    }

}
