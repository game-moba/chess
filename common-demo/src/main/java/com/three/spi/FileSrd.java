package com.three.spi;

import com.google.common.collect.Lists;
import com.three.api.service.BaseService;
import com.three.api.srd.*;
import com.three.utils.JsonUtils;

import java.util.List;

/**
 * Created by mathua on 2017/5/30.
 */
public final class FileSrd extends BaseService implements ServiceRegistry, ServiceDiscovery {

    public static final FileSrd I = new FileSrd();

    @Override
    public void init() {

    }

    @Override
    public void register(ServiceNode node) {
        FileCacheManger.I.hset(node.serviceName(), node.nodeId(), JsonUtils.toJson(node));
    }

    @Override
    public void deregister(ServiceNode node) {
        FileCacheManger.I.hdel(node.serviceName(), node.nodeId());
    }

    @Override
    public List<ServiceNode> lookup(String path) {
        FileCacheManger.I.init();
        return Lists.newArrayList(FileCacheManger.I.hgetAll(path, CommonServiceNode.class).values());
    }

    @Override
    public void subscribe(String path, ServiceListener listener) {

    }

    @Override
    public void unsubscribe(String path, ServiceListener listener) {

    }
}
