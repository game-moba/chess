package com.three.spi;

import com.three.api.spi.Spi;
import com.three.api.spi.common.ServiceDiscoveryFactory;
import com.three.api.srd.ServiceDiscovery;

/**
 * Created by mathua on 2017/6/3.
 */
@Spi(order = 2)
public final class SimpleDiscoveryFactory implements ServiceDiscoveryFactory {
    @Override
    public ServiceDiscovery get() {
        return FileSrd.I;
    }
}
