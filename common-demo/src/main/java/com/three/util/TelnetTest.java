package com.three.util;

import com.three.tools.Utils;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by mathua on 2017/5/30.
 */
public class TelnetTest {

    @Test
    public void test() {
        boolean ret = Utils.checkHealth("120.27.196.68", 82);
        System.out.println(ret);
    }

    @Test
    public void test2() {
        boolean ret = Utils.checkHealth("120.27.196.68", 80);
        System.out.println(ret);
    }

    @Test
    public void uriTest() throws URISyntaxException {
        String url = "http://127.0.0.1";
        URI uri = new URI(url);
        System.out.println(uri.getPort());
    }

}
