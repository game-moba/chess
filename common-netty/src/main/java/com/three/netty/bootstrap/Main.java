package com.three.netty.bootstrap;

import com.three.utils.LogUtils;

/**
 * Created by mathua on 2017/5/25.
 */
public class Main {

    /**
     * 源码启动请不要直接运行此方法，负载不能正确加载配置文件
     *
     * @param args 启动参数
     */
    public static void main(String[] args) {
        LogUtils.init();
        LogUtils.Console.info("launch chess server...");
        ServerLauncher launcher = new ServerLauncher();
        launcher.start();
        addHook(launcher);
    }

    /**
     * 注意点
     * 1.不要ShutdownHook Thread 里调用System.exit()方法，否则会造成死循环。
     * 2.如果有非守护线程，只有所有的非守护线程都结束了才会执行hook
     * 3.Thread默认都是非守护线程，创建的时候要注意
     * 4.注意线程抛出的异常，如果没有被捕获都会跑到Thread.dispatchUncaughtException
     *
     * @param launcher
     */
    private static void addHook(ServerLauncher launcher) {
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {

                    try {
                        launcher.stop();
                    } catch (Exception e) {
                        LogUtils.Console.error("chess server stop ex", e);
                    }
                    LogUtils.Console.info("jvm exit, all service stopped.");

                }, "chess-shutdown-hook-thread")
        );
    }
}

