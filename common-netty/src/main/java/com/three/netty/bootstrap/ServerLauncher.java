package com.three.netty.bootstrap;

import com.three.api.push.PushSender;
import com.three.netty.bootstrap.job.*;
import com.three.netty.core.server.*;

import static com.three.common.ServerNodes.CS;
import static com.three.common.ServerNodes.GS;
import static com.three.common.ServerNodes.WS;
import static com.three.config.common.IConfig.chess.net.tcpGateway;
import static com.three.config.common.IConfig.chess.net.udpGateway;
import static com.three.config.common.IConfig.chess.net.wsEnabled;

/**
 * Created by mathua on 2017/5/25.
 */
public final class ServerLauncher {

    private final BootChain chain = BootChain.chain();

    public ServerLauncher() {
        chain.boot()
                .setNext(new CacheManagerBoot())//1.初始化缓存模块
                .setNext(new ServiceRegistryBoot())//2.启动服务注册与发现模块
                .setNext(new ServerBoot(ConnectionServer.I(), CS))//3.启动接入服务
                .setNext(() -> new ServerBoot(WebSocketServer.I(), WS), wsEnabled())//4.启动websocket接入服务
                .setNext(() -> new ServerBoot(GatewayUDPConnector.I(), GS), udpGateway())//5.启动udp网关服务
                .setNext(() -> new ServerBoot(GatewayServer.I(), GS), tcpGateway())//6.启动tcp网关服务
                .setNext(new ServerBoot(AdminServer.I(), null))//7.启动控制台服务
                .setNext(new PushCenterBoot())//8.启动推送中心组件
                .setNext(new HttpProxyBoot())//9.启动http代理服务，dns解析服务
                .setNext(new MonitorBoot())//10.启动监控服务
                .setNext(new MQProducerBoot())// 11.启动MQProducer服务
                .setNext(new MQPushConsumerBoot())//12.启动MQPushConsumer服务
                .end();
        // 广播发送者的初始化
        PushSender sender = PushSender.get();
        sender.start().join();
    }

    public void start() {
        chain.start();
    }

    public void stop() {
        chain.stop();
    }
}
