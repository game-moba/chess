package com.three.netty.bootstrap.job;

import com.three.api.event.ServerShutdownEvent;
import com.three.api.event.ServerStartupEvent;
import com.three.api.spi.core.ServerEventListenerFactory;
import com.three.event.EventBus;
import com.three.utils.LogUtils;

import java.util.function.Supplier;

/**
 * Created by mathua on 2017/5/23.
 */
public final class BootChain {
    private final BootJob boot = new BootJob() {
        {
            ServerEventListenerFactory.create();// 初始化服务监听
        }

        @Override
        protected void start() {
            LogUtils.Console.info("bootstrap chain starting...");
            startNext();
        }

        @Override
        protected void stop() {
            stopNext();
            LogUtils.Console.info("bootstrap chain stopped.");
            LogUtils.Console.info("===================================================================");
            LogUtils.Console.info("====================CHESS SERVER STOPPED SUCCESS===================");
            LogUtils.Console.info("===================================================================");
        }
    };

    private BootJob last = boot;

    public void start() {
        boot.start();
    }

    public void stop() {
        boot.stop();
    }

    public static BootChain chain() {
        return new BootChain();
    }

    public BootChain boot() {
        return this;
    }

    public void end() {
        setNext(new BootJob() {
            @Override
            protected void start() {
                EventBus.I.post(new ServerStartupEvent());
                LogUtils.Console.info("bootstrap chain started.");
                LogUtils.Console.info("===================================================================");
                LogUtils.Console.info("====================CHESS SERVER START SUCCESS=====================");
                LogUtils.Console.info("====================       JUST DO IT!        =====================");
                LogUtils.Console.info("===================================================================");
            }

            @Override
            protected void stop() {
                LogUtils.Console.info("bootstrap chain stopping...");
                EventBus.I.post(new ServerShutdownEvent());
            }

            @Override
            protected String getName() {
                return "LastBoot";
            }
        });
    }

    public BootChain setNext(BootJob bootJob) {
        this.last = last.setNext(bootJob);
        return this;
    }

    public BootChain setNext(Supplier<BootJob> next, boolean enabled) {
        if (enabled) {
            return setNext(next.get());
        }
        return this;
    }
}
