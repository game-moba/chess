package com.three.netty.bootstrap.job;

import com.three.utils.LogUtils;

import java.util.function.Supplier;

/**
 * 启动作业
 *
 * @Author mathua
 * @Date 2017/5/23 19:54
 */
public abstract class BootJob {
    protected BootJob next;

    protected abstract void start();

    protected abstract void stop();

    public void startNext() {
        if (next != null) {
            LogUtils.Console.info("start bootstrap job [{}]", getNextName());
            next.start();
        }
    }

    public void stopNext() {
        if (next != null) {
            next.stop();
            LogUtils.Console.info("stopped bootstrap job [{}]", getNextName());
        }
    }

    public BootJob setNext(BootJob next) {
        this.next = next;
        return next;
    }

    public BootJob setNext(Supplier<BootJob> next, boolean enabled) {
        if (enabled) {
            return setNext(next.get());
        }
        return this;
    }

    protected String getNextName() {
        return next.getName();
    }

    protected String getName() {
        return this.getClass().getSimpleName();
    }
}