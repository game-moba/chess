package com.three.netty.bootstrap.job;

import com.three.api.spi.common.CacheManagerFactory;
import com.three.common.player.PlayerManager;

/**
 * Created by mathua on 2017/5/25.
 */
public final class CacheManagerBoot extends BootJob {

    @Override
    protected void start() {
        CacheManagerFactory.create().init();
        PlayerManager.I.clearPlayerOnlineData();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        CacheManagerFactory.create().destroy();
        PlayerManager.I.clearPlayerOnlineData();
    }
}

