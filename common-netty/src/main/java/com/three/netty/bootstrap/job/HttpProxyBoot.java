package com.three.netty.bootstrap.job;

import com.three.api.spi.net.DnsMappingManager;
import com.three.config.common.IConfig;
import com.three.netty.http.NettyHttpClient;

/**
 * Created by Mathua on 2017/5/25.
 */
public final class HttpProxyBoot extends BootJob {

    @Override
    protected void start() {
        if (IConfig.chess.http.proxy_enabled) {
            NettyHttpClient.I().syncStart();
            DnsMappingManager.create().start();
        }

        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        if (IConfig.chess.http.proxy_enabled) {
            NettyHttpClient.I().syncStop();
            DnsMappingManager.create().stop();
        }
    }
}
