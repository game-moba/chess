package com.three.netty.bootstrap.job;


import com.three.netty.rocketmq.MQProducerService;

/**
 * Created by Mathua on 2017/6/27.
 */
public final class MQProducerBoot extends BootJob {
    @Override
    protected void start() {
        MQProducerService.I.start();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        MQProducerService.I.stop();
    }
}
