package com.three.netty.bootstrap.job;

import com.three.netty.rocketmq.MQPushConsumerService;

/**
 * Created by Mathua on 2017/6/27.
 */
public final class MQPushConsumerBoot extends BootJob {
    @Override
    protected void start() {
        MQPushConsumerService.I.start();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        MQPushConsumerService.I.stop();
    }
}
