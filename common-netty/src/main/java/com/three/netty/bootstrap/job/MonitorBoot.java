package com.three.netty.bootstrap.job;

import com.three.monitor.server.MonitorService;

/**
 * Created by Mathua on 2017/5/25.
 */
public final class MonitorBoot extends BootJob {
    @Override
    protected void start() {
        MonitorService.I.start();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        MonitorService.I.stop();
    }
}
