package com.three.netty.bootstrap.job;

import com.three.netty.core.push.PushCenter;

/**
 * 推送中心
 * Created by Mathua on 2017/5/25.
 */
public final class PushCenterBoot extends BootJob {
    @Override
    protected void start() {
        PushCenter.I.start();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        PushCenter.I.stop();
    }
}

