package com.three.netty.bootstrap.job;

import com.three.api.service.Listener;
import com.three.api.service.Server;
import com.three.api.spi.common.ServiceRegistryFactory;
import com.three.api.srd.ServiceNode;
import com.three.utils.LogUtils;

/**
 * 服务器启动类
 *
 * @Author mathua
 * @Date 2017/5/23 20:00
 */
public final class ServerBoot extends BootJob {
    private final Server server;
    private final ServiceNode node;

    public ServerBoot(Server server, ServiceNode node) {
        this.server = server;
        this.node = node;
    }

    @Override
    public void start() {
        server.init();
        server.start(new Listener() {
            @Override
            public void onSuccess(Object... args) {
                LogUtils.Console.info("start {} success on:{}", server.getClass().getSimpleName(), args[0]);
                if (node != null) {//注册应用到zk
                    ServiceRegistryFactory.create().register(node);
                    LogUtils.RSD.info("register {} to srd success.", node);
                }
                startNext();
            }

            @Override
            public void onFailure(Throwable cause) {
                LogUtils.Console.error("start {} failure, jvm exit with code -1", server.getClass().getSimpleName(), cause);
                System.exit(-1);
            }
        });
    }

    @Override
    protected void stop() {
        stopNext();
        if (node != null) {
            ServiceRegistryFactory.create().deregister(node);
        }
        LogUtils.Console.info("try shutdown {}...", server.getClass().getSimpleName());
        server.stop().join();
        LogUtils.Console.info("{} shutdown success.", server.getClass().getSimpleName());
    }

    @Override
    protected String getName() {
        return super.getName() + '(' + server.getClass().getSimpleName() + ')';
    }
}
