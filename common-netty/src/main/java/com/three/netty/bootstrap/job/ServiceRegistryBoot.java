package com.three.netty.bootstrap.job;

import com.three.api.spi.common.ServiceRegistryFactory;
import com.three.utils.LogUtils;

/**
 * 服务注册
 *
 * @Author mathua
 * @Date 2017/5/23 19:56
 */
public final class ServiceRegistryBoot extends BootJob {

    @Override
    protected void start() {
        LogUtils.Console.info("init service registry waiting for connected...");
        ServiceRegistryFactory.create().syncStart();
        startNext();
    }

    @Override
    protected void stop() {
        stopNext();
        ServiceRegistryFactory.create().syncStop();
        LogUtils.Console.info("service registry closed...");
    }
}
