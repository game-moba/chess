package com.three.netty.client.connect;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mathua on 2017/5/30.
 */
public final class TestStatistics {
    public AtomicInteger clientNum = new AtomicInteger();
    public AtomicInteger connectedNum = new AtomicInteger();
    public AtomicInteger bindPlayerNum = new AtomicInteger();
    public AtomicInteger receivePushNum = new AtomicInteger();

    @Override
    public String toString() {
        return "TestStatistics{" +
                "clientNum=" + clientNum +
                ", connectedNum=" + connectedNum +
                ", bindPlayerNum=" + bindPlayerNum +
                ", receivePushNum=" + receivePushNum +
                '}';
    }
}
