package com.three.netty.client.gateway;

import com.three.api.connection.ConnectionManager;
import com.three.api.service.Listener;
import com.three.netty.client.gateway.handler.GatewayClientChannelHandler;
import com.three.config.common.IConfig;
import com.three.netty.client.NettyTCPClient;
import com.three.netty.connection.NettyConnectionManager;
import com.three.common.receiver.ReceiverType;
import com.three.tools.thread.NamedPoolThreadFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.sctp.nio.NioSctpChannel;
import io.netty.channel.udt.nio.NioUdtProvider;
import io.netty.handler.traffic.GlobalChannelTrafficShapingHandler;

import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.three.config.common.IConfig.chess.net.traffic_shaping.gateway_client.*;
import static com.three.tools.thread.ThreadNames.T_TRAFFIC_SHAPING;

/**
 * Created by mathua on 2017/5/30.
 */
public class GatewayClient extends NettyTCPClient {
    private final GatewayClientChannelHandler handler;
    private GlobalChannelTrafficShapingHandler trafficShapingHandler;
    private ScheduledExecutorService trafficShapingExecutor;
    private final ConnectionManager connectionManager = new NettyConnectionManager();

    public GatewayClient() {
        handler = new GatewayClientChannelHandler(connectionManager, ReceiverType.GATEWAY_CLIENT_RECEIVER.getReceiver());
        if (enabled) {
            trafficShapingExecutor = Executors.newSingleThreadScheduledExecutor(new NamedPoolThreadFactory(T_TRAFFIC_SHAPING));
            trafficShapingHandler = new GlobalChannelTrafficShapingHandler(
                    trafficShapingExecutor,
                    write_global_limit, read_global_limit,
                    write_channel_limit, read_channel_limit,
                    check_interval);
        }
    }

    @Override
    public ChannelHandler getChannelHandler() {
        return handler;
    }

    @Override
    protected void initPipeline(ChannelPipeline pipeline) {
        super.initPipeline(pipeline);
        if (trafficShapingHandler != null) {
            pipeline.addLast(trafficShapingHandler);
        }
    }

    @Override
    protected void doStop(Listener listener) throws Throwable {
        if (trafficShapingHandler != null) {
            trafficShapingHandler.release();
            trafficShapingExecutor.shutdown();
        }
        super.doStop(listener);
    }

    @Override
    protected void initOptions(Bootstrap b) {
        super.initOptions(b);
        if (IConfig.chess.net.snd_buf.gateway_client > 0) b.option(ChannelOption.SO_SNDBUF, IConfig.chess.net.snd_buf.gateway_client);
        if (IConfig.chess.net.rcv_buf.gateway_client > 0) b.option(ChannelOption.SO_RCVBUF, IConfig.chess.net.rcv_buf.gateway_client);
    }

    @Override
    public ChannelFactory<? extends Channel> getChannelFactory() {
        if (IConfig.chess.net.tcpGateway()) return super.getChannelFactory();
        if (IConfig.chess.net.udtGateway()) return NioUdtProvider.BYTE_CONNECTOR;
        if (IConfig.chess.net.sctpGateway()) return NioSctpChannel::new;
        return super.getChannelFactory();
    }

    @Override
    public SelectorProvider getSelectorProvider() {
        if (IConfig.chess.net.tcpGateway()) return super.getSelectorProvider();
        if (IConfig.chess.net.udtGateway()) return NioUdtProvider.BYTE_PROVIDER;
        if (IConfig.chess.net.sctpGateway()) return super.getSelectorProvider();
        return super.getSelectorProvider();
    }

    @Override
    protected int getWorkThreadNum() {
        return IConfig.chess.thread.pool.gateway_client_work;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }
}
