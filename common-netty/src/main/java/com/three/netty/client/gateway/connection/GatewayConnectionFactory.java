package com.three.netty.client.gateway.connection;

import com.three.api.connection.Connection;
import com.three.api.service.BaseService;
import com.three.api.srd.ServiceListener;
import com.three.common.message.base.BaseMessage;
import com.three.config.common.IConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by mathua on 2017/5/30.
 */
public abstract class GatewayConnectionFactory extends BaseService implements ServiceListener {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static GatewayConnectionFactory create() {
        return IConfig.chess.net.udpGateway() ? new GatewayUDPConnectionFactory() : new GatewayTCPConnectionFactory();
    }

    abstract public Connection getConnection(String hostAndPort);

    abstract public <M extends BaseMessage> boolean send(String hostAndPort, Function<Connection, M> creator, Consumer<M> sender);

    abstract public <M extends BaseMessage> boolean broadcast(Function<Connection, M> creator, Consumer<M> sender);

}

