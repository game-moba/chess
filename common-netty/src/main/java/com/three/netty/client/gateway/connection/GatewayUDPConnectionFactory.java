package com.three.netty.client.gateway.connection;

import com.google.common.collect.Maps;
import com.three.api.connection.Connection;
import com.three.api.service.Listener;
import com.three.api.spi.common.ServiceDiscoveryFactory;
import com.three.api.srd.ServiceDiscovery;
import com.three.api.srd.ServiceNode;
import com.three.common.message.base.BaseMessage;
import com.three.netty.core.server.GatewayUDPConnector;
import com.three.tools.thread.pool.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.three.api.srd.ServiceNames.GATEWAY_SERVER;
import static com.three.config.common.IConfig.chess.net.gateway_server_multicast;
import static com.three.config.common.IConfig.chess.net.gateway_server_port;

/**
 * Created by mathua on 2017/5/30.
 */
public class GatewayUDPConnectionFactory extends GatewayConnectionFactory {

    private final Logger logger = LoggerFactory.getLogger(GatewayUDPConnectionFactory.class);

    private final Map<String, InetSocketAddress> ip_address = Maps.newConcurrentMap();

    private final GatewayUDPConnector connector = GatewayUDPConnector.I();

    private final InetSocketAddress multicastRecipient = new InetSocketAddress(gateway_server_multicast, gateway_server_port);

    @Override
    protected void doStart(Listener listener) throws Throwable {
        ThreadPoolManager.I.newThread("udp-client", () -> connector.start(listener)).start();
        ServiceDiscovery discovery = ServiceDiscoveryFactory.create();
        discovery.subscribe(GATEWAY_SERVER, this);
        discovery.lookup(GATEWAY_SERVER).forEach(this::addConnection);
    }

    @Override
    public void onServiceAdded(String path, ServiceNode node) {
        addConnection(node);
    }

    @Override
    public void onServiceUpdated(String path, ServiceNode node) {
        addConnection(node);
    }

    @Override
    public void onServiceRemoved(String path, ServiceNode node) {
        ip_address.remove(node.hostAndPort());
        logger.warn("Gateway Server zkNode={} was removed.", node);
    }

    private void addConnection(ServiceNode node) {
        ip_address.put(node.hostAndPort(), new InetSocketAddress(node.getHost(), node.getPort()));
    }

    @Override
    public void doStop(Listener listener) throws Throwable {
        ip_address.clear();
        connector.stop();
    }

    @Override
    public Connection getConnection(String hostAndPort) {
        return connector.getConnection();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <M extends BaseMessage> boolean send(String hostAndPort, Function<Connection, M> creator, Consumer<M> sender) {
        InetSocketAddress recipient = ip_address.get(hostAndPort);
        if (recipient == null) return false;// gateway server 找不到，直接返回推送失败

        M message = creator.apply(connector.getConnection());
        message.setRecipient(recipient);
        sender.accept(message);
        return true;
    }

    @Override
    public <M extends BaseMessage> boolean broadcast(Function<Connection, M> creator, Consumer<M> sender) {
        M message = creator.apply(connector.getConnection());
        message.setRecipient(multicastRecipient);
        sender.accept(message);
        return true;
    }
}
