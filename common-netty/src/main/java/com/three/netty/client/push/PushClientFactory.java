package com.three.netty.client.push;

import com.three.api.push.PushSender;
import com.three.api.spi.Spi;
import com.three.api.spi.client.PusherFactory;

/**
 * Created by mathua on 2017/5/30.
 */
@Spi
public class PushClientFactory implements PusherFactory {
    private volatile PushClient client;

    @Override
    public PushSender get() {
        if (client == null) {
            synchronized (PushClientFactory.class) {
                if (client == null) {
                    client = new PushClient();
                }
            }
        }
        return client;
    }
}
