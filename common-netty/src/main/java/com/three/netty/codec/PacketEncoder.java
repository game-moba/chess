package com.three.netty.codec;

import com.three.api.protocol.Packet;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * length(4)+cmd(2)+cc(2)+flags(1)+sessionId(4)+lrc(1)+body(n)
 *
 * Created by mathua on 2017/5/21.
 */
@ChannelHandler.Sharable
public final class PacketEncoder extends MessageToByteEncoder<Packet> {
    public static final PacketEncoder INSTANCE = new PacketEncoder();

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf out) throws Exception {
        Packet.encodePacket(packet, out);
    }
}
