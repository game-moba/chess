package com.three.netty.core.ack;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface AckCallback {
    void onSuccess(AckTask context);

    void onTimeout(AckTask context);
}

