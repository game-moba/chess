package com.three.netty.core.ack;

import com.three.api.service.BaseService;
import com.three.api.service.Listener;
import com.three.tools.thread.pool.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mathua on 2017/5/25.
 */
public final class AckTaskQueue extends BaseService {
    private final Logger logger = LoggerFactory.getLogger(AckTaskQueue.class);

    private static final int DEFAULT_TIMEOUT = 3000;
    public static final AckTaskQueue I = new AckTaskQueue();

    private final ConcurrentMap<Integer, AckTask> queue = new ConcurrentHashMap<>();
    private ScheduledExecutorService scheduledExecutor;

    private AckTaskQueue() {
    }

    public void add(AckTask task, int timeout) {
        queue.put(task.ackMessageId, task);

        //使用 task.getExecutor() 并没更快
        task.setFuture(scheduledExecutor.schedule(task,
                timeout > 0 ? timeout : DEFAULT_TIMEOUT,
                TimeUnit.MILLISECONDS
        ));

        logger.debug("one ack task add to queue, task={}, timeout={}", task, timeout);
    }

    public AckTask getAndRemove(int sessionId) {
        return queue.remove(sessionId);
    }

    @Override
    protected void doStart(Listener listener) throws Throwable {
        scheduledExecutor = ThreadPoolManager.I.getAckTimer();
        super.doStart(listener);
    }

    @Override
    protected void doStop(Listener listener) throws Throwable {
        if (scheduledExecutor != null) {
            scheduledExecutor.shutdown();
        }
        super.doStop(listener);
    }
}