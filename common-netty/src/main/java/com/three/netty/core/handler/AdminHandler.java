package com.three.netty.core.handler;

import com.google.common.base.Strings;
import com.three.common.router.RemoteRouter;
import com.three.common.player.PlayerManager;
import com.three.netty.core.router.RouterCenter;
import com.three.netty.core.server.ConnectionServer;
import com.three.tools.Utils;
import com.three.tools.common.Profiler;
import com.three.utils.JsonUtils;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

/**
 * Created by Mathua on 2017/5/26.
 */
@ChannelHandler.Sharable
public final class AdminHandler extends SimpleChannelInboundHandler<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminHandler.class);

    private static final String EOL = "\r\n";

    private final LocalDateTime startTime = LocalDateTime.now();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String request) throws Exception {
        Command command = Command.help;
        String arg = null;
        String[] args = null;
        if (request != null) {
            String[] cmd_args = request.split(" ");
            command = Command.toCmd(cmd_args[0].trim());
            if (cmd_args.length == 2) {
                arg = cmd_args[1];
            } else if (cmd_args.length > 2) {
                args = Arrays.copyOfRange(cmd_args, 1, cmd_args.length);
            }
        }
        try {
            Object result = args != null ? command.handler(ctx, args) : command.handler(ctx, arg);
            ChannelFuture future = ctx.writeAndFlush(result + EOL + EOL);
            if (command == Command.quit) {
                future.addListener(ChannelFutureListener.CLOSE);
            }
        } catch (Throwable throwable) {
            ctx.writeAndFlush(throwable.getLocalizedMessage() + EOL + EOL);
            StringWriter writer = new StringWriter(1024);
            throwable.printStackTrace(new PrintWriter(writer));
            ctx.writeAndFlush(writer.toString());
        }
        LOGGER.info("receive admin command={}", request);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ctx.write("Welcome to Chess Console [" + Utils.getLocalIp() + "]!" + EOL);
        ctx.write("since " + startTime + " has running " + startTime.until(LocalDateTime.now(), ChronoUnit.HOURS) + "(h)" + EOL + EOL);
        ctx.write("It is " + new Date() + " now." + EOL + EOL);
        ctx.flush();
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    public enum Command {
        help {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) {
                return "Option                               Description" + EOL +
                        "------                               -----------" + EOL +
                        "help                                 show help" + EOL +
                        "quit                                 exit console mode" + EOL +
                        "shutdown                             stop chess server" + EOL +
                        "restart                              restart chess server" + EOL +
                        "zk:<redis, cs ,gs>                   query zk node" + EOL +
                        "count:<conn, online>                 count conn num or online player count" + EOL +
                        "route:<uid>                          show player route info" + EOL +
                        "push:<uid>, <msg>                    push test msg to client" + EOL +
                        "conf:[key]                           show config info" + EOL +
                        "monitor:[mxBean]                     show system monitor" + EOL +
                        "profile:<1,0>                        enable/disable profile" + EOL;
            }
        },
        quit {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) {
                return "have a good day!";
            }
        },
        shutdown {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) {
                new Thread(() -> System.exit(0)).start();
                return "try close connect server...";
            }
        },
        restart {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) {
                return "unsupported";
            }
        },

        count {
            @Override
            public Serializable handler(ChannelHandlerContext ctx, String args) {
                switch (args) {
                    case "conn":
                        return ConnectionServer.I().getConnectionManager().getConnNum();
                    case "online": {
                        return PlayerManager.I.getOnlinePlayerNum();
                    }

                }
                return "[" + args + "] unsupported, try help.";
            }
        },
        route {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) {
                if (Strings.isNullOrEmpty(args)) return "please input playerId";
                Set<RemoteRouter> routers = RouterCenter.I.getRemoteRouterManager().lookupAll(Long.parseLong(args));
                if (routers.isEmpty()) return "player [" + args + "] offline now.";
                return JsonUtils.toJson(routers);
            }
        },
        push {
            @Override
            public String handler(ChannelHandlerContext ctx, String... args) throws Exception {
                //Boolean success = PushSender.create().send(args[1], args[0], null).get(5, TimeUnit.SECONDS);

                return "unsupported";
            }
        },
        // TODO 暂时不支持查阅配置
//        conf {
//            @Override
//            public String handler(ChannelHandlerContext ctx, String args) {
//                if (Strings.isNullOrEmpty(args)) {
//                    return CC.cfg.root().render(ConfigRenderOptions.concise().setFormatted(true));
//                }
//                if (CC.cfg.hasPath(args)) {
//                    return CC.cfg.getAnyRef(args).toString();
//                }
//                return "key [" + args + "] not find in config";
//            }
//        },
        profile {
            @Override
            public String handler(ChannelHandlerContext ctx, String args) throws Exception {
                if (args == null || "0".equals(args)) {
                    Profiler.enable(false);
                    return "Profiler disabled";
                } else {
                    Profiler.enable(true);
                    return "Profiler enabled";
                }
            }
        };

        public Object handler(ChannelHandlerContext ctx, String... args) throws Exception {
            return "unsupported";
        }

        public Object handler(ChannelHandlerContext ctx, String args) throws Exception {
            return "unsupported";
        }

        public static Command toCmd(String cmd) {
            try {
                return Command.valueOf(cmd);
            } catch (Exception e) {
            }
            return help;
        }
    }
}
