package com.three.netty.core.handler.annotation;

import com.three.common.receiver.ReceiverType;
import com.three.protocol.CommandEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mathua on 2017/5/30.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PacketHandlerMethod {
    CommandEnum.Command msgId();

    String describe() default "";

    /**
     * 是否需要验证身份
     * @return
     */
    boolean shouldVerifyIdentity() default true;

    ReceiverType[] receivers();

}
