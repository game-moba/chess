package com.three.netty.core.mq;

import com.three.api.common.Condition;
import com.three.api.protocol.Packet;
import com.three.api.spi.push.IPushMessage;

/**
 * Created by mathua on 2017/5/30.
 */
public final class MQPushMessage implements IPushMessage {

    @Override
    public boolean isBroadcast() {
        return false;
    }

    @Override
    public long getPlayerId() {
        return 0;
    }

    @Override
    public int getClientType() {
        return 0;
    }

    @Override
    public Packet getPushPacket() {
        return null;
    }

    @Override
    public boolean isNeedAck() {
        return false;
    }

    @Override
    public byte getFlags() {
        return 0;
    }

    @Override
    public int getTimeoutMills() {
        return 0;
    }

    @Override
    public String getTaskId() {
        return null;
    }

    @Override
    public Condition getCondition() {
        return null;
    }
}
