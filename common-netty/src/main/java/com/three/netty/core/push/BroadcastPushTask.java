package com.three.netty.core.push;

import com.three.api.Message;
import com.three.api.common.Condition;
import com.three.api.connection.Connection;
import com.three.api.connection.SessionContext;
import com.three.api.protocol.Packet;
import com.three.api.spi.push.IPushMessage;
import com.three.common.condition.AwaysPassCondition;
import com.three.common.message.base.BaseMessage;
import com.three.common.message.base.PushMessage;
import com.three.common.qps.FlowControl;
import com.three.exception.OverFlowException;
import com.three.netty.core.router.LocalRouter;
import com.three.netty.core.router.RouterCenter;
import com.three.tools.common.TimeLine;
import com.three.utils.LogUtils;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class BroadcastPushTask implements PushTask, ChannelFutureListener {

    private final long begin = System.currentTimeMillis();

    private final AtomicInteger finishTasks = new AtomicInteger(0);

    private final FlowControl flowControl;

    private final IPushMessage message;

    private final Condition condition;

    private final TimeLine timeLine = new TimeLine();

    //使用Iterator, 记录任务遍历到的位置，因为有流控，一次任务可能会被分批发送，而且还有在推送过程中上/下线的用户
    private final Iterator<Map.Entry<Long, Map<Integer, LocalRouter>>> iterator;

    public BroadcastPushTask(IPushMessage message, FlowControl flowControl) {
        this.message = message;
        this.flowControl = flowControl;
        this.condition = message.getCondition();
        this.iterator = RouterCenter.I.getLocalRouterManager().routers().entrySet().iterator();
        this.timeLine.begin("push-center-begin");
    }

    @Override
    public void run() {
        flowControl.reset();
        boolean done = broadcast();
        if (done) {//done 广播结束
            if (finishTasks.addAndGet(flowControl.total()) == 0) {
                report();
            }
        } else {//没有结束，就延时进行下次任务 TODO 考虑优先级问题
            PushCenter.I.delayTask(flowControl.getDelay(), this);
        }
        flowControl.end();
    }


    private boolean broadcast() {
        try {
            iterator.forEachRemaining(entry -> {

                long playerId = entry.getKey();
                entry.getValue().forEach((clientType, router) -> {

                    Connection connection = router.getRouteValue();

                    if (checkCondition(condition, connection)) {//1.条件检测
                        if (connection.isConnected()) {
                            if (connection.getChannel().isWritable()) { //检测TCP缓冲区是否已满且写队列超过最高阀值
                                // 转发数据包
                                Packet packet = message.getPushPacket();
                                connection.send(packet, this);
                                //4. 检测qps, 是否超过流控限制，如果超过则结束当前循环直接进入catch
                                if (!flowControl.checkQps()) {
                                    throw new OverFlowException(false);
                                }
                            }
                        } else { //2.如果链接失效，先删除本地失效的路由，再查下远程路由，看用户是否登陆到其他机器
                            LogUtils.PUSH.warn("[Broadcast] find router in local but conn disconnect, message={}, conn={}", message, connection);
                            //删除已经失效的本地路由
                            RouterCenter.I.getLocalRouterManager().unRegister(playerId, clientType);
                        }
                    }

                });

            });
        } catch (OverFlowException e) {
            //超出最大限制，或者遍历完毕，结束广播
            return e.isOverMaxLimit() || !iterator.hasNext();
        }
        return !iterator.hasNext();//遍历完毕, 广播结束
    }

    private void report() {
        LogUtils.PUSH.info("[Broadcast] task finished, cost={}, message={}", (System.currentTimeMillis() - begin), message);
        PushCenter.I.getPushListener().onBroadcastComplete(message, timeLine.end().getTimePoints());//通知发送方，广播推送完毕
    }

    private boolean checkCondition(Condition condition, Connection connection) {
        if (condition == AwaysPassCondition.I) return true;
        SessionContext sessionContext = connection.getSessionContext();
        Map<String, Object> env = new HashMap<>();
        env.put("openId", sessionContext.getOpenId());
        env.put("clientVersion", sessionContext.getClientVersion());
        env.put("osName", sessionContext.getOsName());
        env.put("osVersion", sessionContext.getOsVersion());
        return condition.test(env);
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        if (future.isSuccess()) {//推送成功
            LogUtils.PUSH.info("[Broadcast] push message to client success, playerId={}, message={}", message.getPlayerId(), message);
        } else {//推送失败
            LogUtils.PUSH.warn("[Broadcast] push message to client failure, playerId={}, message={}, conn={}", message.getPlayerId(), message, future.channel());
        }
        if (finishTasks.decrementAndGet() == 0) {
            report();
        }
    }

    @Override
    public ScheduledExecutorService getExecutor() {
        return ((Message) message).getConnection().getChannel().eventLoop();
    }
}
