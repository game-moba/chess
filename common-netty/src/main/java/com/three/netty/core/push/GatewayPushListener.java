package com.three.netty.core.push;

import com.three.api.spi.Spi;
import com.three.api.spi.push.PushListener;
import com.three.api.spi.push.PushListenerFactory;
import com.three.common.message.base.ErrorMessage;
import com.three.common.message.base.OkMessage;
import com.three.common.message.base.gateway.GatewayPushMessage;
import com.three.utils.JsonUtils;
import com.three.utils.LogUtils;

import java.util.concurrent.ScheduledExecutorService;

import static com.three.common.ErrorCode.OFFLINE;
import static com.three.common.ErrorCode.PUSH_CLIENT_FAILURE;
import static com.three.common.ErrorCode.ROUTER_CHANGE;

import static com.three.common.push.GatewayPushResult.toJson;

/**
 * Created by mathua on 2017/5/30.
 */
@Spi(order = 1)
public final class GatewayPushListener implements PushListener<GatewayPushMessage>, PushListenerFactory<GatewayPushMessage> {

    @Override
    public void onSuccess(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    OkMessage
                            .from(message)
                            .setMsg(toJson(message, timePoints))
                            .sendRaw();
                }
            });
        } else {
            LogUtils.PUSH.warn("push message to client success, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }

    @Override
    public void onAckSuccess(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    OkMessage
                            .from(message)
                            .setMsg(toJson(message, timePoints))
                            .sendRaw();
                }
            });

        } else {
            LogUtils.PUSH.warn("client ack success, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }

    @Override
    public void onBroadcastComplete(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    OkMessage
                            .from(message)
                            .sendRaw();
                }
            });
        } else {
            LogUtils.PUSH.warn("broadcast to client finish, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }

    @Override
    public void onFailure(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    ErrorMessage
                            .from(message)
                            .setErrorCode(PUSH_CLIENT_FAILURE)
                            .addData(toJson(message, timePoints))
                            .sendRaw();
                }
            });
        } else {
            LogUtils.PUSH.warn("push message to client failure, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }

    @Override
    public void onOffline(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    ErrorMessage
                            .from(message)
                            .setErrorCode(OFFLINE)
                            .addData(toJson(message, timePoints))
                            .sendRaw();
                }
            });
        } else {
            LogUtils.PUSH.warn("push message to client offline, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }

    @Override
    public void onRedirect(GatewayPushMessage message, Object[] timePoints) {
        if (message.getConnection().isConnected()) {
            PushCenter.I.addTask(new PushTask() {
                @Override
                public ScheduledExecutorService getExecutor() {
                    return message.getExecutor();
                }

                @Override
                public void run() {
                    ErrorMessage
                            .from(message)
                            .setErrorCode(ROUTER_CHANGE)
                            .addData(toJson(message, timePoints))
                            .sendRaw();
                }
            });
        } else {
            LogUtils.PUSH.warn("push message to client redirect, but gateway connection is closed, timePoints={}, message={}"
                    , JsonUtils.toJson(timePoints), message);
        }
    }


    @Override
    public void onTimeout(GatewayPushMessage message, Object[] timePoints) {
        LogUtils.PUSH.warn("push message to client timeout, timePoints={}, message={}"
                , JsonUtils.toJson(timePoints), message);
    }

    @Override
    public PushListener<GatewayPushMessage> get() {
        return this;
    }
}
