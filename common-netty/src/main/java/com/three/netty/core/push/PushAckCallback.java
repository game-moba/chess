package com.three.netty.core.push;

import com.three.api.spi.push.IPushMessage;
import com.three.netty.core.ack.AckCallback;
import com.three.netty.core.ack.AckTask;
import com.three.tools.common.TimeLine;
import com.three.utils.LogUtils;

/**
 * Created by mathua on 2017/5/26.
 */
public final class PushAckCallback implements AckCallback {
    private final IPushMessage message;
    private final TimeLine timeLine;

    public PushAckCallback(IPushMessage message, TimeLine timeLine) {
        this.message = message;
        this.timeLine = timeLine;
    }

    @Override
    public void onSuccess(AckTask task) {
        PushCenter.I.getPushListener().onAckSuccess(message, timeLine.successEnd().getTimePoints());
        LogUtils.PUSH.info("[SinglePlayerPush] client ack success, timeLine={}, task={}", timeLine, task);
    }

    @Override
    public void onTimeout(AckTask task) {
        PushCenter.I.getPushListener().onTimeout(message, timeLine.timeoutEnd().getTimePoints());
        LogUtils.PUSH.warn("[SinglePlayerPush] client ack timeout, timeLine={}, task={}", timeLine, task);
    }
}
