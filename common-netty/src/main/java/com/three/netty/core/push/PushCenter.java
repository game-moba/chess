package com.three.netty.core.push;

import com.three.api.service.BaseService;
import com.three.api.service.Listener;
import com.three.api.spi.Spi;
import com.three.api.spi.push.*;
import com.three.common.qps.FastFlowControl;
import com.three.common.qps.FlowControl;
import com.three.common.qps.GlobalFlowControl;
import com.three.common.qps.RedisFlowControl;
import com.three.config.common.IConfig;
import com.three.monitor.jmx.MBeanRegistry;
import com.three.monitor.jmx.mxbean.PushCenterBean;
import com.three.netty.core.ack.AckTaskQueue;
import com.three.tools.thread.pool.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static com.three.config.common.IConfig.chess.push.flow_control.broadcast;

/**
 * Created by Mathua on 2017/5/25.
 */
public final class PushCenter extends BaseService implements MessagePusher {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final PushCenter I = new PushCenter();

    private final GlobalFlowControl globalFlowControl = new GlobalFlowControl(
            IConfig.chess.push.flow_control.global.limit, IConfig.chess.push.flow_control.global.max, IConfig.chess.push.flow_control.global.duration
    );

    private final AtomicLong taskNum = new AtomicLong();

    private final PushListener<IPushMessage> pushListener = PushListenerFactory.create();

    private PushTaskExecutor executor;

    @Override
    public void push(IPushMessage message) {
        if (message.isBroadcast()) {
            FlowControl flowControl = (message.getTaskId() == null)
                    ? new FastFlowControl(broadcast.limit, broadcast.max, broadcast.duration)
                    : new RedisFlowControl(message.getTaskId(), broadcast.max);
            addTask(new BroadcastPushTask(message, flowControl));
        } else {
            addTask(new SinglePlayerPushTask(message, globalFlowControl));
        }
    }

    public void addTask(PushTask task) {
        executor.addTask(task);
        logger.debug("add new task to push center, count={}, task={}", taskNum.incrementAndGet(), task);
    }

    public void delayTask(long delay, PushTask task) {
        executor.delayTask(delay, task);
        logger.debug("delay task to push center, count={}, task={}", taskNum.incrementAndGet(), task);
    }

    @Override
    protected void doStart(Listener listener) throws Throwable {
        if (IConfig.chess.net.udpGateway() || IConfig.chess.thread.pool.push_task > 0) {
            executor = new CustomJDKExecutor(ThreadPoolManager.I.getPushTaskTimer());
        } else {//实际情况使用EventLoo并没有更快，还有待测试
            executor = new NettyEventLoopExecutor();
        }

        MBeanRegistry.getInstance().register(new PushCenterBean(taskNum), null);
        AckTaskQueue.I.start();
        logger.info("push center start success");
        listener.onSuccess();
    }

    @Override
    protected void doStop(Listener listener) throws Throwable {
        executor.shutdown();
        AckTaskQueue.I.stop();
        logger.info("push center stop success");
        listener.onSuccess();
    }

    public PushListener<IPushMessage> getPushListener() {
        return pushListener;
    }


    /**
     * TCP 模式直接使用GatewayServer work 线程池
     */
    private static class NettyEventLoopExecutor implements PushTaskExecutor {

        @Override
        public void shutdown() {
        }

        @Override
        public void addTask(PushTask task) {
            task.getExecutor().execute(task);
        }

        @Override
        public void delayTask(long delay, PushTask task) {
            task.getExecutor().schedule(task, delay, TimeUnit.NANOSECONDS);
        }
    }


    /**
     * UDP 模式使用自定义线程池
     */
    private static class CustomJDKExecutor implements PushTaskExecutor {
        private final ScheduledExecutorService executorService;

        private CustomJDKExecutor(ScheduledExecutorService executorService) {
            this.executorService = executorService;
        }

        @Override
        public void shutdown() {
            executorService.shutdown();
        }

        @Override
        public void addTask(PushTask task) {
            executorService.execute(task);
        }

        @Override
        public void delayTask(long delay, PushTask task) {
            executorService.schedule(task, delay, TimeUnit.NANOSECONDS);
        }
    }

    private interface PushTaskExecutor {

        void shutdown();

        void addTask(PushTask task);

        void delayTask(long delay, PushTask task);
    }

    @Spi(order = -1)
    public static final class CoreMessagePusherFactory implements MessagePusherFactory {
        @Override
        public MessagePusher get() {
            return PushCenter.I;
        }
    }
}
