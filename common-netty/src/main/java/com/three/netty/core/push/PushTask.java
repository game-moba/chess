package com.three.netty.core.push;

import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface PushTask extends Runnable {
    ScheduledExecutorService getExecutor();
}
