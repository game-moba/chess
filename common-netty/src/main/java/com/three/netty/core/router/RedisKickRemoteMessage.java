package com.three.netty.core.router;

import com.three.common.router.KickRemoteMsg;

/**
 * Created by mathua on 2017/5/26.
 */
class RedisKickRemoteMessage implements KickRemoteMsg {
    private long playerId;
    private String deviceId;
    private String connId;
    private int clientType;
    private String targetServer;
    private int targetPort;

    public RedisKickRemoteMessage setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public RedisKickRemoteMessage setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public RedisKickRemoteMessage setConnId(String connId) {
        this.connId = connId;
        return this;
    }

    public RedisKickRemoteMessage setClientType(int clientType) {
        this.clientType = clientType;
        return this;
    }

    public RedisKickRemoteMessage setTargetServer(String targetServer) {
        this.targetServer = targetServer;
        return this;
    }

    public RedisKickRemoteMessage setTargetPort(int targetPort) {
        this.targetPort = targetPort;
        return this;
    }

    @Override
    public long getPlayerId() {
        return playerId;
    }

    @Override
    public String getDeviceId() {
        return deviceId;
    }

    @Override
    public String getConnId() {
        return connId;
    }

    @Override
    public int getClientType() {
        return clientType;
    }

    @Override
    public String getTargetServer() {
        return targetServer;
    }

    @Override
    public int getTargetPort() {
        return targetPort;
    }

    @Override
    public String toString() {
        return "KickRemoteMsg{"
                + "playerId='" + playerId + '\''
                + ", deviceId='" + deviceId + '\''
                + ", connId='" + connId + '\''
                + ", clientType='" + clientType + '\''
                + ", targetServer='" + targetServer + '\''
                + ", targetPort=" + targetPort
                + '}';
    }
}
