package com.three.netty.core.server;

import com.three.config.common.IConfig;
import com.three.netty.core.handler.AdminHandler;
import com.three.netty.server.NettyTCPServer;
import com.three.tools.thread.ThreadNames;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class AdminServer extends NettyTCPServer {
    private static AdminServer I;

    private AdminHandler adminHandler;

    public static AdminServer I() {
        if (I == null) {
            synchronized (AdminServer.class) {
                I = new AdminServer();
            }
        }
        return I;
    }

    private AdminServer() {
        super(IConfig.chess.net.admin_server_port);
    }

    @Override
    public void init() {
        super.init();
        this.adminHandler = new AdminHandler();
    }

    @Override
    protected void initPipeline(ChannelPipeline pipeline) {
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        super.initPipeline(pipeline);
    }

    @Override
    public ChannelHandler getChannelHandler() {
        return adminHandler;
    }

    @Override
    protected ChannelHandler getDecoder() {
        return new StringDecoder();
    }

    @Override
    protected ChannelHandler getEncoder() {
        return new StringEncoder();
    }

    @Override
    protected int getWorkThreadNum() {
        return 1;
    }

    @Override
    protected String getBossThreadName() {
        return ThreadNames.T_ADMIN_BOSS;
    }

    @Override
    protected String getWorkThreadName() {
        return ThreadNames.T_ADMIN_WORKER;
    }
}

