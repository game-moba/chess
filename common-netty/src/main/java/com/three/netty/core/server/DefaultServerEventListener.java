package com.three.netty.core.server;

import com.three.api.ServerEventListener;
import com.three.api.spi.Spi;
import com.three.api.spi.core.ServerEventListenerFactory;

/**
 * Created by mathua on 2017/5/30.
 */
@Spi(order = 1)
public final class DefaultServerEventListener implements ServerEventListener, ServerEventListenerFactory {

    @Override
    public ServerEventListener get() {
        return this;
    }
}
