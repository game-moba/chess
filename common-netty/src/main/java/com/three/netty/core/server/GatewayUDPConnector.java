package com.three.netty.core.server;

import com.three.api.connection.Connection;
import com.three.config.common.IConfig;
import com.three.netty.udp.NettyUDPConnector;
import com.three.netty.udp.UDPChannelHandler;
import com.three.tools.Utils;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class GatewayUDPConnector extends NettyUDPConnector {

    private static GatewayUDPConnector I;

    private UDPChannelHandler channelHandler;

    public static GatewayUDPConnector I() {
        if (I == null) {
            synchronized (GatewayUDPConnector.class) {
                if (I == null) {
                    I = new GatewayUDPConnector();
                }
            }
        }
        return I;
    }

    private GatewayUDPConnector() {
        super(IConfig.chess.net.gateway_server_port);
    }

    @Override
    public void init() {
        super.init();
        channelHandler.setMulticastAddress(Utils.getInetAddress(IConfig.chess.net.gateway_server_multicast));
        channelHandler.setNetworkInterface(Utils.getLocalNetworkInterface());
    }

    @Override
    protected void initOptions(Bootstrap b) {
        super.initOptions(b);
        b.option(ChannelOption.IP_MULTICAST_LOOP_DISABLED, true);//默认情况下，当本机发送组播数据到某个网络接口时，在IP层，数据会回送到本地的回环接口，选项IP_MULTICAST_LOOP用于控制数据是否回送到本地的回环接口
        b.option(ChannelOption.IP_MULTICAST_TTL, 255);//选项IP_MULTICAST_TTL允许设置超时TTL，范围为0～255之间的任何值
        //b.option(ChannelOption.IP_MULTICAST_IF, null);//选项IP_MULTICAST_IF用于设置组播的默认网络接口，会从给定的网络接口发送，另一个网络接口会忽略此数据,参数addr是希望多播输出接口的IP地址，使用INADDR_ANY地址回送到默认接口。
        //b.option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(32 * 1024, 1024 * 1024));
        if (IConfig.chess.net.snd_buf.gateway_server > 0) b.option(ChannelOption.SO_SNDBUF, IConfig.chess.net.snd_buf.gateway_server);
        if (IConfig.chess.net.rcv_buf.gateway_server > 0) b.option(ChannelOption.SO_RCVBUF, IConfig.chess.net.rcv_buf.gateway_server);
    }

    @Override
    public ChannelHandler getChannelHandler() {
        return channelHandler;
    }

    public Connection getConnection() {
        return channelHandler.getConnection();
    }
}
