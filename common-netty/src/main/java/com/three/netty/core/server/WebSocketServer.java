package com.three.netty.core.server;

import com.three.api.connection.ConnectionManager;
import com.three.api.service.Listener;
import com.three.config.common.IConfig;
import com.three.common.receiver.ReceiverType;
import com.three.netty.server.NettyTCPServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;

/**
 * Created by mathua on 2017/5/28.
 */
public final class WebSocketServer extends NettyTCPServer {

    private static WebSocketServer I;

    private ChannelHandler channelHandler;

    private ConnectionManager connectionManager = new ServerConnectionManager(false);

    public static WebSocketServer I() {
        if (I == null) {
            synchronized (WebSocketServer.class) {
                if (I == null) {
                    I = new WebSocketServer();
                }
            }
        }
        return I;
    }

    private WebSocketServer() {
        super(IConfig.chess.net.ws_server_port);
    }

    @Override
    public void init() {
        super.init();
        connectionManager.init();
        channelHandler = new WebSocketChannelHandler(connectionManager, ReceiverType.WEB_SOCKET_SERVER_RECEIVER.getReceiver());
    }

    @Override
    public void stop(Listener listener) {
        super.stop(listener);
        connectionManager.destroy();
    }

    @Override
    public EventLoopGroup getBossGroup() {
        return ConnectionServer.I().getBossGroup();
    }

    @Override
    public EventLoopGroup getWorkerGroup() {
        return ConnectionServer.I().getWorkerGroup();
    }

    @Override
    protected void initPipeline(ChannelPipeline pipeline) {
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(65536));
        pipeline.addLast(new WebSocketServerCompressionHandler());
        pipeline.addLast(new WebSocketServerProtocolHandler(IConfig.chess.net.ws_path, null, true));
        pipeline.addLast(new WebSocketIndexPageHandler());
        pipeline.addLast(getChannelHandler());
    }

    @Override
    protected void initOptions(ServerBootstrap b) {
        super.initOptions(b);
        b.option(ChannelOption.SO_BACKLOG, 1024);
        b.childOption(ChannelOption.SO_SNDBUF, 32 * 1024);
        b.childOption(ChannelOption.SO_RCVBUF, 32 * 1024);
    }

    @Override
    public ChannelHandler getChannelHandler() {
        return channelHandler;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

}

