package com.three.netty.core.session;

import com.three.api.connection.SessionContext;
import com.three.common.security.AesCipher;

/**
 * Created by mathua on 2017/5/28.
 */
public final class ReusableSession {
    public String sessionId;
    public long expireTime;
    public SessionContext context;

    public static String encode(SessionContext context) {
        StringBuffer sb = new StringBuffer();
        sb.append(context.getOsName()).append(',');
        sb.append(context.getOsVersion()).append(',');
        sb.append(context.getClientVersion()).append(',');
        sb.append(context.getDeviceId()).append(',');
        sb.append(context.getCipher());
        return sb.toString();
    }

    public static ReusableSession decode(String value) {
        String[] array = value.split(",");
        if (array.length != 6) return null;
        SessionContext context = new SessionContext();
        context.setOsName(array[0]);
        context.setOsVersion(array[1]);
        context.setClientVersion(array[2]);
        context.setDeviceId(array[3]);
        byte[] key = AesCipher.toArray(array[4]);
        byte[] iv = AesCipher.toArray(array[5]);
        if (key == null || iv == null) return null;
        context.setCipher(new AesCipher(key, iv));
        ReusableSession session = new ReusableSession();
        session.context = context;
        return session;
    }
}

