package com.three.netty.core.session;

import com.three.api.connection.SessionContext;
import com.three.api.spi.common.CacheManager;
import com.three.api.spi.common.CacheManagerFactory;
import com.three.common.CacheKeys;
import com.three.config.common.IConfig;
import com.three.tools.crypto.MD5Utils;
import com.three.utils.StringUtils;

/**
 * Created by mathua on 2017/5/28.
 */
public final class ReusableSessionManager {
    public static final ReusableSessionManager I = new ReusableSessionManager();
    private final int expiredTime = IConfig.chess.core.session_expired_time;
    private final CacheManager cacheManager = CacheManagerFactory.create();

    public boolean cacheSession(ReusableSession session) {
        String key = CacheKeys.getSessionKey(session.sessionId);
        cacheManager.set(key, ReusableSession.encode(session.context), expiredTime);
        return true;
    }

    public ReusableSession querySession(String sessionId) {
        String key = CacheKeys.getSessionKey(sessionId);
        String value = cacheManager.get(key, String.class);
        if (StringUtils.isBlank(value)) return null;
        return ReusableSession.decode(value);
    }

    public ReusableSession genSession(SessionContext context) {
        long now = System.currentTimeMillis();
        ReusableSession session = new ReusableSession();
        session.context = context;
        session.sessionId = MD5Utils.encrypt(context.getDeviceId() + now);
        session.expireTime = now + expiredTime * 1000;
        return session;
    }
}

