package com.three.netty.http;

import io.netty.handler.codec.http.HttpResponse;

/**
 * Created by mathua on 2017/5/23.
 */
public interface HttpCallback {

    void onResponse(HttpResponse response);

    void onFailure(int statusCode, String reasonPhrase);

    void onException(Throwable throwable);

    void onTimeout();

    boolean onRedirect(HttpResponse response);
}

