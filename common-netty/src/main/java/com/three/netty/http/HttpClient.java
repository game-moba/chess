package com.three.netty.http;

import com.three.api.service.Service;

/**
 * Created by mathua on 2017/5/23.
 */
public interface HttpClient extends Service {
    void request(RequestContext context) throws Exception;
}