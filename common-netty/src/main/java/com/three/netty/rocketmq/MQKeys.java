package com.three.netty.rocketmq;

import com.three.api.connection.Connection;
import com.three.common.message.base.BaseMessage;
import com.three.netty.core.router.LocalRouter;
import com.three.netty.core.router.LocalRouterManager;
import com.three.netty.core.router.RouterCenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Mathua on 2017/6/27.
 */
public enum MQKeys {
    SEND_TO_PLAYER("send-to-player-") {
        @Override
        public boolean check(String key) {
            long playerId = Long.valueOf(key.substring(this.keys.length()));
            LocalRouterManager localRouterManager = RouterCenter.I.getLocalRouterManager();
            return !localRouterManager.lookupAll(playerId).isEmpty();
        }

        @Override
        public Connection getConn(String key) {
            long playerId = Long.valueOf(key.substring(this.keys.length()));
            LocalRouterManager localRouterManager = RouterCenter.I.getLocalRouterManager();
            Set<LocalRouter> localRouters = localRouterManager.lookupAll(playerId);
            for(LocalRouter localRouter : localRouters) {
                return localRouter.getRouteValue();
            }
            return null;
        }
    },
    ;

    private static List<MQKeys> list = new ArrayList<>();
    static {
        for(MQKeys mqKey : values()) {
            list.add(mqKey);
        }
    }

    public final String keys;
    MQKeys(String keys) {
        this.keys = keys;
    }

    public static MQKeys valueOfKey(String key) {
        for(MQKeys mqKey : list) {
            if(key.startsWith(mqKey.keys))
                return mqKey;
        }
        return null;
    }

    public abstract boolean check(String key);

    public abstract Connection getConn(String key);
}
