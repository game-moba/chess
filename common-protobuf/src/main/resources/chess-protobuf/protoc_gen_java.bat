@echo off

cd ..\..
path = %cd%
cd resources\chess-protobuf

echo Generate Protobuf File Start!
for /f "delims=" %%i in ('dir /b "*.proto"') do (
	echo -I=./ --java_out=%%i %path%\java\%%i
	protoc.exe -I=./ --java_out=%path%\java %%i
)

echo Generate Protobuf File End!

pause