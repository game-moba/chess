#!/bin/sh

echo Generate Protobuf File Start!

if [ ! -d java ]; then
	mkdir java
fi

for i in `ls ./*.proto`
do
	echo -I=./ --java_out=$i `pwd`/java/$i
	./protoc -I=./ --java_out=./java $i
done

echo Generate Protobuf File End!
