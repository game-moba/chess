package com.three;

import com.three.utils.FileUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <br>启动命令参数：--spring.profiles.active=dev <br/>
 * Created by 王梓青 on 2017/2/22.
 */
@SpringBootApplication
public class CommonMain {
    public static void main(String[] args) {
        SpringApplication.run(CommonMain.class, args);
        System.out.println(FileUtils.readDirectoryByClasspath("").getAbsolutePath());
    }
}
