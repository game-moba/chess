package com.three.api;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import io.netty.channel.ChannelFutureListener;

/**
 * 消息接口类
 * Created by mathua on 2017/5/21.
 */
public interface Message {

    /**
     * 获取该消息对应的连接
     *
     * @return
     */
    Connection getConnection();

    /**
     * 消息解码
     */
    void decodeBody() throws InvalidProtocolBufferException;

    /**
     * 消息编码
     */
    void encodeBody();

    /**
     * 发送当前message, 并根据情况最body进行数据压缩、加密
     *
     * @param listener 发送成功后的回调
     */
    void send(ChannelFutureListener listener);

    /**
     * 发送当前message, 不会对body进行数据压缩、加密, 原样发送
     *
     * @param listener 发送成功后的回调
     */
    void sendRaw(ChannelFutureListener listener);

    Packet getPacket();
}
