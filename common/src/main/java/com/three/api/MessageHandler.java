package com.three.api;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;

import java.lang.reflect.InvocationTargetException;

/**
 * 消息处理接口类
 * Created by mathua on 2017/5/21.
 */
public interface MessageHandler {
    void handle(Packet packet, Connection connection) throws InvalidProtocolBufferException, InvocationTargetException, IllegalAccessException;
}
