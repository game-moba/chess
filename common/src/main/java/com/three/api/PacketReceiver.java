package com.three.api;

import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;

/**
 * 数据包接收者接口类
 * Created by mathua on 2017/5/21.
 */
public interface PacketReceiver {
    /**
     * 接受数据包时回调
     *
     * @param packet     接收到的数据包
     * @param connection 数据包来源
     */
    void onReceive(Packet packet, Connection connection);
}
