package com.three.api;

import com.three.api.event.*;

/**
 * 服务器事件监听回调类
 * Created by mathua on 2017/5/24.
 */
public interface ServerEventListener {

    /**
     * 接受到服务器启动事件时回调
     *
     * @param event
     */
    default void on(ServerStartupEvent event) {
    }

    /**
     * 接收到服务器关闭事件时回调
     *
     * @param event
     */
    default void on(ServerShutdownEvent event) {
    }

    /**
     * 接收到路由改变事件时回调
     *
     * @param event
     */
    default void on(RouterChangeEvent event) {
    }

    /**
     * 接收到踢玩家下线事件时回调
     *
     * @param event
     */
    default void on(KickPlayerEvent event) {
    }

    /**
     * 接收到玩家上线事件时回调
     *
     * @param event
     */
    default void on(PlayerOnlineEvent event) {
    }

    /**
     * 接收到玩家下线事件时回调
     *
     * @param event
     */
    default void on(PlayerOfflineEvent event) {
    }
}
