package com.three.api.common;

import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface Condition extends Predicate<Map<String, Object>> {
}