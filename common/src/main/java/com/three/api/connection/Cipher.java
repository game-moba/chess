package com.three.api.connection;

/**
 * 密文处理接口
 *
 * @Author mathua
 * @Date 2017/5/19 14:38
 */
public interface Cipher {
    /**
     * 解码
     *
     * @param data 数据字节流
     * @return
     */
    byte[] decrypt(byte[] data);

    /**
     * 编码
     *
     * @param data 数据字节流
     * @return
     */
    byte[] encrypt(byte[] data);
}
