package com.three.api.connection;

import com.three.api.protocol.Packet;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * Created by mathua on 2017/5/21.
 */
public interface Connection {
    byte STATUS_NEW = 0;// 新的连接
    byte STATUS_CONNECTED = 1;// 已经连上的连接
    byte STATUS_DISCONNECTED = 2;// 已经断开的连接

    void init(Channel channel, boolean security);

    /**
     * 获取回话上下文
     *
     * @return
     */
    SessionContext getSessionContext();

    /**
     * 设置会话上下文
     *
     * @param context
     */
    void setSessionContext(SessionContext context);

    /**
     * 发送数据包
     *
     * @param packet 数据包
     * @return 处理结果
     */
    ChannelFuture send(Packet packet);

    /**
     * 发送数据包
     *
     * @param packet   数据包
     * @param listener 处理结果监听器
     * @return 处理结果
     */
    ChannelFuture send(Packet packet, ChannelFutureListener listener);

    String getId();

    ChannelFuture close();

    boolean isConnected();

    boolean isReadTimeout();

    boolean isWriteTimeout();

    void updateLastReadTime();

    void updateLastWriteTime();

    Channel getChannel();

}