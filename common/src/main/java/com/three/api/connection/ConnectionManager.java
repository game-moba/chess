package com.three.api.connection;

import io.netty.channel.Channel;

/**
 * Created by mathua on 2017/5/21.
 */
public interface ConnectionManager {

    /**
     * Channel是Netty的核心概念之一，它是Netty网络通信的主体，由它负责同对端进行网络通信、注册和数据操作等功能。
     * <p>
     * 1.一旦用户端连接成功，将新建一个channel同该用户端进行绑定
     * 2.channel从EventLoopGroup获得一个EventLoop，并注册到该EventLoop，channel生命周期内都和该EventLoop在一起（注册时获得selectionKey）
     * 3.channel同用户端进行网络连接、关闭和读写，生成相对应的event（改变selectinKey信息），触发eventloop调度线程进行执行
     * 4.如果是读事件，执行线程调度pipeline来处理用户业务逻辑
     *
     * @param channel 通道
     * @return
     */
    Connection get(Channel channel);

    Connection removeAndClose(Channel channel);

    void add(Connection connection);

    int getConnNum();

    void init();

    void destroy();
}
