package com.three.api.connection;

import com.three.api.router.ClientClassifier;

/**
 * 会话上下文
 * Created by mathua on 2017/5/21.
 */
public final class SessionContext {
    private String osName;// 系统名字
    private String osVersion;// 系统版本
    private String clientVersion;// 客户端版本
    private String deviceId;// 设备id
    private String openId;// 用户id
    private int heartbeat = 10000;// 10s
    private Cipher cipher;
    private byte clientType;// 客户端类型\
    private long playerId;// 当前在线玩家id

    public void changeCipher(Cipher cipher) {
        this.cipher = cipher;
    }

    public SessionContext setOsName(String osName) {
        this.osName = osName;
        return this;
    }

    public SessionContext setOsVersion(String osVersion) {
        this.osVersion = osVersion;
        return this;
    }

    public SessionContext setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
        return this;
    }

    public SessionContext setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public SessionContext setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public void setCipher(Cipher cipher) {
        this.cipher = cipher;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public void setHeartbeat(int heartbeat) {
        this.heartbeat = heartbeat;
    }

    public void setClientType(byte clientType) {
        this.clientType = clientType;
    }

    public boolean handshakeOk() {
        return deviceId != null && deviceId.length() > 0;
    }

    public int getClientType() {
        if (clientType == 0) {
            clientType = (byte) ClientClassifier.I.getClientType(osName);
        }
        return clientType;
    }

    public boolean isSecurity() {
        return cipher != null;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getOpenId() {
        return openId;
    }

    public int getHeartbeat() {
        return heartbeat;
    }

    public Cipher getCipher() {
        return cipher;
    }

    public long getPlayerId() {
        return playerId;
    }



    @Override
    public String toString() {
        return "{" +
                "osName='" + osName + '\'' +
                ", osVersion='" + osVersion + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", openId='" + openId + '\'' +
                ", heartbeat=" + heartbeat +
                '}';
    }
}

