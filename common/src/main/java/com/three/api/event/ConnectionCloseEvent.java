package com.three.api.event;

import com.three.api.connection.Connection;

/**
 * Created by mathua on 2017/5/21.
 */
public final class ConnectionCloseEvent implements Event {
    public final Connection connection;

    public ConnectionCloseEvent(Connection connection) {
        this.connection = connection;
    }
}
