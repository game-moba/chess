package com.three.api.event;

/**
 * 游戏完成事件
 * TODO
 */
public class GameFinishEvent implements Event {
    private long[] players;
    private int count;
    private long winner;
    private int gameType;
    private int handPattner;

    public long[] getPlayers() {
        return players;
    }

    public int getCount() {
        return count;
    }

    public long getWinner() {
        return winner;
    }

    public int getGameType() {
        return gameType;
    }

    public int getHandPattner() {
        return handPattner;
    }
}
