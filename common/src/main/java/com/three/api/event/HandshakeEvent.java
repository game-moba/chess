package com.three.api.event;

import com.three.api.connection.Connection;

/**
 * 握手事件
 * Created by mathua on 2017/5/21.
 */
public final class HandshakeEvent implements Event {
    public final Connection connection;
    public final int heartbeat;

    public HandshakeEvent(Connection connection, int heartbeat) {
        this.connection = connection;
        this.heartbeat = heartbeat;
    }
}
