package com.three.api.event;

/**
 * 心跳事件
 * Created by mathua on 2017/6/24.
 */
public final class HeartBeatEvent implements Event{
    public final long playerId;

    public HeartBeatEvent(long playerId) {
        this.playerId = playerId;
    }
}
