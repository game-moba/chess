package com.three.api.event;

/**
 * 踢玩家下线事件
 * Created by mathua on 2017/5/22.
 */
public final class KickPlayerEvent implements Event {
    public final long playerId;
    public final String deviceId;// 设备id
    public final String fromServer;

    public KickPlayerEvent(long playerId, String deviceId, String fromServer) {
        this.playerId = playerId;
        this.deviceId = deviceId;
        this.fromServer = fromServer;
    }
}