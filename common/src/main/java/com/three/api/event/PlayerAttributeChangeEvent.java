package com.three.api.event;

/**
 * 用户属性变更事件
 */
public class PlayerAttributeChangeEvent implements Event {
    private final long playerId;
    private final PlayerAttrType attrType;
    private final long preCount;
    private final long currentCount;
    public PlayerAttributeChangeEvent(long playerId, PlayerAttrType attrType,long preCount, long currentCount){
        this.playerId=playerId;
        this.preCount=preCount;
        this.currentCount=currentCount;
        this.attrType=attrType;
    }

    public PlayerAttrType getAttrType() {
        return attrType;
    }

    public long getPlayerId() {
        return playerId;
    }

    public long getPreCount() {
        return preCount;
    }

    public long getCurrentCount() {
        return currentCount;
    }

    /**
     *  对应玩家模块属性，只做标识
     */
    public enum PlayerAttrType{
        GOLD,DIAMOND;
    }
}
