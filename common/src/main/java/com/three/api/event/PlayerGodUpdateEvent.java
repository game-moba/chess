package com.three.api.event;

/**
 * 用户金币改变事件
 */
public class PlayerGodUpdateEvent implements Event {
    public final long playerId;
    private final int preCount;
    private final int currentCount;
    public PlayerGodUpdateEvent(long playerId, int preCount, int currentCount){
        this.playerId=playerId;
        this.preCount=preCount;
        this.currentCount=currentCount;
    }

    public long getPlayerId() {
        return playerId;
    }

    public int getPreCount() {
        return preCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }
}
