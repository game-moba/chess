package com.three.api.event;

public class PlayerInfoChangeEvent implements Event {
    private PlayerInfoType infoType;
    private final long playerId;
    private  Object preData;
    private  Object currentData;
    public PlayerInfoChangeEvent(long playerId){
        this.playerId=playerId;
    }
    public PlayerInfoChangeEvent(long playerId, PlayerInfoType infoType, Object preData, Object currentData){
        this.playerId=playerId;
        this.infoType=infoType;
        this.preData =preData;
        this.currentData = currentData;
    }

    public enum PlayerInfoType {
        NICKNAME,// 昵称
        HEAD,// 头像
        PROVINCEID,// 省份ID
        CITY,// 城市
        PHONE,// 手机
        GENDER;// 性别
    }

    public long getPlayerId() {
        return playerId;
    }

    public PlayerInfoType getInfoType() {
        return infoType;
    }

    public Object getPreData() {
        return preData;
    }

    public Object getCurrentData() {
        return currentData;
    }

    public void setInfoType(PlayerInfoType infoType) {
        this.infoType = infoType;
    }

    public void setPreData(Object preData) {
        this.preData = preData;
    }

    public void setCurrentData(Object currentData) {
        this.currentData = currentData;
    }
}
