package com.three.api.event;

/**
 * 用户升级事件
 */
public class PlayerLevelUpgradeEvent implements Event {
    public final long playerId;
    private final int preLevel;
    private final int currentLevel;
    public PlayerLevelUpgradeEvent(long playerId, int preLevel, int currentLevel){
        this.playerId=playerId;
        this.preLevel=preLevel;
        this.currentLevel=currentLevel;
    }

    public long getPlayerId() {
        return playerId;
    }

    public int getPreLevel() {
        return preLevel;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }
}
