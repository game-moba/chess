package com.three.api.event;

import com.three.api.connection.Connection;

/**
 * 用户离线事件
 * 链接超时，用户解绑的时候,用户主动关闭链接，才会触发该事件
 * <p>
 * Created by mathua on 2017/5/22.
 */
public final class PlayerOfflineEvent implements Event {

    private final Connection connection;
    private final long playerId;

    public PlayerOfflineEvent(Connection connection, long playerId) {
        this.connection = connection;
        this.playerId = playerId;
    }

    public Connection getConnection() {
        return connection;
    }

    public long getPlayerId() {
        return playerId;
    }
}
