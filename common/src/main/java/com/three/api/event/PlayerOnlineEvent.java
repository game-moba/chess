package com.three.api.event;

import com.three.api.connection.Connection;

import java.util.Date;

/**
 * 用户上线事件
 * 绑定用户的时候才会触发该事件
 * Created by mathua on 2017/5/22.
 */
public final class PlayerOnlineEvent implements Event {

    private final Connection connection;
    private final long playerId;
    private final Date preLoginTime;//上次登陆时间

    public PlayerOnlineEvent(Connection connection, long playerId,Date preLoginTime) {
        this.connection = connection;
        this.playerId = playerId;
        this.preLoginTime=preLoginTime;
    }

    public Connection getConnection() {
        return connection;
    }

    public long getPlayerId() {
        return playerId;
    }

    public Date getPreLoginTime() {
        return preLoginTime;
    }
}
