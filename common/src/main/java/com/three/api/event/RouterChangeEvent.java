package com.three.api.event;

import com.three.api.router.Router;

/**
 * 路由改变事件
 * Created by mathua on 2017/5/22.
 */
public final class RouterChangeEvent implements Event {
    public final long playerId;
    public final Router<?> router;

    public RouterChangeEvent(long playerId, Router<?> router) {
        this.playerId = playerId;
        this.router = router;
    }
}
