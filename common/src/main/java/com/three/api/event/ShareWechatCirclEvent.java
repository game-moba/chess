package com.three.api.event;

/**
 * 微信朋友圈分享
 */
public class ShareWechatCirclEvent implements Event {
    private long playerId;

    public long getPlayerId() {
        return playerId;
    }
}
