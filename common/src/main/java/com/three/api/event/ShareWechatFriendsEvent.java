package com.three.api.event;

/**
 * 微信好友分享事件
 */
public class ShareWechatFriendsEvent implements Event {
    private long playerId;

    public long getPlayerId() {
        return playerId;
    }
}
