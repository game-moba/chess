package com.three.api.event;

import java.util.Date;

/**
 * 任务完成事件
 */
public class TaskFinishEvent implements Event {
    private final long playerId;
    private final long taskId;
    private final Date timestamp;
    public TaskFinishEvent(long playerId,long taskId,Date timestamp){
        this.playerId=playerId;
        this.taskId=taskId;
        this.timestamp=timestamp;
    }

    public long getPlayerId() {
        return playerId;
    }

    public long getTaskId() {
        return taskId;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
