package com.three.api.event;

/**
 * Created by Mathua on 2017/5/26.
 */
public interface Topics {
    /**
     * 在线channel
     */
    String ONLINE_CHANNEL = "/chess/online/";

    /**
     * 离线channel
     */
    String OFFLINE_CHANNEL = "/chess/offline/";
}

