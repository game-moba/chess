package com.three.api.protocol;

import com.three.constant.Constants;
import com.three.api.spi.common.Json;
import com.three.protocol.CommandEnum;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.Map;

/**
 * json格式数据包
 *
 * @Author mathua
 * @Date 2017/5/19 15:28
 */
public final class JsonPacket extends Packet {
    public Map<String, Object> body;

    public JsonPacket() {
        super(CommandEnum.Command.UNKNOWN);
        this.addFlag(FLAG_JSON_BODY);// 加入json数据标志
    }

    public JsonPacket(CommandEnum.Command cmd) {
        super(cmd);
        this.addFlag(FLAG_JSON_BODY);// 加入json数据标志
    }

    public JsonPacket(CommandEnum.Command cmd, int sessionId) {
        super(cmd, sessionId);
        this.addFlag(FLAG_JSON_BODY);// 加入json数据标志
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> getBody() {
        return body;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> void setBody(T body) {
        this.body = (Map<String, Object>) body;
    }

    @Override
    public int getBodyLength() {
        return body == null ? 0 : body.size();
    }

    @Override
    public Packet response(CommandEnum.Command command) {
        return new JsonPacket(command, sessionId);
    }

    @Override
    public Object toFrame(Channel channel) {
        byte[] json = Json.JSON.toJson(this).getBytes(Constants.UTF_8);
        return new TextWebSocketFrame(Unpooled.wrappedBuffer(json));
    }

    @Override
    public String toString() {
        return "JsonPacket{" +
                "cmd=" + cmd +
                ", cc=" + cc +
                ", flags=" + flags +
                ", sessionId=" + sessionId +
                ", lrc=" + lrc +
                ", body=" + body +
                '}';
    }
}
