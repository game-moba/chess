package com.three.api.push;

/**
 * 广播控制器
 * Created by Mathua on 2017/5/25.
 */
public interface BroadcastController {

    /**
     * 获取任务id
     *
     * @return
     */
    String taskId();

    /**
     * QPS(也称TPS，Query per second/transaction per second)
     * 并发数/响应时间
     *
     * @return
     */
    int qps();

    void updateQps(int qps);

    boolean isDone();

    int sendCount();

    void cancel();

    boolean isCancelled();

    /**
     * 增加发送数量
     *
     * @param count 增加量
     * @return
     */
    int incSendCount(int count);

}
