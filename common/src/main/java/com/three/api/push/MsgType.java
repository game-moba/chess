package com.three.api.push;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathua on 2017/5/30.
 */
public enum MsgType {
    NOTIFICATION("提醒", 1),//会在通知栏显示
    MESSAGE("消息", 2),//不会在通知栏显示,业务自定义消息
    NOTIFICATION_AND_MESSAGE("提醒+消息", 3);//1+2

    private static Map<Integer, MsgType> map = new HashMap<>();
    static {
        for(MsgType type : values()) {
            map.putIfAbsent(type.value, type);
        }
    }

    public static MsgType valueOf(int id) {
        return map.get(id);
    }

    MsgType(String desc, int value) {
        this.desc = desc;
        this.value = value;
    }

    private final String desc;
    private final int value;

    public String getDesc() {
        return desc;
    }

    public int getValue() {
        return value;
    }
}
