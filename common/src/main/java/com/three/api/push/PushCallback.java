package com.three.api.push;

import com.three.api.router.ClientLocation;

/**
 * Created by mathua on 2017/5/30.
 */
public interface PushCallback {

    default void onResult(PushResult result) {
        switch (result.resultCode) {
            case PushResult.CODE_SUCCESS:
                onSuccess(result.playerId, result.location);
                break;
            case PushResult.CODE_FAILURE:
                onFailure(result.playerId, result.location);
                break;
            case PushResult.CODE_OFFLINE:
                onOffline(result.playerId, result.location);
                break;
            case PushResult.CODE_TIMEOUT:
                onTimeout(result.playerId, result.location);
                break;
        }
    }

    /**
     * 推送成功, 指定用户推送时重写此方法
     *
     * @param playerId   成功的用户, 如果是广播, 值为空
     * @param location 用户所在机器, 如果是广播, 值为空
     */
    default void onSuccess(long playerId, ClientLocation location) {
    }

    /**
     * 推送失败
     *
     * @param playerId   推送用户
     * @param location 用户所在机器
     */
    default void onFailure(long playerId, ClientLocation location) {
    }

    /**
     * 推送用户不在线
     *
     * @param playerId   推送用户
     * @param location 用户所在机器
     */
    default void onOffline(long playerId, ClientLocation location) {
    }

    /**
     * 推送超时
     *
     * @param playerId   推送用户
     * @param location 用户所在机器
     */
    default void onTimeout(long playerId, ClientLocation location) {
    }
}
