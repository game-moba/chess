package com.three.api.push;

import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;

import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by mathua on 2017/5/30.
 */
public class PushContext {
    /**
     * 待推送的消息
     */
    private Packet pushMsg;

    /**
     * 目标用户
     */
    private Long playerId;

    /**
     * 目标用户,批量
     */
    private List<Long> playerIds;

    /**
     * 消息ack模式
     */
    private AckModel ackModel = AckModel.NO_ACK;

    /**
     * 推送成功后的回调
     */
    private PushCallback callback;

    /**
     * 推送超时时间
     */
    private int timeout = 3000;

    //================================broadcast=====================================//

    /**
     * 全网广播在线用户
     */
    private boolean broadcast = false;

    /**
     * 用户标签过滤,目前只有include, 后续会增加exclude
     */
    private Set<String> tags;

    /**
     * 条件表达式, 满足条件的用户会被推送，目前支持的脚本语言为js
     * 可以使用的参数为 playerId,tags,clientVersion,osName,osVersion
     * 比如 :
     * 灰度：playerId % 100 < 20
     * 包含test标签：tags!=null && tags.indexOf("test")!=-1
     * 判断客户端版本号：clientVersion.indexOf("android")!=-1 && clientVersion.replace(/[^\d]/g,"") > 20
     * 等等
     */
    private String condition;

    /**
     * 广播推送的时候可以考虑生成一个ID, 便于控制任务。
     */
    private String taskId;

    public PushContext(Packet packet) {
        this.pushMsg = packet;
    }

    public static PushContext build(Packet packet) {
        return new PushContext(packet);
    }

    public static String genTaskId() {
        return UUID.randomUUID().toString();
    }

    public long getPlayerId() {
        return playerId;
    }

    public PushContext setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public List<Long> getPlayerIds() {
        return playerIds;
    }

    public PushContext setPlayerIds(List<Long> playerIds) {
        this.playerIds = playerIds;
        return this;
    }

    public AckModel getAckModel() {
        return ackModel;
    }

    public PushContext setAckModel(AckModel ackModel) {
        this.ackModel = ackModel;
        return this;
    }

    public PushCallback getCallback() {
        return callback;
    }

    public PushContext setCallback(PushCallback callback) {
        this.callback = callback;
        return this;
    }

    public Packet getPushMsg() {
        return pushMsg;
    }

    public boolean isBroadcast() {
        return broadcast;
    }

    public PushContext setBroadcast(boolean broadcast) {
        this.broadcast = broadcast;
        return this;
    }

    public int getTimeout() {
        return timeout;
    }

    public PushContext setTimeout(int timeout) {
        this.timeout = timeout;
        return this;
    }

    public Set<String> getTags() {
        return tags;
    }

    public PushContext setTags(Set<String> tags) {
        this.tags = tags;
        return this;
    }

    public String getCondition() {
        return condition;
    }

    public PushContext setCondition(String condition) {
        this.condition = condition;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public PushContext setTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }
}
