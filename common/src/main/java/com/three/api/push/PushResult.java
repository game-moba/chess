package com.three.api.push;

import com.three.api.router.ClientLocation;

import java.util.Arrays;

/**
 * Created by mathua on 2017/5/30.
 */
public class PushResult {
    public static final int CODE_SUCCESS = 1;
    public static final int CODE_FAILURE = 2;
    public static final int CODE_OFFLINE = 3;
    public static final int CODE_TIMEOUT = 4;
    public int resultCode;
    public long playerId;
    public Object[] timeLine;
    public ClientLocation location;

    public PushResult(int resultCode) {
        this.resultCode = resultCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    public PushResult setResultCode(int resultCode) {
        this.resultCode = resultCode;
        return this;
    }

    public long getPlayerId() {
        return playerId;
    }

    public PushResult setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public Object[] getTimeLine() {
        return timeLine;
    }

    public PushResult setTimeLine(Object[] timeLine) {
        this.timeLine = timeLine;
        return this;
    }

    public ClientLocation getLocation() {
        return location;
    }

    public PushResult setLocation(ClientLocation location) {
        this.location = location;
        return this;
    }

    public String getResultDesc() {
        switch (resultCode) {
            case CODE_SUCCESS:
                return "success";
            case CODE_FAILURE:
                return "failure";
            case CODE_OFFLINE:
                return "offline";
            case CODE_TIMEOUT:
                return "timeout";
        }
        return Integer.toString(CODE_TIMEOUT);
    }

    @Override
    public String toString() {
        return "PushResult{" +
                "resultCode=" + getResultDesc() +
                ", playerId='" + playerId + '\'' +
                ", timeLine=" + Arrays.toString(timeLine) +
                ", " + location +
                '}';
    }
}