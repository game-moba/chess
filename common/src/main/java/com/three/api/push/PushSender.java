package com.three.api.push;

import com.three.api.protocol.Packet;
import com.three.api.service.Service;
import com.three.api.spi.client.PusherFactory;
import com.three.utils.LogUtils;

import java.util.List;
import java.util.concurrent.FutureTask;

/**
 * Created by mathua on 2017/5/30.
 */
public interface PushSender extends Service {

    /**
     * 创建PushSender实例
     *
     * @return PushSender
     */
    static PushSender get() {
        return PusherFactory.I();
    }

    /**
     * 推送push消息
     *
     * @param context 推送参数
     * @return FutureTask 可用于同步调用
     */
    FutureTask<PushResult> send(PushContext context);

    default FutureTask<PushResult> broadcast(Packet packet) {
        return send(PushContext
                .build(packet)
                .setBroadcast(true)
                .setCallback(new DefaultPushCallback())
        );
    }

    default FutureTask<PushResult> send(Packet packet, List<Long> playerIds) {
        return send(PushContext
                .build(packet)
                .setPlayerId(0)
                .setPlayerIds(playerIds)
                .setCallback(new DefaultPushCallback())
        );
    }

    default FutureTask<PushResult> send(Packet packet, long playerId) {
        return send(PushContext
                .build(packet)
                .setPlayerId(playerId)
                .setCallback(new DefaultPushCallback())
        );
    }

    default FutureTask<PushResult> send(Packet packet, long playerId, PushCallback callback) {
        return send(PushContext
                .build(packet)
                .setPlayerId(playerId)
                .setCallback(callback)
        );
    }

    default FutureTask<PushResult> send(Packet packet, long playerId, AckModel ackModel, PushCallback callback) {
        return send(PushContext
                .build(packet)
                .setAckModel(ackModel)
                .setPlayerId(playerId)
                .setCallback(callback)
        );
    }
}

