package com.three.api.router;

import com.three.api.spi.router.ClientClassifierFactory;

/**
 * 客户端分类
 * Created by mathua on 2017/5/21.
 */
public interface ClientClassifier {
    ClientClassifier I = ClientClassifierFactory.create();

    int getClientType(String osName);
}