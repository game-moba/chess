package com.three.api.router;

import com.three.api.connection.Connection;
import com.three.api.connection.SessionContext;

/**
 * 客户端位置信息
 * Created by mathua on 2017/5/21.
 */
public final class ClientLocation {

    /**
     * 长连接所在的机器IP
     */
    private String host;

    /**
     * 长连接所在的机器端口
     */
    private int port;

    /**
     * 客户端系统类型
     */
    private String osName;

    /**
     * 客户端版本
     */
    private String clientVersion;

    /**
     * 客户端设备ID
     */
    private String deviceId;

    /**
     * 连接ID
     */
    private String connId;

    /**
     * 客户端类型
     */
    private transient int clientType;

    public String getHost() {
        return host;
    }

    public ClientLocation setHost(String host) {
        this.host = host;
        return this;
    }

    public int getPort() {
        return port;
    }

    public ClientLocation setPort(int port) {
        this.port = port;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getConnId() {
        return connId;
    }

    public int getClientType() {
        if (clientType == 0) {
            clientType = ClientClassifier.I.getClientType(osName);
        }
        return clientType;
    }

    public boolean isOnline() {
        return connId != null;
    }

    public boolean isOffline() {
        return connId == null;
    }

    public ClientLocation offline() {
        this.connId = null;
        return this;
    }

    public boolean isThisPC(String host, int port) {
        return this.port == port && this.host.equals(host);
    }

    public String getHostAndPort() {
        return host + ":" + port;
    }

    /**
     * 从连接中获取客户端位置信息
     *
     * @param connection
     * @return
     */
    public static ClientLocation from(Connection connection) {
        SessionContext context = connection.getSessionContext();
        ClientLocation location = new ClientLocation();
        location.osName = context.getOsName();
        location.clientVersion = context.getClientVersion();
        location.deviceId = context.getDeviceId();
        location.connId = connection.getId();
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientLocation location = (ClientLocation) o;

        return clientType == location.clientType;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(clientType);
    }

    @Override
    public String toString() {
        return "ClientLocation{" +
                "host='" + host + ":" + port + "\'" +
                ", osName='" + osName + '\'' +
                ", clientVersion='" + clientVersion + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", connId='" + connId + '\'' +
                '}';
    }
}

