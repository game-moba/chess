package com.three.api.router;

/**
 * Created by mathua on 2017/5/21.
 */
public interface Router<T> {

    T getRouteValue();

    RouterType getRouteType();

    enum RouterType {
        LOCAL,// 本地
        REMOTE// 远程
    }

}
