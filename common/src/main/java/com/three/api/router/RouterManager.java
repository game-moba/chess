package com.three.api.router;

import java.util.Set;

/**
 * Created by mathua on 2017/5/21.
 */
public interface RouterManager<R extends Router> {

    /**
     * 注册路由
     *
     * @param playerId 玩家id
     * @param router 路由信息
     * @return
     */
    R register(long playerId, R router);

    /**
     * 删除路由
     *
     * @param playerId     玩家id
     * @param clientType 客户端类型
     * @return
     */
    boolean unRegister(long playerId, int clientType);

    /**
     * 查询路由
     *
     * @param playerId 玩家id
     * @return
     */
    Set<R> lookupAll(long playerId);

    /**
     * 查询路由
     *
     * @param playerId     玩家id
     * @param clientType 客户端类型
     * @return
     */
    R lookup(long playerId, int clientType);
}
