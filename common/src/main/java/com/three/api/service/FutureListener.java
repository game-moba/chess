package com.three.api.service;

import com.three.exception.ServiceException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 异步处理结果监听器<br>
 *
 * @Author mathua
 * @Date 2017/5/19 16:10
 */
public class FutureListener extends CompletableFuture<Boolean> implements Listener {
    private final Listener l;// 防止Listener被重复执行
    private final AtomicBoolean started;// 防止Listener被重复执行

    public FutureListener(AtomicBoolean started) {
        this.started = started;
        this.l = null;
    }

    public FutureListener(Listener l, AtomicBoolean started) {
        this.l = l;
        this.started = started;
    }

    @Override
    public void onSuccess(Object... args) {
        if (isDone()) return;// 防止Listener被重复执行
        complete(started.get());
        if (l != null) l.onSuccess(args);
    }

    @Override
    public void onFailure(Throwable cause) {
        if (isDone()) return;// 防止Listener被重复执行
        completeExceptionally(cause);
        if (l != null) l.onFailure(cause);
        throw cause instanceof ServiceException
                ? (ServiceException) cause
                : new ServiceException(cause);
    }

    /**
     * 监控服务
     *
     * @param service 被监控的服务
     */
    public void monitor(BaseService service) {
        if (isDone()) return;
        runAsync(() -> {
            try {
                // 在指定的时间内会等待任务执行，超时则抛异常
                this.get(10, TimeUnit.SECONDS);
            } catch (Exception e) {
                this.onFailure(new ServiceException(String.format("service %s monitor timeout", service.getClass().getSimpleName())));
            }
        });
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        throw new UnsupportedOperationException();
    }

}
