package com.three.api.service;

/**
 * 监听者
 *
 * @Author mathua
 * @Date 2017/5/19 16:07
 */
public interface Listener {
    void onSuccess(Object... args);

    void onFailure(Throwable cause);
}
