package com.three.api.service;

import java.util.concurrent.CompletableFuture;

/**
 * 通用服务接口类
 *
 * @Author mathua
 * @Date 2017/5/19 16:03
 */
public interface Service {
    void start(Listener listener);

    void stop(Listener listener);

    /**
     * CompletableFuture在异步任务完成后，需要用到其结果继续操作时，无需等待。<br>
     * 可以直接通过thenAccept、thenApply、thenCompose等方式将前面异步处理的结果交给另外一个异步事件处理线程来处理。
     *
     * @return
     */
    CompletableFuture<Boolean> start();

    CompletableFuture<Boolean> stop();

    boolean syncStart();

    boolean syncStop();

    void init();

    boolean isRunning();
}
