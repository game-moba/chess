package com.three.api.spi;

import java.util.function.Supplier;

/**
 * @Author mathua
 * @Date 2017/5/19 15:50
 */
public interface Factory<T> extends Supplier<T> {
}