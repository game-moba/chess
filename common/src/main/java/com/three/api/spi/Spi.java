package com.three.api.spi;

import java.lang.annotation.*;

/**
 * Single Program Initiation:单个程序启动<br>
 *
 * @Author mathua
 * @Date 2017/5/19 15:36
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Spi {

    /**
     * SPI name
     *
     * @return name
     */
    String value() default "";

    /**
     * 排序顺序
     *
     * @return sortNo
     */
    int order() default 0;

}