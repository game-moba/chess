package com.three.api.spi.client;

import com.three.api.push.PushSender;
import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by mathua on 2017/5/30.
 */
public interface PusherFactory extends Factory<PushSender> {
    static PushSender I() {
        return SpiLoader.load(PusherFactory.class).get();
    }
}
