package com.three.api.spi.common;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by mathua on 2017/5/25.
 */
public interface CacheManagerFactory extends Factory<CacheManager> {
    static CacheManager create() {
        return SpiLoader.load(CacheManagerFactory.class).get();
    }
}
