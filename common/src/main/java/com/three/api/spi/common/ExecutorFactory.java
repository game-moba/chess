package com.three.api.spi.common;

import com.three.api.spi.SpiLoader;

import java.util.concurrent.Executor;

/**
 * Created by mathua on 2017/5/24.
 */
public interface ExecutorFactory {
    String PUSH_CLIENT = "pc";
    String PUSH_TASK = "pt";
    String ACK_TIMER = "at";
    String EVENT_BUS = "eb";
    String MQ = "r";

    Executor get(String name);

    static ExecutorFactory create() {
        return SpiLoader.load(ExecutorFactory.class);
    }
}
