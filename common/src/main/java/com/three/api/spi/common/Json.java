package com.three.api.spi.common;

/**
 * @Author mathua
 * @Date 2017/5/19 15:33
 */
public interface Json {

    Json JSON = JsonFactory.create();

    <T> T fromJson(String json, Class<T> clazz);

    String toJson(Object json);
}
