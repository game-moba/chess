package com.three.api.spi.common;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * @Author mathua
 * @Date 2017/5/19 15:47
 */
public interface JsonFactory extends Factory<Json> {
    static Json create() {
        return SpiLoader.load(JsonFactory.class).get();
    }
}