package com.three.api.spi.common;

/**
 * 消息队列客户端
 * Created by mathua on 2017/5/25.
 */
public interface MQClient {

    /**
     * 订阅消息
     *
     * @param topic    订阅的话题
     * @param receiver 消息接收者
     */
    void subscribe(String topic, MQMessageReceiver receiver);

    /**
     * 发布消息
     *
     * @param topic   要发布的话题
     * @param message 要发布的消息
     */
    void publish(String topic, Object message);
}

