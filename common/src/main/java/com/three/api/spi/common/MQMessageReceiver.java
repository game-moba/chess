package com.three.api.spi.common;

/**
 * 消息队列信息接收者
 * Created by mathua on 2017/5/25.
 */
public interface MQMessageReceiver {
    /**
     * 接收消息
     *
     * @param topic   接收的话题
     * @param message 接受到的消息
     */
    void receive(String topic, Object message);
}