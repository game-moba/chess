package com.three.api.spi.common;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;
import com.three.api.srd.ServiceDiscovery;

/**
 * Created by mathua on 2017/5/30.
 */
public interface ServiceDiscoveryFactory extends Factory<ServiceDiscovery> {
    static ServiceDiscovery create() {
        return SpiLoader.load(ServiceDiscoveryFactory.class).get();
    }
}
