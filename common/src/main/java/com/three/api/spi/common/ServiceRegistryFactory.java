package com.three.api.spi.common;

import com.three.api.srd.ServiceRegistry;
import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * @Author mathua
 * @Date 2017/5/23 19:58
 */
public interface ServiceRegistryFactory extends Factory<ServiceRegistry> {
    static ServiceRegistry create() {
        return SpiLoader.load(ServiceRegistryFactory.class).get();
    }
}
