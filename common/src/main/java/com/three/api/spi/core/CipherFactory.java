package com.three.api.spi.core;

import com.three.api.connection.Cipher;
import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by mathua on 2017/5/22.
 */
public interface CipherFactory extends Factory<Cipher> {
    static Cipher create() {
        return SpiLoader.load(CipherFactory.class).get();
    }
}
