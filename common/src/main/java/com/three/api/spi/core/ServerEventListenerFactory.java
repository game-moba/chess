package com.three.api.spi.core;

import com.three.api.ServerEventListener;
import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by mathua on 2017/5/24.
 */
public interface ServerEventListenerFactory extends Factory<ServerEventListener> {
    static ServerEventListener create() {
        return SpiLoader.load(ServerEventListenerFactory.class).get();
    }
}
