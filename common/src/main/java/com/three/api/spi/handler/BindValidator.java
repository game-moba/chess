package com.three.api.spi.handler;

/**
 * 绑定验证
 * Created by mathua on 2017/5/28.
 */
public interface BindValidator {
    /**
     * 验证玩家
     *
     * @param playerId 玩家id
     * @return 是否验证通过
     */
    boolean validate(long playerId);
}
