package com.three.api.spi.handler;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by mathua on 2017/5/28.
 */
public interface BindValidatorFactory extends Factory<BindValidator> {
    static BindValidator create() {
        return SpiLoader.load(BindValidatorFactory.class).get();
    }
}

