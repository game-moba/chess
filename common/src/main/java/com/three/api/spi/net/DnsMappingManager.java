package com.three.api.spi.net;

import com.three.api.service.Service;
import com.three.api.spi.SpiLoader;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface DnsMappingManager extends Service {

    static DnsMappingManager create() {
        return SpiLoader.load(DnsMappingManager.class);
    }

    /**
     * 查阅域名映射信息
     * @param origin
     * @return
     */
    DnsMapping lookup(String origin);
}

