package com.three.api.spi.push;

import com.three.api.common.Condition;
import com.three.api.protocol.Packet;

/**
 * 推送的消息通用接口定义
 * Created by Mathua on 2017/5/25.
 */
public interface IPushMessage {

    /**
     * 是否广播
     *
     * @return
     */
    boolean isBroadcast();

    long getPlayerId();

    int getClientType();

    Packet getPushPacket();

    /**
     * 是否需要确认
     *
     * @return
     */
    boolean isNeedAck();

    byte getFlags();

    int getTimeoutMills();

    default String getTaskId() {
        return null;
    }

    default Condition getCondition() {
        return null;
    }

    default void finalized() {

    }

}
