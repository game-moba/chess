package com.three.api.spi.push;

/**
 * 消息推送者接口类
 * Created by Mathua on 2017/5/25.
 */
public interface MessagePusher {
    /**
     * 推送消息
     *
     * @param message
     */
    void push(IPushMessage message);
}