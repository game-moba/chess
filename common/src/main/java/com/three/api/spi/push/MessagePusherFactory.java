package com.three.api.spi.push;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface MessagePusherFactory extends Factory<MessagePusher> {

    static MessagePusher create() {
        return SpiLoader.load(MessagePusherFactory.class).get();
    }
}
