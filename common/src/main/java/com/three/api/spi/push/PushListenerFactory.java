package com.three.api.spi.push;

import com.three.api.spi.Factory;
import com.three.api.spi.SpiLoader;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface PushListenerFactory<M extends IPushMessage> extends Factory<PushListener<M>> {

    @SuppressWarnings("unchecked")
    static <M extends IPushMessage> PushListener<M> create() {
        return (PushListener<M>) SpiLoader.load(PushListenerFactory.class).get();
    }
}

