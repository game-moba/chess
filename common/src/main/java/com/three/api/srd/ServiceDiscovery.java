package com.three.api.srd;

import com.three.api.service.Service;

import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/23 19:46
 */
public interface ServiceDiscovery extends Service {

    List<ServiceNode> lookup(String path);

    /**
     * 订阅
     *
     * @param path
     * @param listener
     */
    void subscribe(String path, ServiceListener listener);

    /**
     * 退订
     *
     * @param path
     * @param listener
     */
    void unsubscribe(String path, ServiceListener listener);
}
