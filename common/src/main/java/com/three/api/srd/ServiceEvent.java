package com.three.api.srd;

import com.three.api.event.Event;

/**
 * @Author mathua
 * @Date 2017/5/23 19:44
 */
public interface ServiceEvent extends Event {
}
