package com.three.api.srd;

/**
 * 服务监听接口类
 *
 * @Author mathua
 * @Date 2017/5/23 19:43
 */
public interface ServiceListener {

    /**
     * 加入一个服务的回调处理
     *
     * @param path 服务路径
     * @param node 服务节点
     */
    void onServiceAdded(String path, ServiceNode node);

    /**
     * 更新一个服务的回调处理
     *
     * @param path 服务路径
     * @param node 服务节点
     */
    void onServiceUpdated(String path, ServiceNode node);

    /**
     * 移除一个服务的回调处理
     *
     * @param path 服务路径
     * @param node 服务节点
     */
    void onServiceRemoved(String path, ServiceNode node);

}
