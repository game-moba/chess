package com.three.api.srd;

/**
 * 服务名称类
 *
 * @Author mathua
 * @Date 2017/5/23 19:43
 */
public interface ServiceNames {
    String CONN_SERVER = "/cluster/cs";
    String WS_SERVER = "/cluster/ws";
    String GATEWAY_SERVER = "/cluster/gs";
    String DNS_MAPPING = "/dns/mapping";

    String ATTR_PUBLIC_IP = "public_ip";

}
