package com.three.api.srd;

/**
 * 服务节点
 *
 * @Author mathua
 * @Date 2017/5/23 19:40
 */
public interface ServiceNode {
    String serviceName();// 服务名字

    String nodeId();// 节点id

    String getHost();

    int getPort();

    default String getAttr(String name) {
        return null;
    }

    default boolean isPersistent() {
        return false;
    }

    default String hostAndPort() {
        return getHost() + ":" + getPort();
    }

    default String nodePath() {
        return serviceName() + '/' + nodeId();
    }
}
