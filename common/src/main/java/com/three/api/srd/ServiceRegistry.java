package com.three.api.srd;

import com.three.api.service.Service;

/**
 * 服务注册处接口类
 *
 * @Author mathua
 * @Date 2017/5/23 19:41
 */
public interface ServiceRegistry extends Service {

    /**
     * 注册一个服务器节点
     *
     * @param node
     */
    void register(ServiceNode node);

    /**
     * 注销一个服务器节点
     *
     * @param node
     */
    void deregister(ServiceNode node);
}
