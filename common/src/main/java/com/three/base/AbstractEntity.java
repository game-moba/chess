package com.three.base;

import com.three.utils.BeanUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * 实体标识抽象类
 *
 * @Author mathua
 * @Date 2017/5/18 11:46
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 8882145540383345037L;

    /**
     * 返回实体id
     *
     * @return
     */
    public abstract long getEntityId();

    /**
     * 获取业务主键。业务主键是判断相同类型的两个实体在业务上的等价性的依据。如果相同类型的两个
     * 实体的业务主键相同，则认为两个实体是相同的，代表同一个实体。
     * <p>业务主键由实体的一个或多个属性组成。
     * @return 组成业务主键的属性的数组。
     */
    public String[] businessKeys() {
        return new String[] {"id"};
    }

    /**
     * 依据业务主键获取哈希值。用于判定两个实体是否等价。
     * 等价的两个实体的hashCode相同，不等价的两个实体hashCode不同。
     * @return 实体的哈希值
     */
    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder(13, 37);
        if (businessKeys() == null || businessKeys().length == 0) {
            return builder.append(getEntityId()).toHashCode();
        }
        BeanUtils thisBeanUtils = new BeanUtils(this);
        for (String businessKey : businessKeys()) {
            builder = builder.append(thisBeanUtils.getPropValue(businessKey));
        }
        return builder.toHashCode();
    }

    /**
     * 依据业务主键判断两个实体是否等价。
     * @param other 另一个实体
     * @return 如果本实体和other等价则返回true,否则返回false
     */
    @Override
    public boolean equals(Object other) {
        return EntityEqualsBuilder.isEquals(this, other);
    }

    private static class EntityEqualsBuilder {
        public static boolean isEquals(AbstractEntity entity, Object other) {
            if (entity == other) {
                return true;
            }
            if (other == null) {
                return false;
            }
            if (!(entity.getClass().isAssignableFrom(other.getClass()))) {
                return false;
            }
            BeanUtils thisBeanUtils = new BeanUtils(entity);
            BeanUtils thatBeanUtils = new BeanUtils(other);
            EqualsBuilder builder = new EqualsBuilder();
            if (entity.businessKeys() == null || entity.businessKeys().length == 0) {
                return builder.append(entity.getEntityId(), ((AbstractEntity)other).getEntityId()).isEquals();
            }
            for (String businessKey : entity.businessKeys()) {
                builder.append(thisBeanUtils.getPropValue(businessKey), thatBeanUtils.getPropValue(businessKey));
            }
            return builder.isEquals();
        }
    }
}