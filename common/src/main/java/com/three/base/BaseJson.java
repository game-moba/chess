package com.three.base;

import com.three.utils.JsonUtils;

/**
 * Created by ziqingwang on 2017/5/18 0018.
 */
public interface BaseJson {
    default String toJsonString() {
        return JsonUtils.toJson(this);
    }
}