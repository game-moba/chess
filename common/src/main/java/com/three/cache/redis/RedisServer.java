package com.three.cache.redis;

import com.three.tools.config.data.RedisNode;
import redis.clients.jedis.HostAndPort;

/**
 * redis 相关的配置信息
 * Created by mathua on 2017/5/29.
 */
public class RedisServer extends RedisNode {

    public RedisServer(String ip, int port) {
        super(ip, port);
    }

    public HostAndPort convert() {
        return new HostAndPort(host, port);
    }

}
