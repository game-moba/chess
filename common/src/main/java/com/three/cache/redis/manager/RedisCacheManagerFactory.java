package com.three.cache.redis.manager;

import com.three.api.spi.Spi;
import com.three.api.spi.common.CacheManager;
import com.three.api.spi.common.CacheManagerFactory;

/**
 * Created by mathua on 2017/5/29.
 */
@Spi(order = 1)
public final class RedisCacheManagerFactory implements CacheManagerFactory {
    @Override
    public CacheManager get() {
        return RedisManager.I;
    }
}