package com.three.cache.redis.manager;

import org.springframework.data.redis.connection.RedisServer;

import java.util.List;

/**
 * redis集群管理者
 * Created by mathua on 2017/5/29.
 */
public interface RedisClusterManager {

    void init();

    List<RedisServer> getServers();
}
