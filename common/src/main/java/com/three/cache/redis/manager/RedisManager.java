package com.three.cache.redis.manager;

import com.google.common.collect.Lists;
import com.three.api.spi.common.CacheManager;
import com.three.cache.redis.connection.RedisConnectionFactory;
import com.three.config.common.IConfig;
import com.three.tools.thread.pool.ThreadPoolManager;
import com.three.utils.JsonUtils;
import com.three.utils.LogUtils;
import redis.clients.jedis.*;

import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * redis 对外封装接口
 * Created by mathua on 2017/5/29.
 */
public final class RedisManager implements CacheManager {
    public static final RedisManager I = new RedisManager();

    private final RedisConnectionFactory factory = new RedisConnectionFactory();

    private static final String EX = "EX";

    private static final String NX = "NX";

    public void init() {
        LogUtils.CACHE.info("begin init redis...");
        factory.setPassword(IConfig.chess.redis.password);
        factory.setPoolConfig(IConfig.chess.redis.getPoolConfig(JedisPoolConfig.class));
        factory.setRedisServers(IConfig.chess.redis.nodes);
        factory.setCluster(IConfig.chess.redis.isCluster());
        factory.init();
        test();
        LogUtils.CACHE.info("init redis success...");
    }

    private <R> R call(Function<JedisCommands, R> function, R d) {
        if (factory.isCluster()) {
            try {
                return function.apply(factory.getClusterConnection());
            } catch (Exception e) {
                LogUtils.CACHE.error("redis ex", e);
            }
        } else {
            try (Jedis jedis = factory.getJedisConnection()) {
                return function.apply(jedis);
            } catch (Exception e) {
                LogUtils.CACHE.error("redis ex", e);
            }
        }
        return d;
    }

    private void call(Consumer<JedisCommands> consumer) {
        if (factory.isCluster()) {
            try {
                consumer.accept(factory.getClusterConnection());
            } catch (Exception e) {
                LogUtils.CACHE.error("redis ex", e);
            }
        } else {
            try (Jedis jedis = factory.getJedisConnection()) {
                consumer.accept(jedis);
            } catch (Exception e) {
                LogUtils.CACHE.error("redis ex", e);
            }
        }
    }

    /*********************************    sort    ****************************************/
    /**
     * 获取排序列表(倒序)
     * 0-a 倒序
     * -a -> 0 正序
     *
     * @param key
     * @return
     */
    public Set<Tuple> zrevrangeWithScores(String key, long start, long end) {
        return call(jedis -> jedis.zrevrangeWithScores(key, start, end), Collections.EMPTY_SET);
    }

    /**
     * 添加排序列表
     *
     * @param key
     * @return
     */
    public double zincrby(String key, double score, String member) {
        return call(jedis -> jedis.zincrby(key, score, member), 0.0);
    }

    public long incr(String key) {
        return call(jedis -> jedis.incr(key), 0L);
    }

    public long incr(String key, long start) {
        return call(jedis -> jedis.incr(key), start);
    }

    /**
     * 原子增量
     *
     * @param key
     * @param delt
     * @return
     */
    public long incrBy(String key, long delt) {
        return call(jedis -> jedis.incrBy(key, delt), 0L);
    }

    /**
     * 原子增量 加设置失效时间 ,如果不存在用def的值保存
     * @param key
     * @param addvalue
     * @param def
     * @param expire
     * @return
     */
    public long incrBy(String key, long addvalue, long def, int expire) {
        if (1L == call(jedis ->jedis.expire(key, expire), 0L)) {
            return call(jedis -> jedis.incrBy(key, addvalue), 0L);
        } else {
            return call(jedis -> jedis.incrBy(key, def),0L);
        }
    }

    /**
     * 原子增量 如果不存在用def的值保存
     *
     * @param key
     * @param addvalue
     * @return
     */
    public long incrBy(String key, long addvalue, long def) {
        if (true == call(jedis ->jedis.exists(key),false)) {
            return call(jedis -> jedis.incrBy(key, addvalue), 0L);
        } else {
            return call(jedis -> jedis.incrBy(key, def), 0L);
        }
    }

    /********************* k v redis start ********************************/
    public String getSet(String key, String expireStr) {
        return call(jedis -> jedis.getSet(key, expireStr), null);
    }

    /**
     * @param key
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String key, Class<T> clazz) {
        String value = call(jedis -> jedis.get(key), null);
        if (value == null) return null;
        if (clazz == String.class) return (T) value;
        return JsonUtils.fromJson(value, clazz);
    }

    /**
     * get
     *
     * @param key
     * @return
     */
    public String get(String key) {
        return call(jedis -> jedis.get(key), null);
    }

    public <T> T get(String key, Type type) {
        try {
            String json = get(key);
            if(json != null) {
                return JsonUtils.fromJson(json, type);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 设置 无超时缓存
     */
    public void set(String key, String value) {
        set(key, value, 0);
    }

    public void set(String key, Object value) {
        set(key, value, 0);
    }

    public void set(String key, Object value, int seconds) {
        set(key, JsonUtils.toJson(value), seconds);
    }

    /**
     * 超时set 已存在值不会覆盖
     *
     * @param key
     * @param data
     * @param time 超时时间
     * @return
     */
    public String setExpx(String key, String data, int time) {
        return call(jedis -> jedis.set(key, data, NX, EX, time), null);
    }

    /**
     * 超时set 已存在值会覆盖
     *
     * @param key
     * @param data
     * @param time 超时时间
     * @return
     */
    public String setEx4Str(String key, String data, int time) {
        return call(jedis -> jedis.setex(key, time, data), null);
    }

    /**
     * 设置超时缓存
     */
    public void setex(String key, String value, int seconds) {
        call(jedis -> jedis.setex(key, seconds, value));
    }

    public void setex(String key, Object value, int seconds) {
        setex(key,JsonUtils.toJson(value), seconds);
    }

    /**
     * 设置值并设置超时时间
     *
     * @param key
     * @param data
     * @param time 超时时间
     * @return
     */
    public boolean setnx(String key, String data, int time) {
        Long setnx = call(jedis -> jedis.setnx(key, data), 0L);
        if (setnx == 1) {
            call(jedis -> jedis.expire(key, time));
            return true;
        }
        return setnx == 1 ? true : false;
    }

    /**
     * @param key
     * @param value
     * @param seconds
     */
    public void set(String key, String value, int seconds) {
        call(jedis -> {
            jedis.set(key, value);
            if (seconds > 0) {
                jedis.expire(key, seconds);
            }
        });
    }

    /**
     * del
     *
     * @param key
     * @return
     */
    public boolean del(String key) {
        long res = call(jedis -> jedis.del(key), 0L);
        return res == 1 ? true : false;
    }

    /**
     * 为键设置超时时间
     *
     * @param key
     * @param seconds
     * @return
     */
    public long expire(String key, int seconds) {
        return call(jedis->jedis.expire(key, seconds),0L);
    }

    /**
     * key  是否存在
     *
     * @param key
     * @return
     */
    public boolean exists(String key) {
        return call(jedis -> jedis.exists(key), false);
    }

    /********************* k v redis end ********************************/

    /*********************
     * hash redis start
     ********************************/
    /**
     * 存HASH
     *
     * @param key
     * @param field
     * @param value
     * @return
     */
    public void hset(String key, String field, String value) {
        call(jedis -> jedis.hset(key, field, value));
    }

    public void hset(String key, String field, Object value) {
        hset(key, field, JsonUtils.toJson(value));
    }

    @SuppressWarnings("unchecked")
    public <T> T hget(String key, String field, Class<T> clazz) {
        String value = call(jedis -> jedis.hget(key, field), null);
        if (value == null) return null;
        if (clazz == String.class) return (T) value;
        return JsonUtils.fromJson(value, clazz);
    }

    /**
     * 获取HASH
     *
     * @param key
     * @param field
     * @return
     */
    public String hget(String key, String field) {
        return call(jedis -> jedis.hget(key, field), null);
    }

    /**
     * 删除hash
     *
     * @param key
     * @param field
     * @return
     */
    public long hdel(String key, String field) {
        return call(jedis -> jedis.hdel(key, field), 0L);
    }

    /**
     * 批量获取HASH
     *
     * @param key
     * @return
     */
    public Map<String, String> hgetAll(String key) {
        return call(jedis -> jedis.hgetAll(key), Collections.<String, String>emptyMap());
    }

    public <T> Map<String, T> hgetAll(String key, Class<T> clazz) {
        Map<String, String> result = hgetAll(key);
        if (result.isEmpty()) return Collections.emptyMap();
        Map<String, T> newMap = new HashMap<>(result.size());
        result.forEach((k, v) -> newMap.put(k, JsonUtils.fromJson(v, clazz)));
        return newMap;
    }

    /**
     * 返回 key 指定的哈希集中所有字段的名字。
     *
     * @param key
     * @return
     */
    public Set<String> hkeys(String key) {
        return call(jedis -> jedis.hkeys(key), Collections.<String>emptySet());
    }

    /**
     * 返回 key 指定的哈希集中指定字段的值
     *
     * @param fields
     * @param clazz
     * @return
     */
    public <T> List<T> hmget(String key, Class<T> clazz, String... fields) {
        return call(jedis -> jedis.hmget(key, fields), Collections.<String>emptyList())
                .stream()
                .map(s -> JsonUtils.fromJson(s, clazz))
                .collect(Collectors.toList());

    }

    /**
     * 设置 key 指定的哈希集中指定字段的值。该命令将重写所有在哈希集中存在的字段。如果 key 指定的哈希集不存在，会创建一个新的哈希集并与 key
     * 关联
     *
     * @param hash
     * @param seconds
     */
    public void hmset(String key, Map<String, String> hash, int seconds) {
        call(jedis -> {
            jedis.hmset(key, hash);
            if (seconds > 0) {
                jedis.expire(key, seconds);
            }
        });
    }

    /**
     * 批量存HASH
     *
     * @param key
     * @param hash
     * @return
     */
    public void hmset(String key, Map<String, String> hash) {
        hmset(key, hash, 0);
    }

    /**
     * 原子增量
     *
     * @param key
     * @param field
     * @param value
     * @return
     */
    public long hincrBy(String key, String field, long value) {
        return call(jedis -> jedis.hincrBy(key, field, value), 0L);
    }

    /********************* hash redis end ********************************/

    /********************* list redis start ********************************/
    /**
     * 从队列的左边入队
     */
    public void lpush(String key, String value) {
        call(jedis -> jedis.lpush(key, value));
    }

    public void lpush(String key, Object value) {
        lpush(key, JsonUtils.toJson(value));
    }

    /**
     * 入队列
     *
     * @param key
     * @return
     */
    public long lpush(String key, String... val) {
        return call(jedis -> jedis.lpush(key, val), 0L);
    }

    /**
     * 从队列的右边入队
     */
    public void rpush(String key, String value) {
        call(jedis -> jedis.rpush(key, value));
    }

    public void rpush(String key, Object value) {
        rpush(key, JsonUtils.toJson(value));
    }

    public long rpush(String key, String... val) {
        return call(jedis -> jedis.rpush(key, val), 0L);
    }

    /**
     * 移除并且返回 key 对应的 list 的第一个元素
     */
    @SuppressWarnings("unchecked")
    public <T> T lpop(String key, Class<T> clazz) {
        String value = call(jedis -> jedis.lpop(key), null);
        if (value == null) return null;
        if (clazz == String.class) return (T) value;
        return JsonUtils.fromJson(value, clazz);
    }

    /**
     * 从队列的右边出队一个元素
     */
    @SuppressWarnings("unchecked")
    public <T> T rpop(String key, Class<T> clazz) {
        String value = call(jedis -> jedis.rpop(key), null);
        if (value == null) return null;
        if (clazz == String.class) return (T) value;
        return JsonUtils.fromJson(value, clazz);
    }

    /**
     * 从列表中获取指定返回的元素 start 和 end
     * 偏移量都是基于0的下标，即list的第一个元素下标是0（list的表头），第二个元素下标是1，以此类推。
     * 偏移量也可以是负数，表示偏移量是从list尾部开始计数。 例如， -1 表示列表的最后一个元素，-2 是倒数第二个，以此类推。
     */
    public <T> List<T> lrange(String key, int start, int end, Class<T> clazz) {
        return call(jedis -> jedis.lrange(key, start, end), Collections.<String>emptyList())
                .stream()
                .map(s -> JsonUtils.fromJson(s, clazz))
                .collect(Collectors.toList());
    }

    /**
     * 返回存储在 key 里的list的长度。 如果 key 不存在，那么就被看作是空list，并且返回长度为 0。 当存储在 key
     * 里的值不是一个list的话，会返回error。
     */
    public long llen(String key) {
        return call(jedis -> jedis.llen(key), 0L);
    }

    /**
     * 移除表中所有与 value 相等的值
     *
     * @param key
     * @param value
     */
    public void lRem(String key, String value) {
        call(jedis -> jedis.lrem(key, 0, value));
    }

    public String ltrim(String key, long start, long end) {
        return call(jedis -> jedis.ltrim(key, start, end), null);
    }

    /********************* list redis end ********************************/

    /*********************
     * mq redis start
     ********************************/


    public void publish(String channel, Object message) {
        String msg = message instanceof String ? (String) message : JsonUtils.toJson(message);
        call(jedis -> {
            if (jedis instanceof MultiKeyCommands) {
                ((MultiKeyCommands) jedis).publish(channel, msg);
            } else if (jedis instanceof MultiKeyJedisClusterCommands) {
                ((MultiKeyJedisClusterCommands) jedis).publish(channel, msg);
            }
        });
    }

    public void subscribe(final JedisPubSub pubsub, final String channel) {
        ThreadPoolManager.I.newThread(channel,
                () -> call(jedis -> {
                    if (jedis instanceof MultiKeyCommands) {
                        ((MultiKeyCommands) jedis).subscribe(pubsub, channel);
                    } else if (jedis instanceof MultiKeyJedisClusterCommands) {
                        ((MultiKeyJedisClusterCommands) jedis).subscribe(pubsub, channel);
                    }
                })
        ).start();
    }

    /*********************
     * set redis start
     ********************************/
    /**
     * 存set
     * @param key
     * @param value
     */
    public void sAdd(String key, String value) {
        call(jedis -> jedis.sadd(key, value));
    }

    /**
     * 存set 多个
     *
     * @param key
     * @return
     */
    public long sAdds(String key, String[] value) {
        if (value == null || value.length == 0) {
            return 0;
        }
        return call(jedis -> jedis.sadd(key, value), 0L);
    }

    /**
     * 获取set大小
     * @param key
     * @return
     */
    public long sCard(String key) {
        return call(jedis -> jedis.scard(key), 0L);
    }

    public void sRem(String key, String value) {
        call(jedis -> jedis.srem(key, value));
    }

    /**
     * 默认使用每页10个
     *
     * @param key
     * @param clazz
     * @return
     */
    public <T> List<T> sScan(String key, Class<T> clazz, int start) {
        List<String> list = call(jedis -> jedis.sscan(key, Integer.toString(start), new ScanParams().count(10)).getResult(), null);
        return toList(list, clazz);
    }

    /**
     * 返回集合中的所有成员
     *
     * @param key
     * @return
     */
    public Set<String> sMembers(String key) {
        return call(jedis -> jedis.smembers(key), Collections.EMPTY_SET);
    }

    /**
     * 是否集合成员
     *
     * @param key
     * @return
     */
    public boolean sIsMember(String key, String member) {
        return call(jedis -> jedis.sismember(key, member),false);
    }

    /*********************
     * sorted set
     ********************************/
    /**
     * @param key
     * @param value
     */
    public void zAdd(String key, String value) {
        call(jedis -> jedis.zadd(key, 0, value));
    }

    /**
     * @param key
     * @return
     */
    public Long zCard(String key) {
        return call(jedis -> jedis.zcard(key), 0L);
    }

    public void zRem(String key, String value) {
        call(jedis -> jedis.zrem(key, value));
    }

    /**
     * 从列表中获取指定返回的元素 start 和 end
     * 偏移量都是基于0的下标，即list的第一个元素下标是0（list的表头），第二个元素下标是1，以此类推。
     * 偏移量也可以是负数，表示偏移量是从list尾部开始计数。 例如， -1 表示列表的最后一个元素，-2 是倒数第二个，以此类推。
     */
    public <T> List<T> zrange(String key, int start, int end, Class<T> clazz) {
        Set<String> value = call(jedis -> jedis.zrange(key, start, end), null);
        return toList(value, clazz);
    }

    /**
     * 获取score
     *
     * @param key
     * @return
     */
    public Double zscore(String key, String member) {
        return call(jedis -> jedis.zscore(key, member), 0.0);
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> toList(Collection<String> value, Class<T> clazz) {
        if (value != null) {
            if (clazz == String.class) {
                return (List<T>) new ArrayList<>(value);
            }
            List<T> newValue = Lists.newArrayList();
            for (String temp : value) {
                newValue.add(JsonUtils.fromJson(temp, clazz));
            }
            return newValue;
        }
        return null;
    }

    public void destroy() {
        if (factory != null) factory.destroy();
    }

    public void test() {
        if (factory.isCluster()) {
            JedisCluster cluster = factory.getClusterConnection();
            if (cluster == null) throw new RuntimeException("init redis cluster error.");
        } else {
            Jedis jedis = factory.getJedisConnection();
            if (jedis == null) throw new RuntimeException("init redis error, can not get connection.");
            jedis.close();
        }
    }
}


