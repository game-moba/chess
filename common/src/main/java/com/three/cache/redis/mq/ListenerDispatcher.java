package com.three.cache.redis.mq;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.three.api.spi.common.MQClient;
import com.three.api.spi.common.MQMessageReceiver;
import com.three.cache.redis.manager.RedisManager;
import com.three.tools.thread.pool.ThreadPoolManager;
import com.three.utils.LogUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

/**
 * 监听器调度员
 * Created by mathua on 2017/5/29.
 */
public final class ListenerDispatcher implements MQClient {

    private static ListenerDispatcher I;

    private final Map<String, List<MQMessageReceiver>> subscribes = Maps.newTreeMap();

    private final Executor executor = ThreadPoolManager.I.getRedisExecutor();

    private final Subscriber subscriber = new Subscriber();

    public static ListenerDispatcher I() {
        if (I == null) {
            synchronized (ListenerDispatcher.class) {
                if (I == null) {
                    I = new ListenerDispatcher();
                }
            }
        }
        return I;
    }

    private ListenerDispatcher() {
    }

    public void onMessage(final String channel, final String message) {
        List<MQMessageReceiver> listeners = subscribes.get(channel);
        if (listeners == null) {
            LogUtils.CACHE.info("cannot find listener:%s,%s", channel, message);
            return;
        }
        for (final MQMessageReceiver listener : listeners) {
            executor.execute(() -> listener.receive(channel, message));
        }
    }

    public void subscribe(String channel, MQMessageReceiver listener) {
        subscribes.computeIfAbsent(channel, k -> Lists.newArrayList()).add(listener);
        RedisManager.I.subscribe(subscriber, channel);
    }

    /**
     * 发布消息
     * @param topic   要发布的话题
     * @param message 要发布的消息
     */
    @Override
    public void publish(String topic, Object message) {
        RedisManager.I.publish(topic, message);
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }
}
