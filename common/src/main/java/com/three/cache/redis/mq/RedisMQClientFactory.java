package com.three.cache.redis.mq;

import com.three.api.spi.Spi;
import com.three.api.spi.common.MQClient;
import com.three.api.spi.common.MQClientFactory;

/**
 * Created by mathua on 2017/5/29.
 */
@Spi(order = 1)
public final class RedisMQClientFactory implements MQClientFactory {

    @Override
    public MQClient get() {
        return ListenerDispatcher.I();
    }
}