package com.three.cache.redis.mq;

import com.three.utils.JsonUtils;
import com.three.utils.LogUtils;
import redis.clients.jedis.JedisPubSub;

/**
 * 订阅者
 * Created by mathua on 2017/5/29.
 */
public final class Subscriber extends JedisPubSub {

    @Override
    public void onMessage(String channel, String message) {
        LogUtils.CACHE.info("onMessage:{},{}", channel, message);
        ListenerDispatcher.I().onMessage(channel, message);
        super.onMessage(channel, message);
    }

    @Override
    public void onPMessage(String pattern, String channel, String message) {
        LogUtils.CACHE.info("onPMessage:{},{},{}", pattern, channel, message);
        super.onPMessage(pattern, channel, message);
    }

    @Override
    public void onPSubscribe(String pattern, int subscribedChannels) {
        LogUtils.CACHE.info("onPSubscribe:{},{}", pattern, subscribedChannels);
        super.onPSubscribe(pattern, subscribedChannels);
    }

    @Override
    public void onPUnsubscribe(String pattern, int subscribedChannels) {
        LogUtils.CACHE.info("onPUnsubscribe:{},{}", pattern, subscribedChannels);
        super.onPUnsubscribe(pattern, subscribedChannels);
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        LogUtils.CACHE.info("onSubscribe:{},{}", channel, subscribedChannels);
        super.onSubscribe(channel, subscribedChannels);
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        LogUtils.CACHE.info("onUnsubscribe:{},{}", channel, subscribedChannels);
        super.onUnsubscribe(channel, subscribedChannels);
    }


    /**
     * 退订
     */
    @Override
    public void unsubscribe() {
        LogUtils.CACHE.info("unsubscribe");
        super.unsubscribe();
    }

    @Override
    public void unsubscribe(String... channels) {
        LogUtils.CACHE.info("unsubscribe:{}", JsonUtils.toJson(channels));
        super.unsubscribe(channels);
    }

}
