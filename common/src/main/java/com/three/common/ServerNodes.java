package com.three.common;

import com.three.api.srd.CommonServiceNode;
import com.three.api.srd.ServiceNames;
import com.three.api.srd.ServiceNode;
import com.three.config.common.IConfig;
import com.three.tools.Utils;
import com.three.tools.config.ConfigManager;

import static com.three.api.srd.ServiceNames.ATTR_PUBLIC_IP;

/**
 * Created by mathua on 2017/5/25.
 */
public class ServerNodes {
    public static final ServiceNode CS = cs();// 用户连接服
    public static final ServiceNode GS = gs();// 网关服
    public static final ServiceNode WS = ws();// 世界服


    private static ServiceNode cs() {
        CommonServiceNode node = new CommonServiceNode();
        node.setHost(Utils.getZkServerIp());
        node.setPort(IConfig.chess.net.connect_server_port);
        node.setPersistent(false);
        node.setServiceName(ServiceNames.CONN_SERVER);
        node.addAttr(ATTR_PUBLIC_IP, Utils.getZkServerIp());
        return node;
    }

    private static ServiceNode ws() {
        CommonServiceNode node = new CommonServiceNode();
        node.setHost(Utils.getLocalIp());
        node.setPort(IConfig.chess.net.ws_server_port);
        node.setPersistent(false);
        node.setServiceName(ServiceNames.WS_SERVER);
        node.addAttr(ATTR_PUBLIC_IP, ConfigManager.I.getPublicIp());
        return node;
    }

    private static ServiceNode gs() {
        CommonServiceNode node = new CommonServiceNode();
        node.setHost(Utils.getLocalIp());
        node.setPort(IConfig.chess.net.gateway_server_port);
        node.setPersistent(false);
        node.setServiceName(ServiceNames.GATEWAY_SERVER);
        return node;
    }
}
