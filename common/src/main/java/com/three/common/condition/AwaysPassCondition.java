package com.three.common.condition;

import com.three.api.common.Condition;

import java.util.Map;

/**
 * 条件:总是需要密码
 * Created by Mathua on 2017/5/26.
 */
public final class AwaysPassCondition implements Condition {
    public static final Condition I = new AwaysPassCondition();

    @Override
    public boolean test(Map<String, Object> env) {
        return true;
    }
}

