package com.three.common.handler;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.MessageHandler;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.tools.common.Profiler;

/**
 * 消息处理基础类
 * Created by Mathua on 2017/5/26.
 */
public abstract class BaseMessageHandler<T extends BaseMessage> implements MessageHandler {

    /**
     * 数据包解码
     *
     * @param packet     要解码的数据包
     * @param connection 数据包来源
     * @return 返回数据包对应的消息
     */
    public abstract T decode(Packet packet, Connection connection);

    public abstract void handle(T message);

    public void handle(Packet packet, Connection connection) throws InvalidProtocolBufferException {
        Profiler.enter("time cost on [message decode]");
        T t = decode(packet, connection);
        if (t != null) t.decodeBody();
        Profiler.release();// 数据包解码计时

        if (t != null) {
            Profiler.enter("time cost on [handle]");
            handle(t);
            Profiler.release();// 数据包处理计时
        }
    }

    /*@SuppressWarnings("unchecked")
    private final Class<T> mClass = Reflects.getSuperClassGenericType(this.getClass(), 0);

    protected T decode0(Packet packet, Connection connection) {
        if (packet.hasFlag(Packet.FLAG_JSON_STRING_BODY)) {
            String body = packet.getBody();
            T t = Jsons.fromJson(body, mClass);
            if (t != null) {
                t.setConnection(connection);
                t.setPacket(packet);
            }
            return t;
        }
        return decode(packet, connection);
    }*/
}
