package com.three.common.memory;

import com.three.api.protocol.Packet;
import com.three.api.protocol.UDPPacket;
import com.three.config.common.IConfig;
import com.three.protocol.CommandEnum;

/**
 * 数据包工厂接口类
 * Created by Mathua on 2017/5/26.
 */
public interface PacketFactory {
    PacketFactory FACTORY = IConfig.chess.net.udpGateway() ? UDPPacket::new : Packet::new;

    static Packet get(CommandEnum.Command command) {
        return FACTORY.create(command);
    }

    Packet create(CommandEnum.Command command);
}
