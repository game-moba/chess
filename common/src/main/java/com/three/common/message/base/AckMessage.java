package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class AckMessage extends BaseMessage {

    public AckMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.Ack_15 bodyData = BaseModule.Ack_15.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        BaseModule.Ack_15.Builder builder = BaseModule.Ack_15.newBuilder();
        return builder.build().toByteArray();
    }

    public static AckMessage from(BaseMessage src) {
        return new AckMessage(new Packet(CommandEnum.Command.ACK, src.getSessionId()), src.connection);
    }

    @Override
    public String toString() {
        return "AckMessage{" +
                "packet=" + packet +
                '}';
    }
}
