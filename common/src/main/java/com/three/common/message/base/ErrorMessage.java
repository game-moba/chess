package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.ErrorCode;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;
import io.netty.channel.ChannelFutureListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.three.protocol.CommandEnum.Command.ERROR;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class ErrorMessage extends BaseMessage {
    private short cmd;// 出错的协议
    private short code;// 错误码
    private String reason;// 出错原因
    private List<String> data = new ArrayList<>();// 出错详细记录

    public ErrorMessage(short cmd, Packet message, Connection connection) {
        super(message, connection);
        this.cmd = cmd;
    }

    public ErrorMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.Error_8 bodyData = BaseModule.Error_8.parseFrom(body);
        cmd = (short) bodyData.getCmd().getNumber();
        code = (short) bodyData.getCode();
        reason = bodyData.getReason();
        data = bodyData.getDataList();
    }

    @Override
    public byte[] encode() {
        BaseModule.Error_8.Builder builder = BaseModule.Error_8.newBuilder();
        builder.setCmd(CommandEnum.Command.valueOf((int)cmd));
        if(code != 0)
            builder.setCode(code);
        if(reason != null)
            builder.setReason(reason);
        if(!data.isEmpty())
            builder.addAllData(data);
        return builder.build().toByteArray();
    }

    @Override
    protected Map<String, Object> encodeJsonBody() {
        Map<String, Object> body = new HashMap<>(4);
        if (cmd > 0) body.put("cmd", cmd);
        if (code > 0) body.put("code", code);
        if (reason != null) body.put("reason", reason);
        if (data != null) body.put("data", data);
        return body;
    }

    public static ErrorMessage from(BaseMessage src) {
        return new ErrorMessage(src.packet.cmd, src.packet.response(ERROR), src.connection);
    }

    public static ErrorMessage from(Packet src, Connection connection) {
        return new ErrorMessage(src.cmd, src.response(ERROR), connection);
    }


    public ErrorMessage setReason(String reason) {
        this.reason = reason;
        return this;
    }

    public ErrorMessage addData(String... data) {
        for(String tmp : data) {
            this.data.add(tmp);
        }
        return this;
    }

    public ErrorMessage setErrorCode(ErrorCode code) {
        this.code = code.errorCode;
        this.reason = code.errorMsg;
        return this;
    }

    public ErrorMessage setCode(short code) {
        this.code = code;
        return this;
    }

    @Override
    public void send() {
        super.sendRaw();
    }

    @Override
    public void close() {
        sendRaw(ChannelFutureListener.CLOSE);
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "reason='" + reason + '\'' +
                ", code=" + code +
                ", packet=" + packet +
                '}';
    }

    public short getCmd() {
        return cmd;
    }

    public short getCode() {
        return code;
    }

    public List<String> getData() {
        return data;
    }
}
