package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;

import static com.three.protocol.CommandEnum.Command.FAST_CONNECT;

/**
 * Created by mathua on 2017/5/28.
 */
public final class FastConnectReqMessage extends BaseMessage {
    private String sessionId;
    private String deviceId;
    private int minHeartbeat;
    private int maxHeartbeat;

    public FastConnectReqMessage(Connection connection) {
        super(new Packet(FAST_CONNECT, genSessionId()), connection);
    }

    public FastConnectReqMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.FastConnectReq_7 bodyData = BaseModule.FastConnectReq_7.parseFrom(body);
        sessionId = bodyData.getSessionId();
        deviceId = bodyData.getDeviceId();
        minHeartbeat = bodyData.getMinHeartbeat();
        maxHeartbeat = bodyData.getMaxHeartbeat();
    }

    @Override
    public byte[] encode() {
        BaseModule.FastConnectReq_7.Builder builder = BaseModule.FastConnectReq_7.newBuilder();
        builder.setSessionId(sessionId);
        builder.setDeviceId(deviceId);
        builder.setMinHeartbeat(minHeartbeat);
        builder.setMaxHeartbeat(maxHeartbeat);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "FastConnectMessage{" +
                "deviceId='" + deviceId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", minHeartbeat=" + minHeartbeat +
                ", maxHeartbeat=" + maxHeartbeat +
                ", packet=" + packet +
                '}';
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setMinHeartbeat(int minHeartbeat) {
        this.minHeartbeat = minHeartbeat;
    }

    public void setMaxHeartbeat(int maxHeartbeat) {
        this.maxHeartbeat = maxHeartbeat;
    }

    public String getSessionIdTmp() {
        return sessionId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public int getMinHeartbeat() {
        return minHeartbeat;
    }

    public int getMaxHeartbeat() {
        return maxHeartbeat;
    }
}

