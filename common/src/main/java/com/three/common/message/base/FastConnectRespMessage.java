package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;

import static com.three.protocol.CommandEnum.Command.FAST_CONNECT;

/**
 * Created by mathua on 2017/5/28.
 */
public final class FastConnectRespMessage extends BaseMessage {
    private int heartbeat;

    public FastConnectRespMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.FastConnectResp_7 bodyData = BaseModule.FastConnectResp_7.parseFrom(body);
        heartbeat = bodyData.getHeartbeat();
    }

    @Override
    public byte[] encode() {
        BaseModule.FastConnectResp_7.Builder builder = BaseModule.FastConnectResp_7.newBuilder();
        builder.setHeartbeat(heartbeat);
        return builder.build().toByteArray();
    }

    public static FastConnectRespMessage from(BaseMessage src) {
        return new FastConnectRespMessage(src.packet.response(FAST_CONNECT), src.connection);
    }

    public FastConnectRespMessage setHeartbeat(int heartbeat) {
        this.heartbeat = heartbeat;
        return this;
    }

    @Override
    public String toString() {
        return "FastConnectOkMessage{" +
                "heartbeat=" + heartbeat +
                ", packet=" + packet +
                '}';
    }

    public int getHeartbeat() {
        return heartbeat;
    }
}

