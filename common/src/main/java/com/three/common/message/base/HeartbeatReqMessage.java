package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;

/**
 * Created by mathua on 2017/6/3.
 */
public final class HeartbeatReqMessage extends BaseMessage {

    public HeartbeatReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.HeartBeatReq_1 bodyData = BaseModule.HeartBeatReq_1.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        BaseModule.HeartBeatReq_1.Builder builder = BaseModule.HeartBeatReq_1.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "HeartbeatMessage{" +
                ", packet=" + packet +
                '}';
    }

    public static HeartbeatReqMessage from(Connection connection) {
        return new HeartbeatReqMessage(new Packet(CommandEnum.Command.HEARTBEAT), connection);
    }

}
