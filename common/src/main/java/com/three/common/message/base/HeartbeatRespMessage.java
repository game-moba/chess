package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;

/**
 * Created by mathua on 2017/6/17.
 */
public class HeartbeatRespMessage extends BaseMessage {
    public HeartbeatRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.HeartBeatResp_1 bodyData = BaseModule.HeartBeatResp_1.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        BaseModule.HeartBeatResp_1.Builder builder = BaseModule.HeartBeatResp_1.newBuilder();
        builder.setTimestamp(System.currentTimeMillis());
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "HeartbeatMessage{" +
                ", packet=" + packet +
                '}';
    }

    public static HeartbeatRespMessage from(Connection connection) {
        return new HeartbeatRespMessage(new Packet(CommandEnum.Command.HEARTBEAT), connection);
    }
}
