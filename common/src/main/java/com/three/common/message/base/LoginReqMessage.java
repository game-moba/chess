package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;
import com.three.utils.StringUtils;

import java.util.Arrays;
import java.util.Map;

import static com.three.protocol.CommandEnum.Command.LOGIN;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class LoginReqMessage extends BaseMessage {
    private String openId;// 玩家id
    private String deviceId;// 设备id
    private String osName;// 系统名字
    private String osVersion;// 系统版本号
    private String platform;// 渠道标志
    private String clientVersion;// 客户端版本号
    private byte[] iv;
    private byte[] clientKey;
    private long timestamp;// 时间戳

    public LoginReqMessage(Connection connection) {
        super(new Packet(LOGIN, genSessionId()), connection);
    }

    public LoginReqMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        System.out.println(Arrays.toString(body));
        BaseModule.LoginReq_2 bodyData = BaseModule.LoginReq_2.parseFrom(body);
        openId = bodyData.getOpenId();
        deviceId = bodyData.getDeviceId();
        osName = bodyData.getOsName();
        osVersion = bodyData.getOsVersion();
        platform = bodyData.getPlatform();
        clientVersion = bodyData.getClientVersion();
        if(bodyData.hasIv())
            iv = StringUtils.praseFromString(bodyData.getIv());
        if(bodyData.hasClientKey())
            clientKey = StringUtils.praseFromString(bodyData.getClientKey());
//        System.out.println("收到iv:" + bodyData.getIv());
//        System.out.println("收到clientKey:" + bodyData.getClientKey());
        timestamp = bodyData.getTimestamp();
    }

    @Override
    public byte[] encode() {
        BaseModule.LoginReq_2.Builder builder = BaseModule.LoginReq_2.newBuilder();
        builder.setOpenId(openId);
        builder.setDeviceId(deviceId);
        builder.setOsName(osName);
        builder.setOsVersion(osVersion);
        builder.setPlatform(platform);
        builder.setClientVersion(clientVersion);
//        builder.setIv(Arrays.toString(iv));
//        builder.setClientKey(Arrays.toString(clientKey));
//        System.out.println("发送iv:" + Arrays.toString(iv));
//        System.out.println("发送clientKey:" + Arrays.toString(clientKey));
        builder.setTimestamp(timestamp);
        byte[] bytes = builder.build().toByteArray();
        System.out.println(Arrays.toString(bytes));
        return bytes;
    }

    @Override
    public void decodeJsonBody(Map<String, Object> body) {
        deviceId = (String) body.get("deviceId");
        osName = (String) body.get("osName");
        osVersion = (String) body.get("osVersion");
        clientVersion = (String) body.get("clientVersion");
    }

    @Override
    public String toString() {
        return "HandshakeMessage{" +
                "clientKey=" + Arrays.toString(clientKey) +
                ", deviceId='" + deviceId + '\'' +
                ", osName='" + osName + '\'' +
                ", osVersion='" + osVersion + '\'' +
                ", clientVersion='" + clientVersion + '\'' +
                ", iv=" + Arrays.toString(iv) +
                ", timestamp=" + timestamp +
                ", packet=" + packet +
                '}';
    }

    public String getOpenId() {
        return openId;
    }

    public String getOsName() {
        return osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public byte[] getIv() {
        return iv;
    }

    public byte[] getClientKey() {
        return clientKey;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getPlatform() {
        return platform;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /*****************************这些set方法只是测试用的****************************************/
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    public void setClientKey(byte[] clientKey) {
        this.clientKey = clientKey;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
}