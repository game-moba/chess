package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;

import java.util.Map;

/**
 * Created by mathua on 2017/5/28.
 */
public final class LogoutMessage extends BaseMessage {

    public LogoutMessage(Connection connection) {
        super(new Packet(CommandEnum.Command.LOGIN, genSessionId()), connection);
    }

    public LogoutMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.Logout_6 bodyData = BaseModule.Logout_6.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        BaseModule.Logout_6.Builder builder = BaseModule.Logout_6.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public void decodeJsonBody(Map<String, Object> body) {

    }

    @Override
    public String toString() {
        return "BindPlayerMessage{" +
                ", packet=" + packet +
                '}';
    }

}

