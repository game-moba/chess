package com.three.common.message.base;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.protocol.PopModule;

/**
 * Created by YW0981 on 2017/6/13.
 */
public final class PopMessage extends BaseMessage{

    public PopMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {

    }

    @Override
    public byte[] encode() {
        PopModule.Prompt.Builder prompt = PopModule.Prompt.newBuilder();
        //TODO
        return new byte[0];
    }

    @Override
    public String toString() {
        return null;
    }
}
