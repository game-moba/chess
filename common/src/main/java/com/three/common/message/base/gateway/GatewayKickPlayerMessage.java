package com.three.common.message.base.gateway;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.memory.PacketFactory;
import com.three.common.message.base.BaseMessage;
import com.three.common.router.KickRemoteMsg;
import com.three.protocol.BaseModule;

import static com.three.protocol.CommandEnum.Command.GATEWAY_KICK;

/**
 * 网关踢玩家的消息
 * Created by Mathua on 2017/5/26.
 */
public final class GatewayKickPlayerMessage extends BaseMessage implements KickRemoteMsg {
    private long playerId;// 玩家id
    private String deviceId;// 设备id
    private String connId;// 连接id
    private int clientType;// 客户端类型
    private String targetServer;// 所在目标服务器
    private int targetPort;// 所在目标端口

    public GatewayKickPlayerMessage(Packet message, Connection connection) {
        super(message, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        BaseModule.GatewayKickPlayer_12 bodyData = BaseModule.GatewayKickPlayer_12.parseFrom(body);
        playerId = bodyData.getPlayerId();
        deviceId = bodyData.getDeviceId();
        connId = bodyData.getConnId();
        clientType = bodyData.getClientType();
        targetServer = bodyData.getTargetServer();
        targetPort = bodyData.getTargetPort();
    }

    @Override
    public byte[] encode() {
        BaseModule.GatewayKickPlayer_12.Builder builder = BaseModule.GatewayKickPlayer_12.newBuilder();
        builder.setPlayerId(playerId);
        builder.setDeviceId(deviceId);
        builder.setConnId(connId);
        builder.setClientType(clientType);
        builder.setTargetServer(targetServer);
        builder.setTargetPort(targetPort);
        return builder.build().toByteArray();
    }

    public static GatewayKickPlayerMessage build(Connection connection) {
        Packet packet = PacketFactory.get(GATEWAY_KICK);
        packet.sessionId = genSessionId();
        return new GatewayKickPlayerMessage(packet, connection);
    }

    public GatewayKickPlayerMessage setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public GatewayKickPlayerMessage setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public GatewayKickPlayerMessage setConnId(String connId) {
        this.connId = connId;
        return this;
    }

    public GatewayKickPlayerMessage setClientType(int clientType) {
        this.clientType = clientType;
        return this;
    }

    public GatewayKickPlayerMessage setTargetServer(String targetServer) {
        this.targetServer = targetServer;
        return this;
    }

    public GatewayKickPlayerMessage setTargetPort(int targetPort) {
        this.targetPort = targetPort;
        return this;
    }

    @Override
    public long getPlayerId() {
        return playerId;
    }

    @Override
    public String getDeviceId() {
        return deviceId;
    }

    @Override
    public String getConnId() {
        return connId;
    }

    @Override
    public int getClientType() {
        return clientType;
    }

    @Override
    public String getTargetServer() {
        return targetServer;
    }

    @Override
    public int getTargetPort() {
        return targetPort;
    }

    @Override
    public String toString() {
        return "GatewayKickPlayerMessage{" +
                "playerId='" + playerId + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", connId='" + connId + '\'' +
                ", clientType=" + clientType +
                ", targetServer='" + targetServer + '\'' +
                ", targetPort=" + targetPort +
                '}';
    }
}