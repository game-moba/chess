package com.three.common.message.mail;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.MailModule;

/**
 * Created by mathua on 2017/6/24.
 */
public final class NotifyMailListMessage extends BaseMessage{

    public NotifyMailListMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        MailModule.NotifyMailList_200 bodyData = MailModule.NotifyMailList_200.parseFrom(body);
        for(MailModule.Mail mail : bodyData.getMailsList()) {
            System.out.println();
            System.out.println("MailId :" + mail.getMailId());
            System.out.println("Content :" + mail.getContent());
            System.out.println("ItemsJson :" + mail.getItemsJson());
            System.out.println("Subject :" + mail.getSubject());
            System.out.println("HadRead :" + mail.getHadRead());
            System.out.println("IsRecommend :" + mail.getIsRecommend());
            System.out.println("MailType :" + mail.getMailType());
            System.out.println("SendTime :" + mail.getSendTime());
            System.out.println();
        }
    }

    @Override
    public byte[] encode() {
        return new byte[0];
    }

    @Override
    public String toString() {
        return null;
    }
}
