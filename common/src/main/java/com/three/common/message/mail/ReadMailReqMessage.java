package com.three.common.message.mail;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.MailModule;

/**
 * Created by mathua on 2017/6/24.
 */
public final class ReadMailReqMessage extends BaseMessage {
    private long mailId;

    public ReadMailReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        MailModule.ReadMailReq_201 bodyData = MailModule.ReadMailReq_201.parseFrom(body);
        mailId = bodyData.getMailId();
    }

    @Override
    public byte[] encode() {
        MailModule.ReadMailReq_201.Builder builder = MailModule.ReadMailReq_201.newBuilder();
        builder.setMailId(mailId);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "ReadMailReqMessage{" +
                "mailId=" + mailId +
                '}';
    }

    public long getMailId() {
        return mailId;
    }

}
