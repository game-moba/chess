package com.three.common.message.mail;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.MailModule;

/**
 * Created by mathua on 2017/6/24.
 */
public final class ReadMailRespMessage extends BaseMessage{
    private long mailId;

    public ReadMailRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        MailModule.ReadMailResp_201 bodyData = MailModule.ReadMailResp_201.parseFrom(body);
        mailId = bodyData.getMailId();
    }

    @Override
    public byte[] encode() {
        MailModule.ReadMailResp_201.Builder builder = MailModule.ReadMailResp_201.newBuilder();
        builder.setMailId(mailId);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "ReadMailRespMessage{" +
                "mailId=" + mailId +
                '}';
    }

    public static ReadMailRespMessage from(BaseMessage src) {
        return new ReadMailRespMessage(src.getPacket().response(CommandEnum.Command.READ_MAIL), src.getConnection());
    }

    public ReadMailRespMessage setMailId(long mailId) {
        this.mailId = mailId;
        return this;
    }
}
