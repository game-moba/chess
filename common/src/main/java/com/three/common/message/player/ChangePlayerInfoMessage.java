package com.three.common.message.player;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.constant.GameConstants;
import com.three.protocol.PlayerModule;

/**
 * Created by mathua on 2017/6/20.
 */
public final class ChangePlayerInfoMessage extends BaseMessage{

    private String nickName;
    private String head;
    private int gender = GameConstants.INVALID_VALUE;
    private int provinceID = GameConstants.INVALID_VALUE;
    private String city;
    private long phone = GameConstants.INVALID_VALUE;

    public ChangePlayerInfoMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        PlayerModule.ChangePlayerInfoReq_102 bodyData = PlayerModule.ChangePlayerInfoReq_102.parseFrom(body);
        if(bodyData.hasNickName())
            nickName = bodyData.getNickName();
        if(bodyData.hasHead())
            head = bodyData.getHead();
        if(bodyData.hasGender())
            gender = bodyData.getGender();
        if(bodyData.hasProvinceID())
            provinceID = bodyData.getProvinceID();
        if(bodyData.hasCity())
            city = bodyData.getCity();
        if(bodyData.hasPhone())
            phone = bodyData.getPhone();
    }

    @Override
    public byte[] encode() {
        PlayerModule.ChangePlayerInfoReq_102.Builder builder = PlayerModule.ChangePlayerInfoReq_102.newBuilder();
        if(nickName != null)
            builder.setNickName(nickName);
        if(head != null)
            builder.setHead(head);
        if(gender != GameConstants.INVALID_VALUE)
            builder.setGender(gender);
        if(provinceID != GameConstants.INVALID_VALUE)
            builder.setProvinceID(provinceID);
        if(city != null)
            builder.setCity(city);
        if(phone != GameConstants.INVALID_VALUE)
            builder.setPhone(phone);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "ChangePlayerInfoMessage{" +
                "nickName='" + nickName + '\'' +
                ", head='" + head + '\'' +
                ", gender=" + gender +
                ", provinceID=" + provinceID +
                ", city='" + city + '\'' +
                ", phone=" + phone +
                '}';
    }

    public String getNickName() {
        return nickName;
    }

    public String getHead() {
        return head;
    }

    public int getGender() {
        return gender;
    }

    public int getProvinceID() {
        return provinceID;
    }

    public String getCity() {
        return city;
    }

    public long getPhone() {
        return phone;
    }
}
