package com.three.common.message.player;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.PlayerModule;

/**
 * Created by mathua on 2017/6/18.
 */
public final class NotifyPlayerAttrChangeMessage extends BaseMessage {

    private PlayerModule.PlayerAttr playerAttr;
    private long latestValue;

    public NotifyPlayerAttrChangeMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static NotifyPlayerInfoMessage from(Connection connection) {
        return new NotifyPlayerInfoMessage(new Packet(CommandEnum.Command.NOTIFY_PLAYER_ATTR_CHANGE), connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        PlayerModule.NotifyPlayerAttrChange_101 bodyData = PlayerModule.NotifyPlayerAttrChange_101.parseFrom(body);
        playerAttr = bodyData.getPlayerAttr();
        latestValue = bodyData.getLatestValue();
    }

    @Override
    public byte[] encode() {
        PlayerModule.NotifyPlayerAttrChange_101.Builder builder = PlayerModule.NotifyPlayerAttrChange_101.newBuilder();
        builder.setPlayerAttr(playerAttr);
        builder.setLatestValue(latestValue);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "NotifyPlayerAttrChangeMessage{" +
                "playerAttr='" + playerAttr + '\'' +
                ", latestValue='" + latestValue + '\'' +
                '}';
    }

    public NotifyPlayerAttrChangeMessage setPlayerAttr(PlayerModule.PlayerAttr playerAttr) {
        this.playerAttr = playerAttr;
        return this;
    }

    public NotifyPlayerAttrChangeMessage setLatestValue(long latestValue) {
        this.latestValue = latestValue;
        return this;
    }
}
