package com.three.common.message.player;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.PlayerModule;

/**
 * Created by mathua on 2017/6/11.
 */
public final class NotifyPlayerInfoMessage extends BaseMessage {

    private long playerId;                          // 玩家id
    private String nickName;                        // 昵称
    private String head;                               // 头像配置
    private int level;                              // 玩家等级
    private long exp;                               // 经验
    private int provinceID;                         // 省份id
    private String city;                            // 城市名称
    private long phone;                             // 手机
    private int gender;                             // 性别
    private long gold;                              // 金币
    private long diamond;                           // 钻石

    public NotifyPlayerInfoMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static NotifyPlayerInfoMessage from(Connection connection) {
        return new NotifyPlayerInfoMessage(new Packet(CommandEnum.Command.NOTIFY_PLAYER_INFO), connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        PlayerModule.NotifyPlayerInfo_100 bodyData = PlayerModule.NotifyPlayerInfo_100.parseFrom(body);
        playerId = bodyData.getPlayerId();
        nickName = bodyData.getNickName();
        head = bodyData.getHead();
        level = bodyData.getLevel();
        exp = bodyData.getExp();
        provinceID = bodyData.getProvinceID();
        city = bodyData.getCity();
        phone = bodyData.getPhone();
        gender = bodyData.getGender();
        gold = bodyData.getGold();
        diamond = bodyData.getDiamond();
    }

    @Override
    public byte[] encode() {
        PlayerModule.NotifyPlayerInfo_100.Builder builder = PlayerModule.NotifyPlayerInfo_100.newBuilder();
        builder.setPlayerId(playerId);
        builder.setNickName(nickName);
        builder.setHead(head);
        builder.setLevel(level);
        builder.setExp(exp);
        builder.setProvinceID(provinceID);
        builder.setCity(city);
        builder.setPhone(phone);
        builder.setGender(gender);
        builder.setGold(gold);
        builder.setDiamond(diamond);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return "NotifyPlayerInfoMessage{" +
                "nickName='" + nickName + '\'' +
                ", playerId='" + playerId + '\'' +
                ", head='" + head + '\'' +
                ", level='" + level + '\'' +
                ", exp='" + exp + '\'' +
                ", provinceID='" + provinceID + '\'' +
                ", city='" + city + '\'' +
                ", phone='" + phone + '\'' +
                ", gender='" + gender + '\'' +
                ", gold='" + gold + '\'' +
                '}';
    }

    public NotifyPlayerInfoMessage setPlayerId(long playerId) {
        this.playerId = playerId;
        return this;
    }

    public NotifyPlayerInfoMessage setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    public NotifyPlayerInfoMessage setHead(String head) {
        this.head = head;
        return this;
    }

    public NotifyPlayerInfoMessage setLevel(int level) {
        this.level = level;
        return this;
    }

    public NotifyPlayerInfoMessage setExp(long exp) {
        this.exp = exp;
        return this;
    }

    public NotifyPlayerInfoMessage setProvinceID(int provinceID) {
        this.provinceID = provinceID;
        return this;
    }

    public NotifyPlayerInfoMessage setCity(String city) {
        this.city = city;
        return this;
    }

    public NotifyPlayerInfoMessage setPhone(long phone) {
        this.phone = phone;
        return this;
    }

    public NotifyPlayerInfoMessage setGender(int gender) {
        this.gender = gender;
        return this;
    }

    public NotifyPlayerInfoMessage setGold(long gold) {
        this.gold = gold;
        return this;
    }

    public NotifyPlayerInfoMessage setDiamond(long diamond) {
        this.diamond = diamond;
        return this;
    }

    public long getPlayerId() {
        return playerId;
    }

    public String getNickName() {
        return nickName;
    }

    public String getHead() {
        return head;
    }

    public int getLevel() {
        return level;
    }

    public long getExp() {
        return exp;
    }

    public int getProvinceID() {
        return provinceID;
    }

    public String getCity() {
        return city;
    }

    public long getPhone() {
        return phone;
    }

    public int getGender() {
        return gender;
    }

    public long getGold() {
        return gold;
    }

    public long getDiamond() {
        return diamond;
    }
}
