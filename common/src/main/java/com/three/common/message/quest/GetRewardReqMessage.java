package com.three.common.message.quest;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.TaskModule;

public class GetRewardReqMessage extends BaseMessage {
    private long taskId;
    private long playerId;
    public GetRewardReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }
    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        TaskModule.GetRewardReq_301 bodyData=TaskModule.GetRewardReq_301.parseFrom(body);
        taskId=bodyData.getTaskId();
        playerId=connection.getSessionContext().getPlayerId();
    }

    @Override
    public byte[] encode() {
        return TaskModule.GetRewardReq_301.newBuilder().setTaskId(this.taskId).build().toByteArray();
    }

    @Override
    public String toString() {
        return "GetRewardReqMessage{}";
    }

    public long getTaskId() {
        return taskId;
    }

    public long getPlayerId() {
        return playerId;
    }
}
