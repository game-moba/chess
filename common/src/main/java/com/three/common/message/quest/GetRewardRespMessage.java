package com.three.common.message.quest;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.common.message.mail.ReadMailRespMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.TaskModule;

public class GetRewardRespMessage extends BaseMessage {
    private long taskId;
    public GetRewardRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }
    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        TaskModule.GetRewardReq_301 bodyData=TaskModule.GetRewardReq_301.parseFrom(body);
        taskId=bodyData.getTaskId();
    }

    @Override
    public byte[] encode() {
        return TaskModule.GetRewardReq_301.newBuilder().setTaskId(this.taskId).build().toByteArray();
    }

    @Override
    public String toString() {
        return "GetRewardReqMessage{}";
    }

    public long getTaskId() {
        return taskId;
    }

    public static GetRewardRespMessage from(BaseMessage src) {
        return new GetRewardRespMessage(src.getPacket().response(CommandEnum.Command.GET_REWARD), src.getConnection());
    }

    public GetRewardRespMessage setTaskId(long taskId) {
        this.taskId = taskId;
        return this;
    }
}
