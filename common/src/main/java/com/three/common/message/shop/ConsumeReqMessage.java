package com.three.common.message.shop;

import com.alibaba.druid.support.json.JSONUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.ShopModule;

/**
 * Created by wangziqing on 2017/7/16 0016.
 */
public class ConsumeReqMessage extends BaseMessage{
    private int itemId;
    private long num;
    private int channelId;
    public ConsumeReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        ShopModule.ConsumeReq_400 bodyData=ShopModule.ConsumeReq_400.parseFrom(body);
        this.itemId=bodyData.getItemId();
        this.num=bodyData.getNum();
        this.channelId=bodyData.getChannel();
    }

    @Override
    public byte[] encode() {
        return ShopModule.ConsumeReq_400.newBuilder().setChannel(this.channelId).setItemId(this.itemId).setNum(this.num).build().toByteArray();
    }

    @Override
    public String toString() {
        return JSONUtils.toJSONString(this);
    }

    public int getItemId() {
        return itemId;
    }

    public long getNum() {
        return num;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setNum(long num) {
        this.num = num;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}
