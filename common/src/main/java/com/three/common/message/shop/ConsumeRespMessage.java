package com.three.common.message.shop;

import com.alibaba.druid.support.json.JSONUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.ShopModule;

/**
 * Created by wangziqing on 2017/7/16 0016.
 */
public class ConsumeRespMessage extends BaseMessage{
    private int itemId;
    private long orderId;
    public ConsumeRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        ShopModule.ConsumeResp_401 bodyData=ShopModule.ConsumeResp_401.parseFrom(body);
        this.itemId=bodyData.getItemId();
        this.orderId=bodyData.getOrderId();
    }

    @Override
    public byte[] encode() {
        return ShopModule.ConsumeResp_401.newBuilder().setOrderId(this.orderId).setItemId(this.itemId).build().toByteArray();
    }

    @Override
    public String toString() {
        return JSONUtils.toJSONString(this);
    }

    public static ConsumeRespMessage from(BaseMessage src) {
        return new ConsumeRespMessage(src.getPacket().response(CommandEnum.Command.COMSUME_REQ), src.getConnection());
    }
    public int getItemId() {
        return itemId;
    }

    public long getOrderId() {
        return orderId;
    }

    public ConsumeRespMessage setItemId(int itemId) {
        this.itemId = itemId;
        return this;
    }

    public ConsumeRespMessage setOrderId(long orderId) {
        this.orderId = orderId;
        return this;
    }
}
