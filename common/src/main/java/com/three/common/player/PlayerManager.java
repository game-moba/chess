package com.three.common.player;

import com.three.api.spi.common.CacheManager;
import com.three.api.spi.common.CacheManagerFactory;
import com.three.common.CacheKeys;
import com.three.tools.config.ConfigManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 用户管理类
 * Created by mathua on 2017/5/25.
 */
//查询使用
public final class PlayerManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayerManager.class);
    public static final PlayerManager I = new PlayerManager();

    private final CacheManager cacheManager = CacheManagerFactory.create();

    private final String onlinePlayerListKey = CacheKeys.getOnlinePlayerListKey(ConfigManager.I.getPublicIp());

    public void clearPlayerOnlineData() {
        cacheManager.del(onlinePlayerListKey);
    }

    public void addToOnlineList(long playerId) {
        cacheManager.zAdd(onlinePlayerListKey, Long.toString(playerId));
        LOGGER.info("player online {}", playerId);
    }

    public void remFormOnlineList(long playerId) {
        cacheManager.zRem(onlinePlayerListKey, Long.toString(playerId));
        LOGGER.info("player offline {}", playerId);
    }

    //在线用户数量
    public long getOnlinePlayerNum() {
        Long value = cacheManager.zCard(onlinePlayerListKey);
        return value == null ? 0 : value;
    }

    //在线用户数量
    public long getOnlinePlayerNum(String publicIP) {
        String online_key = CacheKeys.getOnlinePlayerListKey(publicIP);
        Long value = cacheManager.zCard(online_key);
        return value == null ? 0 : value;
    }

    //在线用户列表
    public List<Long> getOnlinePlayerList(String publicIP, int start, int end) {
        String key = CacheKeys.getOnlinePlayerListKey(publicIP);
        return cacheManager.zrange(key, start, end, Long.class);
    }
}
