package com.three.common.push;

import com.three.common.message.base.gateway.GatewayPushMessage;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/5/30.
 */
public final class GatewayPushResult {
    public long playerId;
    public Integer clientType;
    public Object[] timePoints;

    public GatewayPushResult() {
    }

    public GatewayPushResult(long playerId, Integer clientType, Object[] timePoints) {
        this.playerId = playerId;
        this.timePoints = timePoints;
        if (clientType > 0) this.clientType = clientType;
    }

    public static String toJson(GatewayPushMessage message, Object[] timePoints) {
        return JsonUtils.toJson(new GatewayPushResult(message.getPlayerId(), message.getClientType(), timePoints));
    }

    public static GatewayPushResult fromJson(String json) {
        if (json == null) return null;
        return JsonUtils.fromJson(json, GatewayPushResult.class);
    }
}
