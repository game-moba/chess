package com.three.common.receiver;

import com.three.common.MessageDispatcher;

import static com.three.common.MessageDispatcher.POLICY_LOG;

/**
 * Created by mathua on 2017/6/1.
 */
public interface IReceiver {

    MessageDispatcher gatewayClientReceiver = new MessageDispatcher();
    MessageDispatcher gatewayUDPConnectorReceiver = new MessageDispatcher(POLICY_LOG);
    MessageDispatcher webSocketServerReceiver = new MessageDispatcher();
    MessageDispatcher gatewayServerReceiver = new MessageDispatcher();
    MessageDispatcher connectionServerReceiver = new MessageDispatcher();
    MessageDispatcher rocketmqServerReceiver = new MessageDispatcher();
}



