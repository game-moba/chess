package com.three.common.receiver;

import com.three.common.MessageDispatcher;

/**
 * Created by mathua on 2017/6/1.
 */
public enum ReceiverType {
    GATEWAY_CLIENT_RECEIVER(IReceiver.gatewayClientReceiver),//
    GATEWAY_UDP_CONNECTOR_RECEIVER(IReceiver.gatewayUDPConnectorReceiver),//
    WEB_SOCKET_SERVER_RECEIVER(IReceiver.webSocketServerReceiver),//
    GATEWAY_SERVER_RECEIVER(IReceiver.gatewayServerReceiver),//
    CONNECTION_SERVER_RECEIVER(IReceiver.connectionServerReceiver),//
    /**
     * 跨服协议接收者
     */
    ROCKET_MQ_RECEIVER(IReceiver.rocketmqServerReceiver),
    ;

    MessageDispatcher receiver;

    ReceiverType(MessageDispatcher receiver) {
        this.receiver = receiver;
    }

    public MessageDispatcher getReceiver() {
        return receiver;
    }
}
