package com.three.common.router;

import com.three.api.router.ClientClassifier;
import com.three.api.spi.Spi;
import com.three.api.spi.router.ClientClassifierFactory;

/**
 * Created by mathua on 2017/6/11.
 */
@Spi(order = 1)
public final class DefaultClientClassifier implements ClientClassifier, ClientClassifierFactory {

    @Override
    public int getClientType(String osName) {
        return ClientType.find(osName).type;
    }

    @Override
    public ClientClassifier get() {
        return this;
    }
}
