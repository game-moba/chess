package com.three.common.router;

import com.three.common.ServerNodes;
import com.three.tools.Utils;

/**
 * 远程机器踢人消息接口类
 * Created by mathua on 2017/5/25.
 */
public interface KickRemoteMsg {
    long getPlayerId();

    String getDeviceId();

    String getConnId();

    int getClientType();

    String getTargetServer();

    int getTargetPort();

    default boolean isTargetPC() {
        return this.getTargetPort() == ServerNodes.GS.getPort()
                && this.getTargetServer().equals(Utils.getLocalIp());
    }
}
