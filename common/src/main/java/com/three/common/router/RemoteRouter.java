package com.three.common.router;

import com.three.api.router.ClientLocation;
import com.three.api.router.Router;

/**
 * 远程路由信息类
 * Created by mathua on 2017/5/25.
 */
public final class RemoteRouter implements Router<ClientLocation> {
    private final ClientLocation clientLocation;

    public RemoteRouter(ClientLocation clientLocation) {
        this.clientLocation = clientLocation;
    }

    public boolean isOnline() {
        return clientLocation.isOnline();
    }

    public boolean isOffline() {
        return clientLocation.isOffline();
    }

    @Override
    public ClientLocation getRouteValue() {
        return clientLocation;
    }

    @Override
    public RouterType getRouteType() {
        return RouterType.REMOTE;
    }

    @Override
    public String toString() {
        return "RemoteRouter{" + clientLocation + '}';
    }
}
