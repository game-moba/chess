package com.three.common.security;

import com.three.api.connection.Cipher;
import com.three.api.spi.Spi;
import com.three.api.spi.core.CipherFactory;

@Spi
public class RsaCipherFactory implements CipherFactory {
    private static final RsaCipher RSA_CIPHER = RsaCipher.create();

    @Override
    public Cipher get() {
        return RSA_CIPHER;
    }
}
