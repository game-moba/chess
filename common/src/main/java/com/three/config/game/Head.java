package com.three.config.game;

/**
 * 表头<br>
 *
 * @Author mathua
 * @Date 2017/5/18 20:14
 */
public class Head {
    private HeadBase headBase;
    private ListHead listHead;// headBase为list的时候用到
    private boolean isServer;// 服务器用到
    private boolean isClient;// 客户端用到

    public static Head build(HeadBase headBase, ListHead listHead, boolean isServer, boolean isClient) {
        Head head = new Head();
        head.headBase = headBase;
        head.listHead = listHead;
        head.isClient = isClient;
        head.isServer = isServer;
        return head;
    }

    public HeadBase getHeadBase() {
        return headBase;
    }

    public ListHead getListHead() {
        return listHead;
    }

    public boolean isServer() {
        return isServer;
    }

    public boolean isClient() {
        return isClient;
    }
}
