package com.three.config.game;

/**
 * @Author mathua
 * @Date 2017/5/18 20:15
 */
public class HeadBase {
    private String name;
    private Type type;

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public static HeadBase build(String name, Type type) {
        HeadBase headBase = new HeadBase();
        headBase.name = name;
        headBase.type = type;
        return headBase;
    }
}
