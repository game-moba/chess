package com.three.config.game;

/**
 * @Author mathua
 * @Date 2017/5/18 20:15
 */
public class ListHead {
    // 嵌套对象
    private String className;
    private HeadBase headBaseList[];
    // 只有基本类型
    private Type type;

    private String splitStr;
    private HeadType headType;

    public enum HeadType {
        BASE, // 基类类型
        NESTED,// 嵌套类型
    }

    public Type getType() {
        return type;
    }

    public String getClassName() {
        return className;
    }

    public HeadBase[] getHeadBaseList() {
        return headBaseList;
    }

    public String getSplitStr() {
        return splitStr;
    }

    public HeadType getHeadType() {
        return headType;
    }

    public static ListHead build(String className, HeadBase headBaseList[], String splitStr) {
        ListHead listHead = new ListHead();
        listHead.className = className;
        listHead.headBaseList = headBaseList;
        listHead.splitStr = splitStr;
        listHead.headType = HeadType.NESTED;
        return listHead;
    }

    public static ListHead build(Type type, String splitStr) {
        ListHead listHead = new ListHead();
        listHead.type = type;
        listHead.splitStr = splitStr;
        listHead.headType = HeadType.BASE;
        return listHead;
    }
}
