package com.three.config.game;

import java.util.Iterator;
import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/18 20:15
 */
public class Table {
    private String clazzName;
    private String clientName;
    private List<Head> heads;
    private List<List<Object>> datas;

    public StringBuffer parseToLua() {
        StringBuffer lua = new StringBuffer();
        lua.append("Config = Config or {}" + "\n");
        lua.append("Config." + clientName + " = {" + "\n");
        for(int i = 0; i < datas.size(); ++i) {
            List<Object> lineDatas = datas.get(i);
            lua.append("\t");
            for(int j = 0; j < lineDatas.size(); ++j) {
                Head head = heads.get(j);
                Object lineItem = lineDatas.get(j);
                if (j == 0) {
                    if (head.getHeadBase().getType() == Type.STRING) {
                        lua.append(lineItem).append(" = { ");
                    } else {
                        lua.append("[").append(lineItem).append("]").append(" = { ");
                    }
                }
                if (head.getHeadBase().getType() != Type.LIST) {
                    lua.append(captureName(head.getHeadBase().getName())).append(" = ").append(praseValue(lineItem)).append(", ");
                } else {
                    if (head.getListHead().getHeadType() == ListHead.HeadType.BASE) {
                        List<Object> listData = (List<Object>) lineItem;
                        String tmp = "";
                        for (int k = 0; k < listData.size(); ++k) {
                            Object obj = listData.get(k);
                            tmp += obj.toString();
                            if (k != listData.size() - 1)
                                tmp += "|";
                        }
                        lua.append(captureName(head.getHeadBase().getName())).append(" = ").append(praseValue(tmp)).append(", ");
                    } else {
                        lua.append(captureName(head.getHeadBase().getName())).append(" = ").append(" { ");
                        List<Object> listData = (List<Object>) lineItem;
                        for (int k = 0; k < listData.size(); ++k) {
                            lua.append("[").append(k).append("] = {");
                            List<Object> items = (List<Object>) listData.get(k);
                            for (int n = 0; n < items.size(); ++n) {
                                String tmpName = head.getListHead().getHeadBaseList()[n].getName();
                                lua.append(captureName(tmpName)).append(" = ").append(praseValue(items.get(n))).append(", ");
                            }
                            lua.append(" }, ");
                        }
                        lua.append(" }, ");
                    }
                }
                if (j == lineDatas.size() - 1) {
                    lua.append("},");
                }
            }
            lua.append("\n");
        }
        lua.append("}" + "\n");
        return lua;
    }

    private Object praseValue(Object obj) {
        if(obj instanceof String)
            return "\"" + obj + "\"";
        return obj;
    }

    public static Table build(String clazzName, String clientName, List<Head> heads, List<List<Object>> datas) {
        Table table = new Table();
        table.clazzName = clazzName;
        table.clientName = clientName;
        table.heads = heads;
        table.datas = datas;
        return table;
    }

    /**
     * 裁掉非服务器的配置
     */
    public void trimServer() {
        Iterator<Head> it = heads.iterator();
        while (it.hasNext()) {
            Head head = it.next();
            if (!head.isServer())
                it.remove();
        }
    }

    /**
     * 裁掉非客户端的配置
     */
    public void trimClient() {
        Iterator<Head> it = heads.iterator();
        while (it.hasNext()) {
            Head head = it.next();
            if (!head.isClient())
                it.remove();
        }
    }

    public Iterator<List<Object>> iterator() {
        return datas.iterator();
    }

    public String getClazzName() {
        return clazzName;
    }

    public String getClientName() {
        return clientName;
    }

    public List<Head> getHeads() {
        return heads;
    }

    public List<List<Object>> getDatas() {
        return datas;
    }

    /**
     * 首字母转大写
     * @param name
     * @return
     */
    public static String captureName(String name) {
        char[] cs=name.toCharArray();
        if(cs[0] >= 'a' && cs[0] <= 'z')
            cs[0]-=32;
        return String.valueOf(cs);
    }
}
