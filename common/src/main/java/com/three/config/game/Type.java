package com.three.config.game;

import java.util.HashMap;
import java.util.Map;

/**
 * 表的数据类型<br>
 *
 * @Author mathua
 * @Date 2017/5/18 20:16
 */
public enum Type {
    LONG(1), //
    INT(2), //
    SHORT(3), //
    BYTE(4), //
    BOOLEAN(5), //
    FLOAT(6), //
    DOUBLE(7), //
    STRING(8), //
    LIST(9),//
    ;
    public final int id;

    private static Map<Integer, Type> map = new HashMap<Integer, Type>();
    static {
        for (Type type : Type.values()) {
            map.put(type.id, type);
        }
    }

    private Type(int id) {
        this.id = id;
    }
}
