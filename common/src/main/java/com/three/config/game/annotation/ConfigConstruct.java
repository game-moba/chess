package com.three.config.game.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @Author mathua
 * @Date 2017/5/18 20:19
 */
@Retention(RUNTIME)
@Target(METHOD)
@Documented
public @interface ConfigConstruct {
    ConfigMethodType type() default ConfigMethodType.INIT;// 方法类型
    boolean isReturn() default true;// 是否返回
}
