package com.three.config.game.annotation;

/**
 * @Author mathua
 * @Date 2017/5/18 20:20
 */
public enum ConfigMethodType {
    INIT, // 初始化方法
    CHECK,// 检测方法
}
