package com.three.config.spring;
/**
 * Created by ziqingwang on 2017/5/19 0019.
 */
@Config
public interface Chess {

    interface buss {
        interface common {
            String key = null;
        }

        interface sso {
            String key = null;
        }
    }

    interface db {
        interface player {
            String url = null;
            String playername = null;
            String password = null;
            @Config(name = "driver-class-name")
            String driver_class_name = null;
        }

        interface buss {
            String url = null;
            String playername = null;
            String password = null;
            @Config(name = "driver-class-name")
            String driver_class_name = null;
        }
    }

    interface nsq {
        Boolean enable = null;
        String nsqd = null;
        String nsqlookupd = null;
    }

    interface thread {
        interface pool {
            @Config(name = "conn-work")
            Integer conn_work = null;
            @Config(name = "gateway-server-work")
            Integer gateway_server_work = null;
            @Config(name = "http-work")
            Integer http_work = null;
            @Config(name = "ack-timer")
            Integer ack_timer = null;
            @Config(name = "push-task")
            Integer push_task = null;
            @Config(name = "push-client")
            Integer push_client = null;

            interface event_bus {
                Integer min = null;
                Integer max = null;
                @Config(name = "queue-size")
                Integer queue_size = null;
            }

            interface mq {
                Integer min = null;
                Integer max = null;
                @Config(name = "queue-size")
                Integer queue_size = null;
            }
        }
    }

    interface zk {

//        Integer sessionTimeoutMs = null;
//
//        String watch_path = null;

        //        int connectionTimeoutMs = (int) cfg.getDuration("connectionTimeoutMs", TimeUnit.MILLISECONDS);
//
//        String namespace = null;
//
//        String digest = null;
//
        @Config(name = "server_address")
        String server_address = null;
//
//        interface retry {
//
//
//            Integer maxRetries = null;
//
//            Integer baseSleepTimeMs = null;
//
//            Integer maxSleepMs = null;
//        }
    }
}
