package com.three.config.spring;

import java.lang.annotation.*;

/**
 * Created by wangziqing on 2017/5/21 0021.
 */
@Documented
@Inherited
@Target(value = {ElementType.TYPE,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Config {
    String name() default "";
}
