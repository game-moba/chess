package com.three.constant;

/**
 * @Author mathua
 * @Date 2017/5/18 20:12
 */
public final class ConfigConstants {
    public static final int CLASS_NAME_ROW = 0;// 类名字行
    public static final int CLASS_NAME_CELL = 1;
    public static final int CLIENT_NAME_ROW = 1;// 前段名字行
    public static final int CLIENT_NAME_CELL = 1;
    public static final int FLAG_ROW = 5;// 规则行
    public static final int NAME_ROW = 6;// 名字行
    public static final int TYPE_ROW = 7;// 类型行
}
