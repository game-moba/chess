package com.three.constant;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @Author mathua
 * @Date 2017/5/19 15:29
 */
public interface Constants {
    Charset UTF_8 = StandardCharsets.UTF_8;
    byte[] EMPTY_BYTES = new byte[0];
    String HTTP_HEAD_READ_TIMEOUT = "readTimeout";
    String EMPTY_STRING = "";
    String ANY_HOST = "0.0.0.0";
    long ILLEGAL_PLAYER_ID = -1;// 非法玩家id
}
