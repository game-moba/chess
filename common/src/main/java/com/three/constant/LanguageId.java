package com.three.constant;

/**
 * 语言包id常量
 * Created by Mathua on 2017/6/19.
 */
public interface LanguageId {
    // 1-5000 提示类语言包
    long PARAM_INVALID = 1;//无效参数
    long REPEAT_LOGIN = 2;//重复登陆
    long LOGIN_SUCCESS = 3;// 登录成功
    long LOGIN_FAILED = 4;// 登录失败
    long NOT_LOGIN = 5;// 未登录
    long INVALID_ACTION = 6;// 无效操作
    long LOGOUT_SUCCESS = 7;// 登出成功
    long LOGOUT_FAILED = 8;// 登出失败
    long SESSION_EXPIRED = 9;// 会话过期
    long INVALID_DEVICE = 10;// 无效设备
    long PARAM_VALUE_ERROR = 11;// 参数值出错:%s
    long CHANGE_PLAYER_INFO_SUCCESS = 12;// 角色信息修改成功
    long HAND_OUT_FAIL_BY_OVER_FLOW = 13;// %s领取失败，领取将超出上限!
    long OVER_MAIL_SUBJECT_LIMIT = 14;// 邮件主题超限制
    long OVER_MAIL_CONTENT_LIMIT = 15;// 邮件内容超限制
    long MAIL_NOT_EXIST = 16;// 邮件不存在
    long FAIL_TO_OBTAIN_ITEMS = 17;// 领取物品失败
    long HAD_READ_MAIL = 18;// 邮件已读
    long CAN_NOT_OBTAIN_MAIL_ITEMS = 19;// 无法领取邮件附件
    long MAIL_CONTENT_FOR_NO_ITEMS_SPACE = 20;// 由于物品达到上限，尚未领取的物品已经暂存在邮件。
    long MAIL_SUBJECT_FOR_NO_ITEMS_SPACE = 21;// 物品暂存邮件通知
    long NO_SEAT_IN_ROOM = 22;// 房间人数已满，加入失败！
    long FAIL_TO_CREATE_ROOM_BY_SYSTEM_ERROR = 23;// 系统内部错误造成创建房间失败。
    long THE_ROOM_NOT_EXIST = 24;// 房间不存在
    long YOU_NOT_JOIN_ANOTHER_ROOM = 25;// 你并未加入任何房间
    long YOU_ARE_IN_GAME = 26;// 你已在游戏中
    long YOU_NO_AUTHORITY_DISSOLVED_ROOM = 27;// 你没有权限解散房间
    long YOU_HAD_DISSOLVED = 28;// 你已经投过票啦
    long CAN_NOT_VOTE_TO_DISSOLVED_AGAIN = 29;// 不能重复发起投票
    long NOT_MEMBER_VOTE_TO_DISSOLVED = 30;// 无人发起投票解散
    long YOU_HAD_READY = 31;// 你已经进入游戏准备啦

    // 5001-6000 常用物品命名
    long GOLD = 5001;// 金币
    long DIAMOND = 5002;// 钻石

    // 模块2的语言包id
    long TASK_NOT_EXIST=100;//不存在的任务
    long TASK_REPEAT_REWARD=101;//重复领取
    // 模块3的语言包id
    long ITEM_NOT_EXIST=200;//商品不存在
    long NOT_ENOUGH_MONEY=201;//余额不足（金币、钻石等）

}
