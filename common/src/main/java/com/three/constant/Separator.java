package com.three.constant;

/**
 * Created by wangziqing on 2017/5/21 0021.
 */
public interface Separator {
    String COMMA=",";
    String POINT=".";
    String UNDERLINE="_";
}
