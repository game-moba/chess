package com.three.core.event;

import java.util.HashMap;
import java.util.Map;

/**
 * 事件分发
 * 
 * @author pzt
 * 
 */
public class DispatchEvent {


	/**
	 * 事件处理器
	 */
	private static Map<Integer, Map<Integer, EventHandler>> handlers = new HashMap<Integer, Map<Integer, EventHandler>>();

	/**
	 * 注册事件处理器
	 * 
	 * @param eventType
	 * @param handler
	 */
	public static synchronized void addEventListener(int eventType,
			EventHandler handler) {
		Map<Integer, EventHandler> map = handlers.get(eventType);
		if (map == null) {
			map = new HashMap<Integer, EventHandler>();
			handlers.put(eventType, map);
		}
		map.put(handler.getHandle(), handler);
	}

	/**
	 * 发送事件
	 * 
	 * @param event
	 *            事件
	 * @return 成功发送返回true
	 */
	public static int dispacthEvent(Event event) {
		Map<Integer, EventHandler> map = handlers.get(event.getEventType());
		if (map == null) {
			//LogUtil.debug("不存事件处理器类型 [{}]"+event.getEventType());
			event.setFlag(EventConstant.FAIL);
			return EventConstant.FAIL;
		}
		EventHandler handler = map.get(event.getHandle());
		if (handler == null) {
			/*LogUtil.debug("事件处理器类型 [{}] 不存在处理器 [{}]"+event.getEventType()+ event
					.getHandle());*/
			event.setFlag(EventConstant.FAIL);
			return EventConstant.FAIL;
		}
		return handler.execute(event);
	}

    /**
     * 获得事件处理器
     *
     * @param event
     * @param handle
     * @return
     */
	public static EventHandler getEventHandler(int event, int handle) {
        Map<Integer, EventHandler> map = handlers.get(event);
        if (map != null) {
            return map.get(handle);
        }
        return null;
    }
}
