package com.three.core.event;

/**
 * @author pzt
 * 
 */
public interface EventConstant {

	/**
	 * 成功
	 */
	public final static int SUCCESS = 0;

	/**
	 * 失败
	 */
	public final static int FAIL = -10000;
}
