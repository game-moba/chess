/**
 * 
 */
package com.three.core.event;




/**
 * 事件处理器
 * 
 * @author pzt
 * 
 */
public interface EventHandler {

	/**
	 * 执行处理器
	 * 
	 * @param event
	 */
	int execute(Event event);

	/**
	 * 处理器句柄
	 * 
	 * @return
	 */
	int getHandle();

}
