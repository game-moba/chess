/**
 * 
 */
package com.three.core.thread;

import com.three.utils.LogUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;



/**
 * @author pzt
 *
 */
public abstract class SingleThreadTask implements Runnable {

	public void run() {
		    long time =System.currentTimeMillis();
		    try {
		    	doTask();
				long dif =System.currentTimeMillis()-time;
				if(dif>1000){
					
					LogUtils.Console.info(getSingleID()+"任务消耗缓慢！！！"+dif);
				}
			} catch (Exception e) {
				LogUtils.Console.error(ExceptionUtils.getStackFrames(e).toString());
			}
			//LogUtil.info(singleData.getSingleId()+"任务消耗"+(System.currentTimeMillis()-time));
//			LogUtil.debug(match.getId() + " run in " + Thread.currentThread().getName());
	}
	
	public abstract int getSingleID();
	
	public abstract void doTask();

}
