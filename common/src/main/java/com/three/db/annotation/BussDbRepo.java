package com.three.db.annotation;

import java.lang.annotation.*;

/**
 * Created by ziqingwang on 2017/5/18 0018.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BussDbRepo {
    String name() default "buss";
}
