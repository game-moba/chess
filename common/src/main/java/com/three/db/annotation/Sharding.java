package com.three.db.annotation;

import com.dangdang.ddframe.rdb.sharding.api.strategy.database.DatabaseShardingAlgorithm;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.TableShardingAlgorithm;
import com.three.db.shard.base.ShardingType;

import java.lang.annotation.*;
import java.time.temporal.ChronoUnit;

/**
 * 分库分表注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Sharding {
    //数据库逻辑名
    String db_name()default "";
    //分库数量
    int shard_db_num() default 0;
    //分表数量
    int shard_tb_num() default 0;
    //分库实际命名
    String shard_db_name()default "";
    //分表实际命名
    String shard_tb_name()default "";
    //分库规则
    ShardingType shard_db_type() default ShardingType.SELF;
    //分表规则
    ShardingType shard_tb_type() default ShardingType.SELF;
    //自定义分库规则
    Class<?extends DatabaseShardingAlgorithm> shard_db_clazz()default DatabaseShardingAlgorithm.class;
    //自定义分表规则
    Class<?extends TableShardingAlgorithm> shard_tb_clazz()default TableShardingAlgorithm.class;
    //时间分库单位
    ChronoUnit shard_db_unit()default ChronoUnit.MONTHS;
    //时间分表单位
    ChronoUnit shard_tb_unit()default ChronoUnit.MONTHS;
}
