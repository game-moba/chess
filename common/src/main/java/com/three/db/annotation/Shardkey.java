package com.three.db.annotation;

import java.lang.annotation.*;

import static com.three.db.annotation.Shardkey.TYPE.ALL;

/**
 * 分片键标识
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Shardkey {
    enum TYPE{
        DB,TB,ALL;
    }
    TYPE type()default ALL;
}
