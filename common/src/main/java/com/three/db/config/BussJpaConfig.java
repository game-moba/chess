package com.three.db.config;


import com.three.db.annotation.BussDbRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * <p>数据库JPA配置</p>
 * 通过不同的数据源，支持多数据源
 * Created by ziqingwang on 2017/3/6.
 */
@Configuration
//启动项目配置  db.ordinary或者db.sharding,存在该数据库，才配置
@DataSourceConditional(db_name = {"buss"})
@EnableConfigurationProperties(JpaProperties.class)
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory_buss",
        transactionManagerRef = "transactionManager_buss",
        basePackages ={"com.three.**"}, includeFilters={@ComponentScan.Filter(type = FilterType.ANNOTATION, value = BussDbRepo.class)}//不指定包，只会扫描当前目录下的包和类
)
public class BussJpaConfig {
    private final String dbKey="buss";
    @Autowired
    private DataSourceConfig dataSourceConfig;
    @Autowired
    private JpaProperties jpaProperties;
    @Bean(name="entityManagerFactory_buss")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryPrimary(EntityManagerFactoryBuilder builder,@Qualifier(value = "bussDataSource")DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .properties(jpaProperties.getHibernateProperties(dataSource))
                .packages("com.three.**") //设置实体类所在位置
                .persistenceUnit("buss")
                .build();
    }

    @Bean(name="transactionManager_buss")
    public PlatformTransactionManager transactionManagerPrimary(@Qualifier(value = "entityManagerFactory_buss") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
    @Bean(name="bussDataSource")
    public DataSource dataSource(){
        return dataSourceConfig.getDataSource(dbKey);
    }
}
