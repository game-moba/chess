package com.three.db.config;


import com.three.db.annotation.ChessDbRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * <p>数据库JPA配置</p>
 * Created by ziqingwang on 2017/3/6.
 */
@Configuration
////启动项目配置  db.ordinary或者db.sharding,存在该数据库，才配置
@DataSourceConditional(db_name = {"chess"})
@EnableConfigurationProperties(JpaProperties.class)
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactory_chess",
        transactionManagerRef = "transactionManager_chess",
        basePackages ={"com.three.**"}, includeFilters={@ComponentScan.Filter(type = FilterType.ANNOTATION, value = ChessDbRepo.class)}//不指定包，只会扫描当前目录下的包和类
) //设置Repository所在位置
public class ChessJpaConfig {
	private final String dbKey="chess";
    @Autowired
    private JpaProperties jpaProperties;
    @Autowired
    private DataSourceConfig dataSourceConfig;

    @Bean(name="entityManagerFactory_chess")
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryPrimary(EntityManagerFactoryBuilder builder,@Qualifier(value = "chessDataSource")DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .properties(jpaProperties.getHibernateProperties(dataSource))
                .packages("com.three.**") //设置实体类所在位置
                .persistenceUnit("chess")
                .build();
    }

    @Bean(name="transactionManager_chess")
    @Primary
    public PlatformTransactionManager transactionManagerPrimary(@Qualifier(value = "entityManagerFactory_chess") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
    @Bean(name="chessDataSource")
    @Primary
    public DataSource dataSource(){
        return dataSourceConfig.getDataSource(dbKey);
    }
}
