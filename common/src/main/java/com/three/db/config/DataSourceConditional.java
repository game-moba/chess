package com.three.db.config;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * Created by ziqingwang on 2017/6/19 0019.
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(DataSourceExistCondition.class)
public @interface DataSourceConditional {
    String[] db_name() default {};
}
