package com.three.db.schema;

import org.hibernate.dialect.Dialect;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.ServiceRegistryAwareService;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.tool.schema.internal.*;
import org.hibernate.tool.schema.spi.*;

import java.util.Map;

public class SupportShardingHibernateSchemaManagerTool  implements SchemaManagementTool, ServiceRegistryAwareService {
    private ServiceRegistry serviceRegistry;

    @Override
    public SchemaCreator getSchemaCreator(Map options) {
        return new SchemaCreatorImpl();
    }

    @Override
    public SchemaDropper getSchemaDropper(Map options) {
        return new SchemaDropperImpl();
    }

    @Override
    public SchemaMigrator getSchemaMigrator(Map options) {
        return new SupportShardingSchemaMigratorImpl();
    }

    @Override
    public SchemaValidator getSchemaValidator(Map options) {
        final Dialect dialect = serviceRegistry.getService( JdbcServices.class ).getDialect();
        return new SupportShardingSchemaValidatorImpl(dialect);
    }

    @Override
    public void injectServices(ServiceRegistryImplementor serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
