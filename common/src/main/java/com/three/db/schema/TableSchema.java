package com.three.db.schema;

import java.util.Set;

/**
 * Created by ziqingwang on 2017/6/20 0020.
 */
public class TableSchema {
    private String logicName;
    private Set<String> shardingTable;
    private Set<String> shardingDataBase;

    public String getLogicName() {
        return logicName;
    }

    public void setLogicName(String logicName) {
        this.logicName = logicName;
    }

    public Set<String> getShardingDataBase() {
        return shardingDataBase;
    }

    public void setShardingDataBase(Set<String> shardingDataBase) {
        this.shardingDataBase = shardingDataBase;
    }

    public Set<String> getShardingTable() {
        return shardingTable;
    }

    public void setShardingTable(Set<String> shardingTable) {
        this.shardingTable = shardingTable;
    }
}
