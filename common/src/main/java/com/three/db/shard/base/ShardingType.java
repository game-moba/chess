package com.three.db.shard.base;

public enum ShardingType {
    //取模
    MOLD,
    //时间
    TIME,
    //自定义
    SELF,
    //不分片
    NONE;
}
