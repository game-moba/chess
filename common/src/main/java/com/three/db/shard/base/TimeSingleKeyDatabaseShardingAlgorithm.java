package com.three.db.shard.base;

import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;

import java.time.temporal.ChronoUnit;

public interface TimeSingleKeyDatabaseShardingAlgorithm <T extends Comparable<?>> extends SingleKeyDatabaseShardingAlgorithm<T> {
    boolean match(ChronoUnit chronoUnit);
}
