package com.three.db.shard.base;

import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;

import java.time.temporal.ChronoUnit;

public interface TimeSingleKeyTableShardingAlgorithm<T extends Comparable<?>> extends SingleKeyTableShardingAlgorithm<T>{
    boolean match(ChronoUnit chronoUnit);
}
