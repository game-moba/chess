package com.three.db.shard.base;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TimeUtils {
    /**
     * @param date
     * @return 201701-2017-12
     */
    public static String getMounthKey(Date date){
        LocalDateTime time = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return time.getYear() + time.getMonthValue() > 9 ? String.valueOf(time.getMonthValue()) : "0" + time.getMonthValue();
    }

    /**
     * @param date
     * @return 01-12
     */
    public static int getMounth(Date date){
        LocalDateTime time = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return time.getMonthValue();
    }
}
