package com.three.db.shard.time;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.three.db.shard.base.TimeSingleKeyDatabaseShardingAlgorithm;

import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;

/**
 * Created by ziqingwang on 2017/6/1 0001.
 */
public class TimeDbShardingAlgorithm implements SingleKeyDatabaseShardingAlgorithm<Date> {
    private ChronoUnit timeUnit;
    private TimeSingleKeyDatabaseShardingAlgorithm<Date> timeSingleKeyDatabaseShardingAlgorithm;
    public TimeDbShardingAlgorithm(ChronoUnit timeUnit){
        this.timeUnit=timeUnit;
        this.timeSingleKeyDatabaseShardingAlgorithm = TimeShardingHandlerManager.get4Db(timeUnit);
    }
    /**
     *
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    @Override
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyDatabaseShardingAlgorithm.doEqualSharding(availableTargetNames,shardingValue);
    }

    @Override
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyDatabaseShardingAlgorithm.doInSharding(availableTargetNames,shardingValue);
    }

    @Override
    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyDatabaseShardingAlgorithm.doBetweenSharding(availableTargetNames,shardingValue);
    }
}
