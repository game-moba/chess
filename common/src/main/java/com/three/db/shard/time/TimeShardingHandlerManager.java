package com.three.db.shard.time;

import com.google.common.collect.Lists;
import com.three.db.shard.base.TimeSingleKeyDatabaseShardingAlgorithm;
import com.three.db.shard.base.TimeSingleKeyTableShardingAlgorithm;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class TimeShardingHandlerManager {
    private static final List<TimeSingleKeyDatabaseShardingAlgorithm> DbHandlers= Lists.newArrayList();
    private static final List<TimeSingleKeyTableShardingAlgorithm> TbHandler= Lists.newArrayList();
    static{
        //db
        DbHandlers.add(new DbShardingAlgorithm4Mounth());
        //tb
        TbHandler.add(new TbShardingAlgorithm4Mounth());
    }
    public static TimeSingleKeyDatabaseShardingAlgorithm get4Db(ChronoUnit chronoUnit){
        return DbHandlers.stream()
                .filter(h->h.match(chronoUnit))
                .findFirst()
                .get();
    }
    public static TimeSingleKeyTableShardingAlgorithm get4Tb(ChronoUnit chronoUnit){
        return TbHandler.stream()
                .filter(h->h.match(chronoUnit))
                .findFirst()
                .get();
    }
}
