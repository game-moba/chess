package com.three.db.shard.time;

import com.dangdang.ddframe.rdb.sharding.api.ShardingValue;
import com.dangdang.ddframe.rdb.sharding.api.strategy.database.SingleKeyDatabaseShardingAlgorithm;
import com.dangdang.ddframe.rdb.sharding.api.strategy.table.SingleKeyTableShardingAlgorithm;
import com.three.db.shard.base.TimeSingleKeyDatabaseShardingAlgorithm;
import com.three.db.shard.base.TimeSingleKeyTableShardingAlgorithm;

import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;

/**
 * Created by ziqingwang on 2017/6/1 0001.
 */
public class TimeTbShardingAlgorithm implements SingleKeyTableShardingAlgorithm<Date> {
    private ChronoUnit timeUnit;
    private TimeSingleKeyTableShardingAlgorithm<Date> timeSingleKeyTableShardingAlgorithm;
    public TimeTbShardingAlgorithm(ChronoUnit timeUnit){
        this.timeUnit=timeUnit;
        this.timeSingleKeyTableShardingAlgorithm = TimeShardingHandlerManager.get4Tb(timeUnit);
    }
    /**
     *
     * @param availableTargetNames
     * @param shardingValue
     * @return
     */
    @Override
    public String doEqualSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyTableShardingAlgorithm.doEqualSharding(availableTargetNames,shardingValue);
    }

    @Override
    public Collection<String> doInSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyTableShardingAlgorithm.doInSharding(availableTargetNames,shardingValue);
    }

    @Override
    public Collection<String> doBetweenSharding(Collection<String> availableTargetNames, ShardingValue<Date> shardingValue) {
        return timeSingleKeyTableShardingAlgorithm.doBetweenSharding(availableTargetNames,shardingValue);
    }
}
