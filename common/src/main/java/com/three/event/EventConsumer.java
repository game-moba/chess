package com.three.event;

/**
 * 事件消费者抽象类
 * Created by mathua on 2017/5/25.
 */
public abstract class EventConsumer {

    public EventConsumer() {
        EventBus.I.register(this);
    }
}
