package com.three.exception;

/**
 * @Author mathua
 * @Date 2017/5/23 19:52
 */
public class BootException extends RuntimeException {
    public BootException(String s) {
        super(s);
    }

    public BootException(String message, Throwable cause) {
        super(message, cause);
    }
}
