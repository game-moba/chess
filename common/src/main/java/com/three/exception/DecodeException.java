package com.three.exception;

/**
 * @Author mathua
 * @Date 2017/5/19 16:20
 */
public class DecodeException extends RuntimeException {
    public DecodeException(String message) {
        super(message);
    }
}