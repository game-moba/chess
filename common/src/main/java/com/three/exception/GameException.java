package com.three.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author mathua
 * @Date 2017/5/18 19:41
 */
public class GameException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String errorMsg;
    private short errorId;
    private String[] params;

    public GameException(long errorId, String... params) {
        this.errorId = (short) errorId;
        this.params = params;
    }

    public GameException(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public short getErrorId() {
        return errorId;
    }

    public String[] getParams() {
        return params;
    }
}