package com.three.exception;

/**
 * @Author mathua
 * @Date 2017/5/19 16:09
 */
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String format, Object... args){
        super(String.format(format,args));
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(Throwable cause,String message) {
        super(message, cause);
    }

    public ServiceException(Throwable cause,String format, Object... args){
        super(String.format(format,args),cause);
    }
}
