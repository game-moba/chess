package com.three.function;


import com.google.protobuf.MessageLite;
import com.three.api.protocol.Packet;
import com.three.api.push.PushResult;
import com.three.api.push.PushSender;
import com.three.protocol.CommandEnum;
import com.three.utils.LogUtils;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.concurrent.FutureTask;

import static com.three.protocol.CommandEnum.*;

/**
 * Created by YW0981 on 2017/7/18.
 */
@Component
public class PromptFunction {

    private static PushSender sender = PushSender.get();

    public void sendMessage(Command cmd, MessageLite msg, long rid) {
        try {
            Packet packet = Packet.build(cmd, msg.toByteArray());
            FutureTask<PushResult> result = sender.send(packet, rid);
            LogUtils.Console.debug("send " + msg.getClass().getSimpleName() + " to rid:" +
                    rid + ", cmd:" + cmd + ", state:" + result.get().toString());
        }catch (Exception e) {
            LogUtils.Console.error(e.toString());
        }

    }

    public void sendMessage(Command cmd, MessageLite msg, Collection<Long> rids) {
        if(rids != null && rids.size() > 0) {
            for(Long rid : rids) {
                sendMessage(cmd, msg, rid);
            }
        }else {
            LogUtils.Console.error("rids is null");
        }
    }
}
