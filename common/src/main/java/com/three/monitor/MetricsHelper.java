package com.three.monitor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.actuate.trace.TraceRepository;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by ziqingwang on 2017/1/5.
 */
@Component
public class MetricsHelper {
    /**
     * 自定义自己的指标
     * GaugeService提供一个submit方法。记录应用程序中动态的瞬时值
     */
    @Autowired
    private GaugeService gaugeService;
    /**
     * 自定义追踪事件
     * add方法接收一个将被转化为JSON的Map结构，该数据将被记录下来。
     * 默认情况下，使用的InMemoryTraceRepository将存储最新的100个事件。
     * 如果需要扩展该容量，你可以定义自己的InMemoryTraceRepository实例。
     * 如果需要，你可以创建自己的替代TraceRepository实现。
     */
    @Autowired
    private TraceRepository traceRepository;
    /*
    * CounterService暴露increment，decrement和reset方法；
    * 记录应用程序中动态的变动值
    */
    @Autowired
    private CounterService counterService;

    public void addTraceData(Map<String,Object> data){
        traceRepository.add(data);
    }

    public void submitData(String metricName,double value){
        gaugeService.submit(metricName,value);
    }

    public void increment(String key){
        if(StringUtils.isNotEmpty(key)){
            counterService.increment(key);
        }
    }
    public void decrement(String key){
        if(StringUtils.isNoneEmpty(key)){
            counterService.decrement(key);
        }
    }
}
