package com.three.monitor;

import java.lang.annotation.*;

/**
 * 此注解，使用AOP记录方法访问
 * Created by ziqingwang on 2017/1/5.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MetricsMethod {
    String value()default "";
}
