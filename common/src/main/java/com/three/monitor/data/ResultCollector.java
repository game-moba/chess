package com.three.monitor.data;

import com.three.monitor.quota.impl.*;

/**
 * 结果收集者
 * Created by Mathua on 2017/5/25.
 */
public class ResultCollector {
    public MonitorResult collect() {
        MonitorResult result = new MonitorResult();
        result.addResult("jvm-info", JVMInfo.I.monitor());
        result.addResult("jvm-gc", JVMGC.I.monitor());
        result.addResult("jvm-memory", JVMMemory.I.monitor());
        result.addResult("jvm-thread", JVMThread.I.monitor());
        result.addResult("jvm-thread-pool", JVMThreadPool.I.monitor());
        return result;
    }
}
