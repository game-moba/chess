package com.three.monitor.jmx;

/**
 * Created by mathua on 2017/5/26.
 */
public interface MBeanInfo {
    String getName();
}
