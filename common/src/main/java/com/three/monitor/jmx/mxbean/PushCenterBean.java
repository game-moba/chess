package com.three.monitor.jmx.mxbean;

import com.three.monitor.jmx.MBeanInfo;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Mathua on 2017/5/26.
 */
public final class PushCenterBean implements PushCenterMXBean, MBeanInfo {
    private final AtomicLong taskNum;

    public PushCenterBean(AtomicLong taskNum) {
        this.taskNum = taskNum;
    }

    @Override
    public String getName() {
        return "PushCenter";
    }

    @Override
    public long getTaskNum() {
        return taskNum.get();
    }
}
