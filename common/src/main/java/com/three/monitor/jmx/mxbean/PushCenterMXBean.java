package com.three.monitor.jmx.mxbean;

/**
 * Created by Mathua on 2017/5/26.
 */
public interface PushCenterMXBean {
    long getTaskNum();
}

