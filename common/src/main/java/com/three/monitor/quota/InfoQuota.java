package com.three.monitor.quota;

/**
 * Created by Mathua on 2017/5/25.
 */
public interface InfoQuota extends MonitorQuota {

    String pid();

    double load();
}
