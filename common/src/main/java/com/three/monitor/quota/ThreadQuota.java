package com.three.monitor.quota;

/**
 * 线程监控指标
 * Created by Mathua on 2017/5/25.
 */
public interface ThreadQuota extends MonitorQuota {

    int daemonThreadCount();

    int threadCount();

    long totalStartedThreadCount();

    int deadLockedThreadCount();

}
