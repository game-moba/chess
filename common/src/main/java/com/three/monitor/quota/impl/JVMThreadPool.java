package com.three.monitor.quota.impl;

import com.three.monitor.quota.ThreadPoolQuota;
import com.three.tools.thread.pool.ThreadPoolManager;
import io.netty.channel.EventLoopGroup;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import static com.three.tools.thread.pool.ThreadPoolManager.getPoolInfo;

/**
 * Created by Mathua on 2017/5/25.
 */
public class JVMThreadPool implements ThreadPoolQuota {
    public static final JVMThreadPool I = new JVMThreadPool();

    private JVMThreadPool() {
    }

    @Override
    public Object monitor(Object... args) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Executor> pools = ThreadPoolManager.I.getActivePools();
        for (Map.Entry<String, Executor> entry : pools.entrySet()) {
            String serviceName = entry.getKey();
            Executor executor = entry.getValue();
            if (executor instanceof ThreadPoolExecutor) {
                result.put(serviceName, getPoolInfo((ThreadPoolExecutor) executor));
            } else if (executor instanceof EventLoopGroup) {
                result.put(serviceName, getPoolInfo((EventLoopGroup) executor));
            }
        }
        return result;
    }
}
