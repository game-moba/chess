package com.three.tools.common;

import com.three.api.spi.Spi;
import com.three.api.spi.common.Json;
import com.three.api.spi.common.JsonFactory;
import com.three.utils.JsonUtils;

/**
 * Created by Mathua on 2017/6/15.
 */
@Spi
public final class DefaultJsonFactory implements JsonFactory, Json {
    @Override
    public <T> T fromJson(String json, Class<T> clazz) {
        return JsonUtils.fromJson(json, clazz);
    }

    @Override
    public String toJson(Object json) {
        return JsonUtils.toJson(json);
    }

    @Override
    public Json get() {
        return this;
    }
}
