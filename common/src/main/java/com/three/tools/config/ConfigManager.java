package com.three.tools.config;

import com.three.config.common.IConfig;
import com.three.tools.Utils;

/**
 * Created by mathua on 2017/5/25.
 */
public class ConfigManager {
    public static final ConfigManager I = new ConfigManager();

    private ConfigManager() {
    }

    public int getHeartbeat() {
        return Math.max(IConfig.chess.core.min_heartbeat , IConfig.chess.core.max_heartbeat);
    }

    /**
     * 获取内网IP地址
     *
     * @return 内网IP地址
     */
    public String getLocalIp() {
        return Utils.getLocalIp();
    }

    /**
     * 获取外网IP地址
     *
     * @return 外网IP地址
     */
    public String getPublicIp() {

//        String localIp = Utils.getLocalIp();
//
//        String remoteIp = Utils.getExtranetIp();
//
//        return remoteIp == null ? localIp : remoteIp;
        return Utils.getZkServerIp();
    }
}

