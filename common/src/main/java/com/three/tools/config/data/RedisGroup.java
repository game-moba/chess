package com.three.tools.config.data;

import org.springframework.data.redis.connection.RedisNode;

import java.util.Collections;
import java.util.List;

/**
 * redis 组
 */
public class RedisGroup {
    public List<RedisNode> redisNodeList = Collections.emptyList();

    public RedisGroup() {
    }

    public RedisGroup(List<RedisNode> redisNodeList) {
        this.redisNodeList = redisNodeList;
    }

    @Override
    public String toString() {
        return "RedisGroup{" +
                "redisNodeList=" + redisNodeList +
                '}';
    }
}
