package com.three.utils;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * ???????
 * 
 * @author shenlong
 *
 */
public class DateUtils {

	/**
	 * һ???ĺ???
	 */
	public static final int ONE_SECOND_MILLISECOND = 1000;

	/**
	 * һ??ӵĺ???
	 */
	public static final int ONE_MINUTE_MILLISECOND = 60 * ONE_SECOND_MILLISECOND;

	/**
	 * һСʱ?ĺ???
	 */
	public static final int ONE_HOUR_MILLISECOND = 60 * ONE_MINUTE_MILLISECOND;

	/**
	 * һ??ĺ???
	 */
	public static final int ONE_DAY_MILLISECOND = 24 * ONE_HOUR_MILLISECOND;

	/**
	 * һ??ӵ???
	 */
	public static final int ONE_MINUTE_SECOND = ONE_MINUTE_MILLISECOND / ONE_SECOND_MILLISECOND;

	/**
	 * 1Сʱ????
	 */
	public static final long ONE_HOUR_SECOND = ONE_HOUR_MILLISECOND / ONE_SECOND_MILLISECOND;

	/**
	 * 1?????
	 */
	public static final long ONE_DAY_SECOND = ONE_DAY_MILLISECOND / ONE_SECOND_MILLISECOND;

	/**
	 * ??????ʱ??ǲ??ͬһ?
	 * 
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static boolean isSameDate(Date time1, Date time2) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(time1);
		int year1 = cal.get(Calendar.YEAR);
		int month1 = cal.get(Calendar.MONTH);
		int date1 = cal.get(Calendar.DAY_OF_YEAR);

		cal.setTime(time2);
		int year2 = cal.get(Calendar.YEAR);
		int month2 = cal.get(Calendar.MONTH);
		int date2 = cal.get(Calendar.DAY_OF_YEAR);

		return year1 == year2 && month1 == month2 && date1 == date2;
	}

	public static Date add(Date theDate, int addHours, int addMinutes, int addSecond) {
		if (theDate == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(theDate);

		cal.add(HOUR_OF_DAY, addHours);
		cal.add(MINUTE, addMinutes);
		cal.add(SECOND, addSecond);

		return cal.getTime();
	}

	public static Date add(Date theDate, int days, int addHours, int addMinutes, int addSecond) {
		if (theDate == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(theDate);

		cal.add(Calendar.DATE, days);
		cal.add(HOUR_OF_DAY, addHours);
		cal.add(MINUTE, addMinutes);
		cal.add(SECOND, addSecond);

		return cal.getTime();
	}

	/**
	 * ??ĳһʱ??0??
	 * 
	 * @param theDate
	 *            ?Ҫ???ʱ??
	 */
	public static Date getDate0AM(Date theDate) {
		if (theDate == null) {
			return null;
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(theDate);
		return new GregorianCalendar(cal.get(YEAR), cal.get(MONTH), cal.get(DAY_OF_MONTH)).getTime();
	}

	/**
	 * ?????ʱ???????,???????????????ʱ???????????)
	 * 
	 * @param startDate
	 *            ?ʼʱ??
	 * @param endDate
	 *            ???ʱ??
	 */
	public static int calcIntervalDay(Date startDate, Date endDate) {
		if (startDate == null || endDate == null) {
			return 0;
		}
		Date startDate0AM = getDate0AM(startDate);
		Date endDate0AM = getDate0AM(endDate);
		long v1 = startDate0AM.getTime() - endDate0AM.getTime();

		BigDecimal bd1 = new BigDecimal(Math.abs(v1));
		BigDecimal bd2 = new BigDecimal(ONE_DAY_MILLISECOND);

		int days = (int) bd1.divide(bd2, 0, BigDecimal.ROUND_UP).doubleValue();
		return days;
	}
	
	public static long nextZeroTime(long time){
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		cal.set(Calendar.HOUR,0);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.HOUR, 24);
		return cal.getTimeInMillis();
	}

}