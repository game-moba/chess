package com.three.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

/**
 * 文件工具类<br>
 *
 * @Author mathua
 * @Date 2017/5/18 19:37
 */
public final class FileUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    /**
     * 获取相对路径的文件夹
     */
    public static File readDirectoryByClasspath(String relativePath) {
        try {
            Resource resource = new ClassPathResource("/" + relativePath);
            return resource.getFile();
        } catch (IOException e) {
            return new File(relativePath);
        }
    }

}