package com.three.utils;

import com.three.config.common.IConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ch.qos.logback.classic.util.ContextInitializer.CONFIG_FILE_PROPERTY;

/**
 * Created by mathua on 2017/5/22.
 */
public interface LogUtils {
    boolean logInit = init();

    static boolean init() {
        if (logInit) return true;
        System.setProperty("log.home", IConfig.chess.log_dir);
        System.setProperty("log.root.level", IConfig.chess.log_level);
        System.setProperty(CONFIG_FILE_PROPERTY, IConfig.chess.log_conf_path);
        LoggerFactory
                .getLogger("console")
                .info("init logs!");
        return true;
    }

    Logger Console = LoggerFactory.getLogger("console"),

    CONN = LoggerFactory.getLogger("chess.conn.log"),

    MONITOR = LoggerFactory.getLogger("chess.monitor.log"),

    PUSH = LoggerFactory.getLogger("chess.push.log"),

    HB = LoggerFactory.getLogger("chess.heartbeat.log"),

    CACHE = LoggerFactory.getLogger("chess.cache.log"),

    RSD = LoggerFactory.getLogger("chess.srd.log"),

    HTTP = LoggerFactory.getLogger("chess.http.log"),

    PROFILE = LoggerFactory.getLogger("chess.profile.log"),

    ZOOKEEPER = LoggerFactory.getLogger("chess.zookeeper.log"),

    SHOP = LoggerFactory.getLogger("chess.shop.log");
}
