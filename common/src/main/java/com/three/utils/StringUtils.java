package com.three.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author mathua
 * @Date 2017/5/18 19:47
 */
public final class StringUtils {
    private static String[] chinesePunctuationList = {"“", "”", "‘", "’", "。", "，", "；", "：", "？", "！", "……", "—", "～",
            "（", "）", "《", "》", "【", "】"};
    private static String[] englishPunctuationList = {"\"", "\"", "'", "'", ".", ",", ";", ":", "?", "!", "…", "-",
            "~", "(", ")", "<", ">", "[", "]"};
    public static final String EMPTY = "";

    /**
     * 替换中文符号为英文符号
     *
     * @param str
     * @return
     */
    public static String replaceChinesePunctuation(String str) {
        String tmp = str;
        for (int i = 0; i < chinesePunctuationList.length; ++i) {
            tmp = tmp.replace(chinesePunctuationList[i], englishPunctuationList[i]);
        }
        return tmp;
    }

    public static boolean isBlank(CharSequence text) {
        if (text == null || text.length() == 0) return true;
        for (int i = 0, L = text.length(); i < L; i++) {
            if (!Character.isWhitespace(text.charAt(i))) return false;
        }
        return true;
    }

    public static String trimAll(CharSequence s) {
        if (s == null || s.length() == 0) return StringUtils.EMPTY;
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0, L = s.length(); i < L; i++) {
            char c = s.charAt(i);
            if (c != ' ') sb.append(c);
        }
        return sb.toString();
    }

    public static byte[] praseFromString(String value) {
        if(value.length() <= 2 || !value.startsWith("[") || !value.endsWith("]"))
            return new byte[0];
        try {
            String tmpValue = value.substring(1, value.length() - 1);
            String[] items = tmpValue.split("\\,");
            byte[] datas = new byte[items.length];
            for(int i = 0; i < items.length; ++i) {
                String val = items[i].trim();
                datas[i] = Byte.parseByte(val);
            }
            return datas;
        }catch(Exception e) {
            return new byte[0];
        }
    }

    /**
     * 验证手机号码
     * @param mobileNumber
     * @return
     */
    public static boolean checkMobileNumber(String mobileNumber){
        boolean flag = false;
        try{
            Pattern regex = Pattern.compile("^(((13[0-9])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8})|(0\\d{2}-\\d{8})|(0\\d{3}-\\d{7})$");
            Matcher matcher = regex.matcher(mobileNumber);
            flag = matcher.matches();
        } catch(Exception e){
            flag = false;
        }
        return flag;
    }
}
