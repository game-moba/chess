package com.three.zk.lock;

/**
 * Created by Mathua on 2017/6/29.
 */
public interface LockListener {
    /**
     * call back called when the lock
     * is acquired
     */
    public void lockAcquired();

    /**
     * call back called when the lock is
     * released.
     */
    public void lockReleased();
}
