package com.three.zk.lock;

import org.apache.zookeeper.KeeperException;

/**
 * Created by mathua on 2017/6/28.
 */
public interface ZooKeeperOperation {

    /**
     * Performs the operation - which may be involved multiple times if the connection
     * to ZooKeeper closes during this operation
     *
     * @return the result of the operation or null
     * @throws KeeperException
     * @throws InterruptedException
     */
    public boolean execute() throws KeeperException, InterruptedException;
}
