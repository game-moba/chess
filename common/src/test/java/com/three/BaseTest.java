package com.three;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * Created by ziqingwang on 2017/5/18 0018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CommonMain.class)
public class BaseTest {
    protected Logger logger = LoggerFactory.getLogger(getClass());
}
