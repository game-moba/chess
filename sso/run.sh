#!/bin/bash
#通过git config credential.helper store保存了账号密码
#sh run.sh 分支 项目
branches=$1
project=$2
jar=${project}".jar"
cd /data/source/chess
# 下载远程仓库的所有变动
git fetch
#切换分支
git checkout "origin/"${branches}
cd $project
#协议
#sh common-protobuf/src/main/
#打包
gradle bootRepackage
rm -rf /data/$project
mkdir /data/$project
cp build/libs/$jar /data/$project
#复制公共配置
cp -r ../common/build/resources/main/** /data/$project/
#覆盖模块配置
\cp -r build/resources/main/** /data/$project/
cd /data/$project
#启动
java -jar $jar &>> app.log