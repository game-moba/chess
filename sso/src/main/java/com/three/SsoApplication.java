package com.three;

import com.three.netty.bootstrap.Main;
import com.three.utils.ConfigUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * netstat -ano|findstr 3000
 * taskkill /pid 6492 /F
 */
@SpringBootApplication
public class SsoApplication {

	public static void main(String[] args) {
		start();
		SpringApplication.run(SsoApplication.class, args);
	}

	public static void start() {
		System.setProperty("io.netty.leakDetection.level", "PARANOID");
		System.setProperty("io.netty.noKeySetOptimization", "false");
		ConfigUtils.loadAllConfig("config", "com.three.config");
//		ConfigUtils.loadAllLuaConfig("config", "D:\\game_version\\chess-client\\client\\Chess\\Majiang\\Config");
		Main.main(null);
	}

}
