package com.three.client;

import com.three.api.srd.ServiceNode;
import com.three.common.security.CipherBox;
import com.three.netty.client.connect.ClientConfig;
import com.three.utils.LogUtils;
import io.netty.channel.ChannelFuture;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * Created by mathua on 2017/5/30.
 */
public class ConnClientTestMain {

    public static void main(String[] args) throws Exception {
        int count = 1;
        String playerPrefix = "";
        int printDelay = 1;
        boolean sync = true;

        if (args.length > 0) {
            count = NumberUtils.toInt("3", count);
        }

        if (args.length > 1) {
            playerPrefix = args[1];
        }

        if (args.length > 2) {
            printDelay = NumberUtils.toInt("1000", printDelay);
        }

        if (args.length > 3) {
            sync = !"1".equals(/*args[3]*/"1");
        }

        testConnClient(count, playerPrefix, printDelay, sync);
    }

    public void testConnClient() throws Exception {
        testConnClient(1, "", 1, true);
        LockSupport.park();
    }

    private static void testConnClient(int count, String playerPrefix, int printDelay, boolean sync) throws Exception {
        LogUtils.init();
        ConnClientBoot boot = new ConnClientBoot();
        boot.start().get();

        List<ServiceNode> serverList = boot.getServers();
        if (serverList.isEmpty()) {
            boot.stop();
            System.out.println("no chess server.");
            return;
        }

        if (printDelay > 0) {
            Executors.newSingleThreadScheduledExecutor()
                    .scheduleAtFixedRate(
                            () -> System.err.println(ConnClientChannelHandler.STATISTICS)
                            , 3, printDelay, TimeUnit.SECONDS
                    );
        }

        for (int i = 0; i < 1; i++) {
            String clientVersion = "1.0." + i;
            String osName = "android";
            String osVersion = "1.0.1";
            String deviceId = playerPrefix + "test-device-id-" + i;
            byte[] clientKey = CipherBox.I.randomAESKey();
            byte[] iv = CipherBox.I.randomAESIV();

            ClientConfig config = new ClientConfig();
            config.setClientKey(clientKey);
            config.setIv(iv);
            config.setClientVersion(clientVersion);
            config.setDeviceId(deviceId);
            config.setOsName(osName);
            config.setOsVersion(osVersion);

            int L = serverList.size();
            int index = (int) ((Math.random() % L) * L);
            ServiceNode node = serverList.get(index);

            ChannelFuture future = boot.connect(/*node.getAttr(ATTR_PUBLIC_IP)*/"127.0.0.1", node.getPort(), config);
            if (sync) future.awaitUninterruptibly();
        }
    }
}