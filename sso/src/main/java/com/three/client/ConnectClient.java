package com.three.client;

import com.google.common.eventbus.Subscribe;
import com.three.api.event.ConnectionCloseEvent;
import com.three.event.EventBus;
import com.three.netty.client.NettyTCPClient;
import com.three.netty.client.connect.ClientConfig;
import io.netty.channel.ChannelHandler;

/**
 * Created by mathua on 2017/5/30.
 */
public class ConnectClient extends NettyTCPClient {
    private final ConnClientChannelHandler handler;

    public ConnectClient(String host, int port, ClientConfig config) {
        handler = new ConnClientChannelHandler(config);
        EventBus.I.register(this);
    }

    @Override
    public ChannelHandler getChannelHandler() {
        return handler;
    }

    @Subscribe
    void on(ConnectionCloseEvent event) {
        this.stop();
    }

    @Override
    protected int getWorkThreadNum() {
        return 1;
    }
}

