package com.three.config;

import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 头像配置表
 * Created by mathua on 2017/6/10.
 */
public final class HeadPortraitConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(HeadPortraitConfig.class);
    private static Map<Long, HeadPortraitConfig> configs;

    public static final long DEFAULT_HEAD_PROTRAIT_ID = 1;// 默认头像id

    private int id;
    private int gender;// 性别
    private String iconName;// 头像名称

    public static HeadPortraitConfig getHeadPortraitConfig(long id) {
        return configs.get(id);
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        configs = ConfigUtils.getConfigs(HeadPortraitConfig.class);
        return configs != null;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        if(!configs.containsKey(DEFAULT_HEAD_PROTRAIT_ID)) {
            LOGGER.error("HeadPortraitConfig default HeadPortrait ID{} is not exist!", DEFAULT_HEAD_PROTRAIT_ID);
            return false;
        }
        for(HeadPortraitConfig config : configs.values()) {
            if(config.gender != GenderType.FEMALE && config.gender != GenderType.MALE) {
                LOGGER.error("HeadPortraitConfig gender type error!config id={}, gender={}!", config.id, config.gender);
                return false;
            }
        }
        return true;
    }

    public interface GenderType {
        int MALE = 0;
        int FEMALE = 1;
    }

    public int getId() {
        return id;
    }

    public int getGender() {
        return gender;
    }

    public String getIconName() {
        return iconName;
    }
}
