package com.three.config;

import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import com.three.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 语言包配置表
 * Created by Mathua on 2017/6/19.
 */
public final class LanguageConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageConfig.class);
    private static Map<Long, LanguageConfig> configs;

    private int id;
    private String text;// 文本

    public static String getText(long id) {
        LanguageConfig languageConfig = configs.get(id);
        return languageConfig != null ? languageConfig.text : StringUtils.EMPTY;
    }

    public static String replacePattern(String text, Object... params) {
        return String.format(text, params);
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        configs = ConfigUtils.getConfigs(LanguageConfig.class);
        return configs != null;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        return true;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}
