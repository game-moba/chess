package com.three.config;

import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 地区配置表
 * Created by mathua on 2017/6/10.
 */
public final class LocationSetConfig {
    private static Map<Integer, LocationSetConfig> configs = new HashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(LocationSetConfig.class);

    public static final int DEFAULT_PROVINCE_ID = 1;// 默认省份id

    private int id;
    private int provinceID;// 省份id
    private String provinceName;// 省份名字
    private List<String> cityName;// 城市名字

    public String getDefaultCityName() {
        return cityName.get(0);
    }

    public static LocationSetConfig getLocationSetConfig(int provinceID) {
        return configs.get(provinceID);
    }

    public static boolean check(int provinceID, String city) {
        LocationSetConfig config = getLocationSetConfig(provinceID);
        if(config == null)
            return false;
        for(String cityName : config.getCityName()) {
            if(cityName.equals(city))
                return true;
        }
        return false;
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        try {
            Map<Long, LocationSetConfig> tmpConfigs = ConfigUtils.getConfigs(LocationSetConfig.class);
            if (tmpConfigs == null)
                return false;
            for (LocationSetConfig config : tmpConfigs.values()) {
                LocationSetConfig old = configs.putIfAbsent(config.provinceID, config);
                if (old != null) {
                    LOGGER.error("LocationSetConfig provinceID{} error!provinceID is repeat!", config.provinceID);
                    return false;
                }
            }
            return true;
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        for(LocationSetConfig locationSet : configs.values()) {
            if(locationSet.getCityName().isEmpty()) {
                LOGGER.error("LocationSetConfig id{} error!cityName is empty!", locationSet.id);
                return false;
            }
        }
        if(!configs.containsKey(DEFAULT_PROVINCE_ID)) {
            LOGGER.error("LocationSetConfig default provinceID{} is not exist!", DEFAULT_PROVINCE_ID);
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public int getProvinceID() {
        return provinceID;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public List<String> getCityName() {
        return cityName;
    }
}
