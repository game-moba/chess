package com.three.config;

import com.three.mahjong.base.model.MJGameType;
import com.three.mahjong.base.model.PayType;
import com.three.mahjong.base.model.RuleType;
import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import com.three.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 房间规则配置信息
 * Created by mathua on 2017/7/20.
 */
public final class MJCreateRoomRuleConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(MJCreateRoomRuleConfig.class);
    private static Map<MJGameType, Map<RuleType, List<MJCreateRoomRuleConfig>>> configs;

    private int id;
    private int gameType;
    private int ruleType;
    private List<String> ruleParams;
    private String ruleName;

    public static boolean checkCeilingScore(MJGameType gameType, int ceilingScore) {
        Map<RuleType, List<MJCreateRoomRuleConfig>> tmpConfigs = configs.get(gameType);
        List<MJCreateRoomRuleConfig> ruleConfigs = tmpConfigs.get(RuleType.CEILING_SCORE);
        for(MJCreateRoomRuleConfig ruleConfig : ruleConfigs) {
            if(Integer.valueOf(ruleConfig.getRuleParams().get(0)) == ceilingScore) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkBaseScore(MJGameType gameType, int baseScore) {
        Map<RuleType, List<MJCreateRoomRuleConfig>> tmpConfigs = configs.get(gameType);
        List<MJCreateRoomRuleConfig> ruleConfigs = tmpConfigs.get(RuleType.BASE_SCORE);
        for(MJCreateRoomRuleConfig ruleConfig : ruleConfigs) {
            if(Integer.valueOf(ruleConfig.getRuleParams().get(0)) == baseScore) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkGameInning(MJGameType gameType, int gameInning) {
        Map<RuleType, List<MJCreateRoomRuleConfig>> tmpConfigs = configs.get(gameType);
        List<MJCreateRoomRuleConfig> ruleConfigs = tmpConfigs.get(RuleType.INNING);
        for(MJCreateRoomRuleConfig ruleConfig : ruleConfigs) {
            if(Integer.valueOf(ruleConfig.getRuleParams().get(0)) == gameInning) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkPayType(MJGameType gameType, PayType payType) {
        Map<RuleType, List<MJCreateRoomRuleConfig>> tmpConfigs = configs.get(gameType);
        List<MJCreateRoomRuleConfig> ruleConfigs = tmpConfigs.get(RuleType.PAY_TYPE);
        for(MJCreateRoomRuleConfig ruleConfig : ruleConfigs) {
            if(Integer.valueOf(ruleConfig.getRuleParams().get(0)) == payType.id) {
                return true;
            }
        }
        return false;
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        try {
            Map<Long, MJCreateRoomRuleConfig> tmpConfigs = ConfigUtils.getConfigs(MJCreateRoomRuleConfig.class);
            if(tmpConfigs == null)
                return false;
            configs = new HashMap<>();
            for(MJCreateRoomRuleConfig config : tmpConfigs.values()) {
                MJGameType gameType = config.getGameType();
                if(gameType == null) {
                    LOGGER.error("MJCreateRoomRuleConfig gameType={} error!", config.gameType);
                    return false;
                }
                Map<RuleType, List<MJCreateRoomRuleConfig>> rules = new HashMap<>();
                Map<RuleType, List<MJCreateRoomRuleConfig>> oldRules = configs.putIfAbsent(gameType, rules);
                if(oldRules != null)
                    rules = oldRules;

                RuleType ruleType = config.getRuleType();
                if(ruleType == null) {
                    LOGGER.error("MJCreateRoomRuleConfig ruleType={} error!", config.ruleType);
                    return false;
                }
                List<MJCreateRoomRuleConfig> ruleList = new ArrayList<>();
                List<MJCreateRoomRuleConfig> oldRuleList = rules.putIfAbsent(ruleType, ruleList);
                if(oldRuleList != null)
                    ruleList = oldRuleList;
                ruleList.add(config);
            }
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return configs != null;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        // 检查麻将的房间规则
        if (!configs.containsKey(MJGameType.GUANG_DONG_MA_JIANG)) {
            LOGGER.error("Not Exist MJGameType={}.", MJGameType.GUANG_DONG_MA_JIANG);
            return false;
        }
        Map<RuleType, List<MJCreateRoomRuleConfig>> rulesOfMaJiang = configs.get(MJGameType.GUANG_DONG_MA_JIANG);
        for (RuleType type : RuleType.values()) {
            if (!rulesOfMaJiang.containsKey(type)) {
                LOGGER.error("Not Exist RuleType={}.", type);
                return false;
            }
            List<MJCreateRoomRuleConfig> configList = rulesOfMaJiang.get(type);
            for (MJCreateRoomRuleConfig config : configList) {
                if (!type.checkConfig(config))
                    return false;
            }
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public MJGameType getGameType() {
        return MJGameType.get(gameType);
    }

    public RuleType getRuleType() {
        return RuleType.valueOf(ruleType);
    }

    public List<String> getRuleParams() {
        return ruleParams;
    }

    public String getRuleName() {
        return ruleName;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
