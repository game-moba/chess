package com.three.config;

import com.three.mahjong.base.model.HandPattern;
import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.domain.quest.GameType;
import com.three.domain.quest.TaskType;
import com.three.reward.RewardType;
import com.three.utils.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class MissionConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(MissionConfig.class);
    private static Map<Long, MissionConfig> configs;

    private long id;
    private int taskType;// 任务类型
    private long frontTask;//前置任务
    private long postTask;//后置任务
    private int mission;//任务触发类型
    private int gameType;//游戏类型
    private int handPatterns;//牌型
    private int count;//数量
//    private String timeQuantum;//时间量
    private int rewardType;//奖励类型
    private int rewardParam;//奖励参数
    private int rewardsCount;//奖励数量
    private String missionIcon;//任务图标
    private int missionTips;//任务提示
    private String tips;//任务描述

    public static MissionConfig getConfig(long id) {
        return configs.get(id);
    }

    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        configs = ConfigUtils.getConfigs(MissionConfig.class);
        return configs != null;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        return configs.values().stream()
                .allMatch(MissionConfig::verfyConfig);
    }

    private static boolean verfyConfig(MissionConfig config){
        long id=config.id;
        if(!TaskType.verfy(config.getTaskType())){
            LOGGER.error("MissionConfig.TaskType error, configId:{},taskType:{}", id,config.taskType);
            return false;
        }
        //前后置任务
        if(config.frontTask!=0&&!configs.keySet().contains((long)config.frontTask)){
            LOGGER.error("MissionConfig.frontTask error, configId:{},frontTask:{}", id,config.taskType);
            return false;
        }
        if(config.postTask!=0&&!configs.keySet().contains((long)config.postTask)){
            LOGGER.error("MissionConfig.postTask error, configId:{},postTask:{}", id,config.taskType);
            return false;
        }
        if(!GameType.verfy(config.getGameType())){
            LOGGER.error("MissionConfig.GameType error, configId:{},GameType:{}", id,config.gameType);
            return false;
        }
        if(!HandPattern.verfy(config.getHandPatterns())){
            LOGGER.error("MissionConfig.HandPatterns error, configId:{},HandPatterns:{}", id,config.handPatterns);
            return false;
        }
        if(config.count<0){
            LOGGER.error("MissionConfig.count error, configId:{},count:{}", id,config.count);
            return false;
        }
        if(!RewardType.verfy(config.getRewardType())||config.rewardsCount<0){
            LOGGER.error("MissionConfig.Rewards error, configId:{},Rewards:{},rewardsCount:{}", id,config.rewardType,config.rewardsCount);
            return false;
        }
        return true;
    }

    public long getId() {
        return id;
    }

    public int getTaskType() {
        return taskType;
    }

    public long getFrontTask() {
        return frontTask;
    }

    public long getPostTask() {
        return postTask;
    }

    public int getMission() {
        return mission;
    }

    public int getGameType() {
        return gameType;
    }

    public int getHandPatterns() {
        return handPatterns;
    }

    public int getCount() {
        return count;
    }

    public int getRewardType() {
        return rewardType;
    }

    public int getRewardParam() {
        return rewardParam;
    }

    public int getRewardsCount() {
        return rewardsCount;
    }

    public String getMissionIcon() {
        return missionIcon;
    }

    public int getMissionTips() {
        return missionTips;
    }

    public String getTips() {
        return tips;
    }

    public static Map<Long, MissionConfig> getConfigs() {
        return configs;
    }
}
