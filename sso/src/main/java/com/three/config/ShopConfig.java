package com.three.config;

import com.three.config.game.annotation.ConfigConstruct;
import com.three.config.game.annotation.ConfigMethodType;
import com.three.utils.ConfigUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ShopConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopConfig.class);
    private static Map<Long, ShopConfig> configs;
    /**配置项**/
    private int id;
    private int shopType;//商店类型
    private int itemId;// 物品id
    private int itemOrder;//物品排序
    private String itemName;//物品名称
    private int itemPrice;//价格配置整数，不考虑浮点数的情况
    private int priceUnit;//价格单位
    private String itemInfo;//具体描述
    private long number;//数量
    private int gift;//加赠百分比
    /**
     * 初始化配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.INIT)
    private static boolean init() {
        configs = ConfigUtils.getConfigs(ShopConfig.class);
        return configs != null;
    }

    /**
     * 检查配置表
     *
     * @return
     */
    @ConfigConstruct(type = ConfigMethodType.CHECK)
    private static boolean check() {
        return configs.values().stream()
                .allMatch(ShopConfig::verfyConfig);
    }

    /**
     * TODO 07-09
     * 检查单个配置项
     * @param config
     * @return
     */
    private static boolean verfyConfig(ShopConfig config){
        int id=config.getId();
        //商店类型
        if(!ShopType.verfy(config.getShopType())){
            LOGGER.error("MissionConfig.ShopType error, configId:{},ShopType:{}", id,config.shopType);
            return false;
        }
        if(!PriceUnit.verfy(config.getPriceUnit())){
            LOGGER.error("MissionConfig.PriceUnit error, configId:{},PriceUnit:{}", id,config.priceUnit);
            return false;
        }
        if(!ItemConfig.verfyConfig(config.getItemId())) {
            LOGGER.error("ItemConfig not exist, configId:{},ItemId:{}", id,config.getItemId());
            return false;
        }
        return true;
    }
    public static Map<Long, ShopConfig> getConfigs() {
        return configs;
    }

    public int getId() {
        return id;
    }

    public int getShopType() {
        return shopType;
    }

    public int getItemOrder() {
        return itemOrder;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public int getPriceUnit() {
        return priceUnit;
    }

    public String getItemInfo() {
        return itemInfo;
    }

    public long getNumber() {
        return number;
    }

    public int getGift() {
        return gift;
    }

    public int getItemId() {
        return itemId;
    }

    /**
     * 商店类型
     */
    public enum  ShopType {
        GOLD(0),//金币商店
        DIAMOND(1),//钻石商店
        ITEM(2);//道具商店
        private static Map<Integer,ShopType> VALUES= Arrays.stream(ShopType.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
        ShopType(int id){
            this.id=id;
        }
        public int id;
        public static boolean verfy(int id){
            return VALUES.containsKey(id);
        }
        public static ShopType get(int id){
            return VALUES.get(id);
        }
    }
    /**
     * 价格单位
     */
    public enum  PriceUnit {
        GOLD(1),//金币
        DIAMOND(2),//钻石
        RMB(3);//软妹币
        private static Map<Integer,PriceUnit> VALUES= Arrays.stream(PriceUnit.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
        PriceUnit(int id){
            this.id=id;
        }
        public int id;
        public static boolean verfy(int id){
            return VALUES.containsKey(id);
        }
        public static PriceUnit get(int id){
            return VALUES.get(id);
        }
    }
}
