package com.three.constant;

/**
 * Created by mathua on 2017/6/23.
 */
public interface MailConstants {

    // 主题长度-18
    int SUBJECT_LIMIT = 18;

    // 纯文本内容长度-2000
    int CONTENT_LIMIT_FOR_PLAIN_TEXT = 2000;
    // 物品类内容长度-400
    int CONTENT_LIMIT_FOR_ITEMS_MAIL = 400;
}
