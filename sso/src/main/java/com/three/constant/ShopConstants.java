package com.three.constant;

public interface ShopConstants {
    String SUCCESS = "success";
    String FAILURE = "failure";

    /*******************支付宝订单状态***********************/
    //交易创建，等待买家付款
    String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
    //未付款交易超时关闭，或支付完成后全额退款
    String TRADE_CLOSED = "TRADE_CLOSED";
    //交易支付成功
    String TRADE_SUCCESS = "TRADE_SUCCESS";
    //交易结束，不可退款
    String TRADE_FINISHED = "TRADE_FINISHED";
    /*******************业务订单状态***********************/
    //初始状态
    int ORDER_STATUS_FAILURE=0;
    /**
     * 已付款
     * 充值——扣软妹币
     * 消费——扣金币、钻石
     */
    int ORDER_STATUS_PAID=1;
    /**
     * 已收货
     * 充值——给金币、钻石
     * 消费——给道具
     */
    int ORDER_STATUS_RECEIVED=2;
    /*****************************************************/
}
