package com.three.domain.account;

import com.three.base.AbstractEntity;
import com.three.db.idService.IdService;
import com.three.repository.AccountRepository;
import com.three.utils.SpringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by mathua on 2017/6/11.
 */
@Entity
@Table(name = "t_account",catalog = "chess_db")
public class AccountPo extends AbstractEntity {

    @Id
    private long id;
    @Column(name = "open_id", length = 128, nullable = false, updatable = false)
    private String openId;// 身份标识
    @Column(name = "player_id", nullable = false, updatable = false)
    private long playerId;// 玩家id
    @Column(name = "platform", nullable = false, updatable = false)
    private String platform;// 渠道标志
    @Column(name = "total_pay")
    private double totalPay;// 累计充值金额(元)
    @Column(name = "is_online")
    private boolean isOnline;// 是否在线
    @Column(name = "shut_up")
    private Date shutUp;// 封号时间
    @Column(name = "forbidden_end_time")
    private Date forbiddenEndTime;// 封号结束时间

    // 玩家注册信息
    @Column(name = "register_system")
    private String registerSystem;// 注册系统
    @Column(name = "create_date")
    private Date createDate;// 角色创建时间
    @Column(name = "register_ip")
    private String registerIp;// 注册ip

    // 玩家登陆信息
    @Column(name = "login_time")
    private Date loginTime;// 登录时间
    @Column(name = "login_system")
    private String loginSystem;// 登录系统
    @Column(name = "login_ip")
    private String loginIp;// 登录ip

    public static AccountPo createAccount(long playerId, String openId, String platform, String ip, String loginSystem) {

        Date now = new Date();
        AccountPo account = new AccountPo();
        account.id = IdService.get().get();
        account.openId = openId;
        account.playerId = playerId;
        account.platform = platform;
        account.totalPay = 0.0;
        account.isOnline = true;
        account.shutUp = new Date(0);
        account.forbiddenEndTime = new Date(0);

        // 注册信息
        account.registerSystem = loginSystem;
        account.registerIp = ip;
        account.createDate = now;

        // 登录信息
        account.loginTime = now;
        account.loginIp = ip;
        account.loginSystem = loginSystem;
        return account;
    }

    /**
     * 更新登录信息
     * @param ip 登录ip
     * @param loginSystem 登录机器的系统
     */
    public void updateLoginInfo(String ip, String loginSystem) {
        Date now = new Date();
        this.loginTime = now;
        this.loginIp = ip;
        this.loginSystem = loginSystem;
        SpringUtils.getBean(AccountRepository.class).save(this);
    }

    public long getId() {
        return id;
    }

    public String getOpenId() {
        return openId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public String getPlatform() {
        return platform;
    }

    public double getTotalPay() {
        return totalPay;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public Date getShutUp() {
        return shutUp;
    }

    public Date getForbiddenEndTime() {
        return forbiddenEndTime;
    }

    public String getRegisterSystem() {
        return registerSystem;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public String getLoginSystem() {
        return loginSystem;
    }

    public String getLoginIp() {
        return loginIp;
    }

    @Override
    public long getEntityId() {
        return id;
    }
}
