package com.three.domain.item;

import com.three.base.AbstractEntity;
import com.three.config.ItemConfig;
import com.three.db.idService.IdService;
import com.three.repository.ItemRepository;
import com.three.utils.JsonUtils;
import com.three.utils.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Mathua on 2017/7/17.
 */
@Entity
@Table(name = "t_item")
public class ItemPo extends AbstractEntity {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemPo.class);

    @Id
    private long id;
    @Column(name = "owner_id")
    private long ownerId;// 拥有者id
    @Column(name = "item_id")
    private int itemId;// 物品id
    @Column(name = "count")
    private int count;// 物品数量
    @Column(name = "expire")
    private long expire;//过期时间(0代表永不过期)

    public void add(Integer num) {
        if(expire != 0)
            return;
        int addCount = 0;
        ItemConfig itemConfig = getCfg();
        if(count + num > itemConfig.getAccumulateLimit())
            addCount = itemConfig.getAccumulateLimit() - count;
        else
            addCount = num;

        if(addCount > 0) {
            count += addCount;
            num -= addCount;
            SpringUtils.getBean(ItemRepository.class).save(this);
        }
    }

    /**
     * tmpItem接口，协议使用
     * @param id
     * @param itemId
     * @param expire
     * @param count
     * @return
     */
    public static ItemPo buildTmp(long id, int itemId, long expire, int count) {
        ItemPo itemPo = new ItemPo();
        itemPo.id = id;
        itemPo.itemId = itemId;
        itemPo.expire = expire;
        itemPo.count = count;
        return itemPo;
    }

    public static ItemPo build(long ownerId, int itemId, Integer count) {

        if(count <= 0) {
            LOGGER.error("Item Count Error, count:{}", count);
            return null;
        }

        ItemConfig itemConfig = ItemConfig.get(itemId);
        if(itemConfig == null) {
            LOGGER.error("Item Config Not Found, itemId:{}", itemId);
            return null;
        }

        // 计算当前背包物品数量
        int realCount = count;
        if(itemConfig.getAccumulateLimit() != 0 && count >= itemConfig.getAccumulateLimit()) {
            realCount = itemConfig.getAccumulateLimit();
        }
        count -= realCount;

        // 计算过期时间
        long expire = 0;
        if(itemConfig.getLifeTime() != 0) {
            expire = System.currentTimeMillis() + itemConfig.getLifeTime();
        }

        ItemPo item = new ItemPo();
        item.id = IdService.get().get();
        item.ownerId = ownerId;
        item.itemId = itemId;
        item.count = realCount;
        item.expire = expire;

        return item;
    }

    public ItemConfig getCfg() {
        return ItemConfig.get(itemId);
    }

    @Override
    public long getEntityId() {
        return id;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getCount() {
        return count;
    }

    public long getExpire() {
        return expire;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
