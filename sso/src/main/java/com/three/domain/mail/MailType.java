package com.three.domain.mail;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mathua on 2017/6/22.
 */
public enum MailType {
    PLAIN_TEXT(1),// 纯文本
    ITEMS_MAIL(2),// 物品类
    ;
    private final int id;
    MailType(int id) {
        this.id = id;
    }

    private static Map<Integer, MailType> map = new HashMap<>();
    static {
        for(MailType type : values()) {
            map.putIfAbsent(type.id, type);
        }
    }

    public static MailType valueOf(int id) {
        return map.get(id);
    }

    public int getId() {
        return id;
    }
}
