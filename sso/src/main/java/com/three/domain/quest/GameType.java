package com.three.domain.quest;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by mathua on 2017/7/22.
 */
public enum GameType {
    // 0-99：麻将类型
    GUANG_DONG_MA_JIANG(0),// 广东麻将
    TYPE_1(1)// 待扩展其他麻将

    // 100-199：斗地主

    // 200-299：德州
    ;

    private static Map<Integer, GameType> VALUES= Arrays.stream(GameType.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
    GameType(int id){
        this.id=id;
    }
    public int id;
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public static GameType get(int id){
        return VALUES.get(id);
    }
}
