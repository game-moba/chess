package com.three.domain.quest;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 任务触发类型
 */
public enum Mission {
    WECHATCIRCL(1),
    WECHATFRIENDS(2),
    GOLD(3),
    LOGO(4),
    LEVEL_UPGRADE(5),
    GAME(6);
    private static Map<Integer, Mission> VALUES= Arrays.stream(Mission.values()).collect(Collectors.toMap(t->t.id,Function.identity()));
    Mission(int id){
        this.id=id;
    }
    public int id;
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public Mission get(int id){
        return VALUES.get(id);
    }
}
