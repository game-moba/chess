package com.three.domain.quest;

import com.three.api.event.*;
import com.three.base.AbstractEntity;
import com.three.config.MissionConfig;
import com.three.config.LanguageConfig;
import com.three.constant.LanguageId;
import com.three.domain.player.PlayerAttrType;
import com.three.event.EventBus;
import com.three.exception.GameException;
import com.three.repository.TaskRepository;
import com.three.reward.RewardItem;
import com.three.reward.RewardType;
import com.three.reward.impl.PlayerAttrReward;
import com.three.utils.SpringUtils;

import javax.persistence.*;
import java.util.Date;

/**
 *
 */
@Entity
@Table(name = "t_task")
public class TaskPO extends AbstractEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column(name = "player_id", nullable = false)
    private long playerId;// 拥有者id
    @Column(name = "mission_id", nullable = false)
    private long missionId;//配置ID
    @Column(name = "progress")
    private long progress;//进度
    @Column(name = "rewarded")
    private boolean rewarded;//是否已领取奖励
    @Column(name = "modifyTime")
    private Date modifyTime;//修改时间

    @Override
    public long getEntityId() {
        return id;
    }

    //领取奖励
    public void reward() throws GameException{
        if(!completed()){
            throw new GameException(LanguageConfig.getText(LanguageId.TASK_REPEAT_REWARD));
        }
        if(isRewarded()){
            throw new GameException(LanguageConfig.getText(LanguageId.TASK_REPEAT_REWARD));
        }
        MissionConfig config = MissionConfig.getConfig(missionId);
        if (config.getRewardType() == RewardType.PLAYER_ATTR.getId()) {//属性奖励
            switch (config.getRewardParam()) {
                case (int) LanguageId.GOLD:
                    new PlayerAttrReward(RewardItem.build(RewardType.PLAYER_ATTR, PlayerAttrType.GOLD.getPlayerAttr(), config.getRewardsCount()),playerId).handOutReward(false);
                    break;
                case (int) LanguageId.DIAMOND:
                    new PlayerAttrReward(RewardItem.build(RewardType.PLAYER_ATTR, PlayerAttrType.DIAMOND.getPlayerAttr(), config.getRewardsCount()),playerId).handOutReward(false);
                    break;
                default:
                    break;
            }
        }
    }

    //登记进度
    public void progress(int pluProgress) {
        this.progress = this.progress + pluProgress;
        if (completed()) {
            EventBus.I.post(new TaskFinishEvent(playerId, id, new Date()));
        }
    }

    public void setProgress(long progress) {
        this.progress = progress;
        if (completed()) {
            EventBus.I.post(new TaskFinishEvent(playerId, id, new Date()));
        }
    }

    /**
     * 任务是否完成
     *
     * @return
     */
    public boolean completed() {
        //TODO 配置更新，下架任务类型的情况
        return progress >= MissionConfig.getConfig(missionId).getCount();
    }

    /**
     * 任务触发条件是否支持
     *
     * @param event
     * @return
     */
    public boolean support(Event event) {
        MissionConfig config = MissionConfig.getConfig(missionId);
        if (event instanceof GameFinishEvent) {
            return config.getMission() == Mission.GAME.id &&
                    config.getGameType() == ((GameFinishEvent) event).getGameType() &&
                    config.getHandPatterns() == ((GameFinishEvent) event).getHandPattner();
        }
        if (event instanceof PlayerLevelUpgradeEvent) {
            return config.getMission() == Mission.LEVEL_UPGRADE.id;
        }
        if (event instanceof PlayerInfoChangeEvent) {
            return ((PlayerInfoChangeEvent) event).getInfoType().equals(PlayerInfoChangeEvent.PlayerInfoType.HEAD) &&
                    config.getMission() == Mission.LOGO.id;
        }
        if (event instanceof PlayerAttributeChangeEvent) {
            return ((PlayerAttributeChangeEvent) event).getAttrType().equals(PlayerAttributeChangeEvent.PlayerAttrType.GOLD) &&
                    config.getMission() == Mission.GOLD.id;
        }
        if (event instanceof ShareWechatCirclEvent) {
            return config.getMission() == Mission.WECHATCIRCL.id;
        }
        if (event instanceof ShareWechatFriendsEvent) {
            return config.getMission() == Mission.WECHATFRIENDS.id;
        }
        return false;
    }

    /**
     * 重置任务状态
     */
    public void reset() {
        this.progress = 0;
        this.rewarded = false;
    }

    /**
     * 是否前置任务
     */
    public boolean isFrontTask(long taskId) {
        return taskId == MissionConfig.getConfig(missionId).getFrontTask();
    }

    /**
     * 保存对象
     */
    public void save() {
        SpringUtils.getBean(TaskRepository.class).save(this);
    }

    public static TaskPO build(long playerId, long missionId, int progress) {
        TaskPO taskPO = new TaskPO();
        taskPO.playerId = playerId;
        taskPO.missionId = missionId;
        taskPO.progress = progress;
        taskPO.modifyTime = new Date();
        return taskPO;
    }

    public long getId() {
        return id;
    }

    public long getPlayerId() {
        return playerId;
    }

    public long getMissionId() {
        return missionId;
    }

    public long getProgress() {
        return progress;
    }

    public boolean isRewarded() {
        return rewarded;
    }

    public Date getModifyTime() {
        return modifyTime;
    }
}
