package com.three.domain.quest;


import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 任务类型
 */
public enum  TaskType {
    DAILY(0),
    MAIN(1);
    private static Map<Integer,TaskType> VALUES= Arrays.stream(TaskType.values()).collect(Collectors.toMap(t->t.id,Function.identity()));
    TaskType(int id){
        this.id=id;
    }
    public int id;
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public TaskType get(int id){
        return VALUES.get(id);
    }
}
