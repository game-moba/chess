package com.three.domain.shop;

import com.three.base.AbstractEntity;
import com.three.config.LanguageConfig;
import com.three.config.ShopConfig;
import com.three.constant.LanguageId;
import com.three.constant.ShopConstants;
import com.three.db.idService.IdService;
import com.three.exception.GameException;
import com.three.repository.ConsumeLogRepository;
import com.three.service.ShopService;
import com.three.utils.SpringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * 消费流水
 * Created by wangziqing on 2017/7/16 0016.
 */
@Entity
@Table(name = "t_consume_log")
public class ConsumeLog extends AbstractEntity {
    /*****************公共参数*********************/
    @Id
    private long orderId;//订单ID
    @Column(name = "player_id", nullable = false)
    private long playerId;//玩家ID
    @Column(name = "channelId", nullable = false)
    private int channelId;//渠道ID
    @Column(name = "modify_time")
    private Date modifyTime;//更改时间
    @Column(name = "log_time")
    private Date logTime;//日志创建时间
    @Column(name = "status")
    private int status= ShopConstants.ORDER_STATUS_FAILURE;//内部交易处理状态
    /*****************业务参数*********************/
    @Column(name = "item_id")
    private int itemId;//商店类型
    @Column(name = "shop_type")
    private int shopType;//商店类型
    @Column(name = "item_name")
    private String itemName;//物品名称
    @Column(name = "item_price")
    private int itemPrice;//价格配置整数，不考虑浮点数的情况
    @Column(name = "price_unit")
    private int priceUnit;//价格单位
    @Column(name = "item_info")
    private String itemInfo;//具体描述
    @Column(name = "number")
    private long number;//数量
    @Column(name = "gift")
    private int gift;//加赠百分比

    @Override
    public long getEntityId() {
        return orderId;
    }
    public static ConsumeLog build(long playerId, int channelId,int itemId){
        return new ConsumeLog(playerId,channelId,itemId);
    }
    public ConsumeLog(long playerId, int channelId,int itemId){
        ShopConfig item=ShopConfig.getConfigs().get((long)itemId);
        if(Objects.isNull(item)){//商品不存在
            throw new GameException(LanguageConfig.getText(LanguageId.ITEM_NOT_EXIST));
        }
        this.itemId=itemId;
        this.orderId=IdService.get().get();
        this.playerId=playerId;
        this.channelId=channelId;
        this.logTime=new Date();

        this.shopType=item.getShopType();
        this.itemName=item.getItemName();
        this.itemInfo=item.getItemInfo();
        this.itemPrice=item.getItemPrice();
        this.priceUnit=item.getPriceUnit();
        this.number=item.getNumber();
        this.gift=item.getGift();
    }
    /**
     * 开收据
     * @return
     */
    public void receipt(){
        if(SpringUtils.getBean(ShopService.class).receipt(this)){
            this.status=ShopConstants.ORDER_STATUS_RECEIVED;
            save();
        }
    }

    /**
     * 支付
     * @return
     */
    public void pay(){
        if(SpringUtils.getBean(ShopService.class).pay(this)){
            this.status=ShopConstants.ORDER_STATUS_PAID;
            save();
        }
    }

    /**
     * 保存
     */
    public void save(){
        this.modifyTime=new Date();
        SpringUtils.getBean(ConsumeLogRepository.class).save(this);
    }
    /**
     * 订单处理前置校验
     * @return
     */
    public boolean check(){
        return true;
    }

    public boolean matched(Predicate<? super ConsumeLog> predicate){
        return predicate.test(this);
    }
    private void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    private void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    private void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    private void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    private void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    private void setStatus(int status) {
        this.status = status;
    }

    public long getOrderId() {
        return orderId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public int getChannelId() {
        return channelId;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public Date getLogTime() {
        return logTime;
    }

    public int getStatus() {
        return status;
    }

    public int getShopType() {
        return shopType;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public int getPriceUnit() {
        return priceUnit;
    }

    public String getItemInfo() {
        return itemInfo;
    }

    public long getNumber() {
        return number;
    }

    public int getGift() {
        return gift;
    }

    public int getItemId() {
        return itemId;
    }
}
