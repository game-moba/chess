package com.three.domain.shop;

import com.alibaba.druid.support.json.JSONUtils;
import com.three.base.AbstractEntity;
import com.three.constant.ShopConstants;
import com.three.db.idService.IdService;
import com.three.repository.PayLogRepository;
import com.three.service.ShopService;
import com.three.utils.LogUtils;
import com.three.utils.SpringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * 支付流水
 */
@Entity
@Table(name = "t_pay_log")
public class PayLog extends AbstractEntity {
    /*****************公共参数*********************/
    @Id
    private long orderId;//订单ID
    @Column(name = "player_id", nullable = false)
    private long playerId;//玩家ID
    @Column(name = "channelId", nullable = false)
    private int channelId;//渠道ID
    @Column(name = "modify_time")
    private Date modifyTime;//更改时间
    @Column(name = "log_time")
    private Date logTime;//日志创建时间
    @Column(name = "status")
    private int status= ShopConstants.ORDER_STATUS_FAILURE;//内部交易处理状态
    /*****************业务参数*********************/
    @Column(name = "shop_type")
    private int shopType;//商店类型
    @Column(name = "item_name")
    private String itemName;//物品名称
    @Column(name = "item_price")
    private int itemPrice;//价格配置整数，不考虑浮点数的情况
    @Column(name = "price_unit")
    private int priceUnit;//价格单位
    @Column(name = "item_info")
    private String itemInfo;//具体描述
    @Column(name = "number")
    private long number;//数量
    @Column(name = "gift")
    private int gift;//加赠百分比
    /*****************第三方参数********************/
    @Column(name = "order_create_time")
    private Date orderCreateTime;//订单创建时间
    @Column(name = "order_end_time")
    private Date orderEndTime;//订单付款时间
    @Column(name = "total_amount")
    private String totalAmount;//订单总金额
    @Column(name = "buyer_pay_amount")
    private String buyerPayAmount;//玩家付款金额
    @Column(name = "buyer_logon_id")
    private String buyerLogonId;//玩家支付宝账号
    @Column(name = "buyer_id")
    private String buyerId;//玩家支付宝用户号
    @Column(name = "receipt_amount")
    private String receiptAmount;//商家实收金额
    @Column(name = "seller_email")
    private String sellerEmail;//商家支付宝账号
    @Column(name = "seller_id")
    private String sellerId;//商家支付宝用户号
    @Column(name = "trado_no")
    private String tradeNo;//支付宝交易凭证——唯一
    @Column(name = "trade_status")
    private String tradeStatus;//支付宝交易状态

    /**
     * 开收据
     * @return
     */
    public void receipt(){
        if(SpringUtils.getBean(ShopService.class).receipt(this)){
            this.status=ShopConstants.ORDER_STATUS_RECEIVED;
            save();
        }
    }

    /**
     * 支付
     * @return
     */
    public void pay(){
        if(SpringUtils.getBean(ShopService.class).pay(this)){
            this.status=ShopConstants.ORDER_STATUS_PAID;
            save();
        }
    }

    /**
     * 保存
     */
    public void save(){
        this.modifyTime=new Date();
        SpringUtils.getBean(PayLogRepository.class).save(this);
    }

    /**
     * 订单处理前置校验
     * @return
     */
    public boolean check(){
        if(matched(log->log.itemPrice!=Integer.valueOf(log.totalAmount))){
            LogUtils.SHOP.warn("阿里支付，订单金额不一致，信息：{}", JSONUtils.toJSONString(this));
            return false;
        }
        if(matched(log->SpringUtils.getBean(PayLogRepository.class).countByTradeNo(this.tradeNo)>0)){
            LogUtils.SHOP.warn("阿里支付，重复订单，信息：{}", JSONUtils.toJSONString(this));
            return false;
        }
        return true;
    }


    public boolean matched(Predicate<? super PayLog> predicate){
        return predicate.test(this);
    }
    @Override
    public long getEntityId() {
        return orderId;
    }

    public static PayLog build(long playerId, int channelId){
        return new PayLog(playerId,channelId);
    }

    public PayLog (long playerId,int channelId){
        this.orderId= IdService.get().get();
        this.playerId=playerId;
        this.channelId=channelId;
        this.logTime=new Date();
    }

    public PayLog setItemName(String itemName) {
        this.itemName = itemName;return this;
    }

    public PayLog setItemInfo(String itemInfo) {
        this.itemInfo = itemInfo;return this;
    }
    public PayLog setShopType(int shopType) {
        this.shopType = shopType;return this;
    }

    public PayLog setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;return this;
    }

    public PayLog setPriceUnit(int priceUnit) {
        this.priceUnit = priceUnit;return this;
    }

    public PayLog setNumber(long number) {
        this.number = number;return this;
    }

    public PayLog setGift(int gift) {
        this.gift = gift;return this;
    }

    public PayLog setOrderCreateTime(Date orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
        return this;
    }

    public PayLog setOrderEndTime(Date orderEndTime) {
        this.orderEndTime = orderEndTime;return this;
    }

    public PayLog setBuyerPayAmount(String buyerPayAmount) {
        this.buyerPayAmount = buyerPayAmount;return this;
    }

    public PayLog setBuyerLogonId(String buyerLogonId) {
        this.buyerLogonId = buyerLogonId;return this;
    }

    public PayLog setBuyerId(String buyerId) {
        this.buyerId = buyerId;return this;
    }

    public PayLog setReceiptAmount(String receiptAmount) {
        this.receiptAmount = receiptAmount;return this;
    }

    public PayLog setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;return this;
    }

    public PayLog setSellerId(String sellerId) {
        this.sellerId = sellerId;return this;
    }

    public PayLog setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;return this;
    }

    public PayLog setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;return this;
    }

    public PayLog setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;return this;
    }

    public int getStatus() {
        return status;
    }

    public long getOrderId() {
        return orderId;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemPrice() {
        return itemPrice;
    }

    public int getPriceUnit() {
        return priceUnit;
    }

    public String getItemInfo() {
        return itemInfo;
    }

    public long getNumber() {
        return number;
    }

    public int getGift() {
        return gift;
    }

    public long getPlayerId() {
        return playerId;
    }

    public Date getOrderCreateTime() {
        return orderCreateTime;
    }

    public Date getOrderEndTime() {
        return orderEndTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public Date getLogTime() {
        return logTime;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getBuyerPayAmount() {
        return buyerPayAmount;
    }

    public String getBuyerLogonId() {
        return buyerLogonId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public String getReceiptAmount() {
        return receiptAmount;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public String getSellerId() {
        return sellerId;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public int getShopType() {
        return shopType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }
}
