package com.three.event;

import com.google.common.eventbus.Subscribe;
import com.three.api.event.HeartBeatEvent;
import com.three.api.event.PlayerOnlineEvent;
import com.three.api.protocol.Packet;
import com.three.api.push.PushSender;
import com.three.domain.mail.MailPo;
import com.three.domain.mail.MailType;
import com.three.domain.player.PlayerAttrType;
import com.three.exception.GameException;
import com.three.protocol.CommandEnum;
import com.three.protocol.MailModule;
import com.three.repository.MailRepository;
import com.three.reward.RewardItem;
import com.three.reward.RewardType;
import com.three.service.MailModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mathua on 2017/6/23.
 */
@Component
/*package*/ final class MailEventConsumer extends EventConsumer {
    @Autowired
    private MailRepository mailRepository;
    @Autowired
    private MailModuleService mailModuleService;

    @Subscribe
    void on(PlayerOnlineEvent event) {
//        // 测试数据
//        try {
//            List<RewardItem> rewardItems = new ArrayList<>();
//            rewardItems.add(RewardItem.build(RewardType.PLAYER_ATTR, PlayerAttrType.DIAMOND.getPlayerAttr(), 1000));
//            MailPo mailPo = MailPo.build(event.getPlayerId(), "停服奖励", "停服奖励，谢谢支持", rewardItems, false, new Date(), null);
//            mailRepository.save(mailPo);
//            mailPo = MailPo.build(event.getPlayerId(), "系统通知", "广东麻将10周年庆典", null, false, new Date(), null);
//            mailRepository.save(mailPo);
//        } catch (GameException e) {
//        }

        List<MailPo> mails =  mailRepository.findByReceiverId(event.getPlayerId());
        if(mails != null && !mails.isEmpty()) {
            MailModule.NotifyMailList_200.Builder builder = MailModule.NotifyMailList_200.newBuilder();
            Iterator<MailPo> iter = mails.iterator();
            while(iter.hasNext()) {
                MailPo mailPo = iter.next();
                // 删除邮件
                if(mailPo.getDeleteTime() != null && mailPo.getDeleteTime().after(new Date())) {
                    mailRepository.delete(mailPo.getEntityId());
                    iter.remove();
                    continue;
                }
                mailModuleService.toMailData(builder, mailPo);
            }
            Packet packet = Packet.build(CommandEnum.Command.NOTIFY_MAIL_LIST, builder.build().toByteArray());
            PushSender sender = PushSender.get();
            sender.send(packet, event.getPlayerId());
        }
    }

    @Subscribe
    void on(HeartBeatEvent event) {
        List<MailPo> mails =  mailRepository.findByReceiverId(event.playerId);
        if(mails != null && !mails.isEmpty()) {
            MailModule.NotifyMailList_200.Builder notifyMailList = MailModule.NotifyMailList_200.newBuilder();
            MailModule.DeleteMailsResp_202.Builder deleteMailsResp = MailModule.DeleteMailsResp_202.newBuilder();
            Iterator<MailPo> iter = mails.iterator();
            while(iter.hasNext()) {
                MailPo mailPo = iter.next();
                // 通知邮件删除
                if(mailPo.getDeleteTime() != null && mailPo.getDeleteTime().after(new Date())) {
                    mailRepository.delete(mailPo.getEntityId());
                    deleteMailsResp.addMailId(mailPo.getEntityId());
                    iter.remove();
                    continue;
                }
                // 定时发邮件
                if(!mailPo.isHadSend()) {
                    mailModuleService.toMailData(notifyMailList, mailPo);
                }
            }
            PushSender sender = PushSender.get();
            if(notifyMailList.getMailsCount() > 0) {
                Packet packet = Packet.build(CommandEnum.Command.NOTIFY_MAIL_LIST, notifyMailList.build().toByteArray());
                sender.send(packet, event.playerId);
            }
            if(deleteMailsResp.getMailIdCount() > 0) {
                Packet packet = Packet.build(CommandEnum.Command.DELETE_MAILS, deleteMailsResp.build().toByteArray());
                sender.send(packet, event.playerId);
            }
        }
    }
}
