package com.three.event;

import com.google.common.eventbus.Subscribe;
import com.three.api.event.*;
import com.three.service.TaskService;
import com.three.config.MissionConfig;
import com.three.domain.quest.TaskPO;
import com.three.repository.TaskRepository;
import com.three.utils.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
final class TaskEventConsumer extends EventConsumer {
    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskRepository taskRepository;

    @Subscribe
    void on(PlayerOnlineEvent event) {
        if (!DateUtils.isSameDate(event.getPreLoginTime(), new Date())) {
            //重置日常任务
            taskService.resetDailyTask(event.getPlayerId());
        }
        //下发任务列表
        taskService.nitifyTasks(event.getPlayerId(),taskService.getTasksByPlayer(event.getPlayerId()));
    }

    //游戏完成事件 TODO
    @Subscribe
    void on(GameFinishEvent event) {
        Arrays.stream(event.getPlayers()).forEach(
                playerId -> {
                    List<TaskPO> tasks = taskService.getTasksByPlayer(playerId).stream()
                            .filter(task -> task.support(event))
                            .filter(task -> !task.completed())
                            .peek(task -> task.progress(event.getCount()))
                            .collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(tasks)) {
                        taskRepository.save(tasks);
                        taskService.nitifyTasks(playerId,tasks);
                    }
                }
        );
    }

    //升级 TODO
    @Subscribe
    void on(PlayerLevelUpgradeEvent event) {
        List<TaskPO> tasks = taskService.getTasksByPlayer(event.getPlayerId()).stream()
                .filter(task -> task.support(event))
                .filter(task -> !task.completed())
                .peek(task -> task.setProgress(event.getCurrentLevel()))
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }

    //修改头像
    @Subscribe
    void on(PlayerInfoChangeEvent event) {
        List<TaskPO> tasks = taskService.getTasksByPlayer(event.getPlayerId()).stream()
                .filter(task -> task.support(event))
                .filter(task -> !task.completed())
                .peek(task -> task.setProgress(1))
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }

    //金币数量变更
    @Subscribe
    void on(PlayerAttributeChangeEvent event) {
        List<TaskPO> tasks = taskService.getTasksByPlayer(event.getPlayerId()).stream()
                .filter(task -> task.support(event))
                .filter(task -> !task.completed())
                .peek(task -> task.setProgress(event.getCurrentCount()))
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }

    //朋友圈分享 TODO
    @Subscribe
    void on(ShareWechatCirclEvent event) {
        List<TaskPO> tasks = taskService.getTasksByPlayer(event.getPlayerId()).stream()
                .filter(task -> task.support(event))
                .filter(task -> !task.completed())
                .peek(task -> task.setProgress(1))
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }

    //微信好友分享 TODO
    @Subscribe
    void on(ShareWechatFriendsEvent event) {
        List<TaskPO> tasks = taskService.getTasksByPlayer(event.getPlayerId()).stream()
                .filter(task -> task.support(event))
                .filter(task -> !task.completed())
                .peek(task -> task.setProgress(1))
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }

    //任务完成事件(进度记录抛出)
    @Subscribe
    void on(TaskFinishEvent event) {
        List<TaskPO> tasks = MissionConfig.getConfigs().values().stream()
                .filter(config -> config.getFrontTask() == event.getTaskId())
                .map(config -> {
                    return TaskPO.build(event.getPlayerId(), config.getId(), 0);
                })
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(tasks)) {
            taskRepository.save(tasks);
            taskService.nitifyTasks(event.getPlayerId(),tasks);
        }
    }
}

