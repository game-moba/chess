package com.three.handler;

import com.three.message.item.GetPackItemsReqMessage;
import com.three.common.receiver.ReceiverType;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import com.three.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mathua on 2017/7/17.
 */
@Service
@PacketHandlerClass
public class ItemModuleHandler {

    @Autowired
    private ItemService itemService;

    @PacketHandlerMethod(msgId = CommandEnum.Command.GET_PACK_ITEMS, describe = "获取背包物品列表", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER,ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void getPackItemsHandler(GetPackItemsReqMessage message) throws Exception {
        itemService.getPackItems(message);
    }
}
