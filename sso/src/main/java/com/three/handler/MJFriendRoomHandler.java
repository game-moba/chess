package com.three.handler;

import com.three.message.hall.*;
import com.three.mahjong.base.service.MJFriendRoomManager;
import com.three.common.message.base.ErrorMessage;
import com.three.common.receiver.ReceiverType;
import com.three.exception.GameException;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 麻将好友房处理类
 * Created by mathua on 2017/6/13.
 */
@Service
@PacketHandlerClass
public class MJFriendRoomHandler {

    @Autowired
    private MJFriendRoomManager mjFriendRoomManager;

    @PacketHandlerMethod(msgId = CommandEnum.Command.CREATE_FRIEND_ROOM, describe = "创建房间", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void createFriendRoom(CreateFriendRoomReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.createFriendRoom(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.JOIN_ROOM, describe = "加入房间", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void joinRoom(JoinRoomReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.joinRoom(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.LEAVE_ROOM, describe = "请求离开房间", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void leaveRoom(LeaveRoomReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.leaveRoom(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.DISSOLVED_ROOM, describe = "解散房间", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void dissolvedRomd(DissolvedRoomReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.dissolvedRoom(message);
            DissolvedRoomRespMessage.from(message).send();
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.VOTE_TO_DISSOLVED_ROOM, describe = "投票解散房间", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void voteToDissolvedRoom(VoteToDissolvedRoomReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.voteToDissolvedRoom(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.DEAL_VOTE, describe = "处理投票", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void dealVote(DealVoteReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.dealVote(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

    @PacketHandlerMethod(msgId = CommandEnum.Command.READY_GAME, describe = "游戏准备", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void readyGame(ReadyGameReqMessage message) throws Exception {
        try {
            mjFriendRoomManager.readyGame(message);
        }catch (GameException e) {
            ErrorMessage.from(message).setReason(e.getErrorMsg()).send();
        }
    }

}
