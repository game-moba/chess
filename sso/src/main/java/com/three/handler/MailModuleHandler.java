package com.three.handler;

import com.three.common.message.mail.ReadMailReqMessage;
import com.three.common.receiver.ReceiverType;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import com.three.service.MailModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mathua on 2017/6/24.
 */
@Service
@PacketHandlerClass
public class MailModuleHandler {
    @Autowired
    private MailModuleService mailModuleService;

    @PacketHandlerMethod(msgId = CommandEnum.Command.READ_MAIL, describe = "阅读邮件", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void readMailHandler(ReadMailReqMessage message) {
        mailModuleService.readMail(message);
    }
}
