package com.three.handler;

import com.three.common.message.player.ChangePlayerInfoMessage;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.common.receiver.ReceiverType;
import com.three.protocol.CommandEnum;
import com.three.service.PlayerModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mathua on 2017/6/22.
 */
@Service
@PacketHandlerClass
public class PlayerModuleHandler {

    @Autowired
    private PlayerModuleService playerModuleService;

    @PacketHandlerMethod(msgId = CommandEnum.Command.CHANGE_PLAYER_INFO, describe = "修改玩家信息", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER, ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void changePlayerInfoHandler(ChangePlayerInfoMessage message) throws Exception {
        playerModuleService.changePlayerInfo(message);
    }
}
