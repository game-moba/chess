package com.three.handler;

import com.three.api.connection.SessionContext;
import com.three.common.message.shop.ConsumeReqMessage;
import com.three.common.message.shop.ConsumeRespMessage;
import com.three.common.receiver.ReceiverType;
import com.three.domain.shop.ConsumeLog;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import org.springframework.stereotype.Service;

/**
 * Created by wangziqing on 2017/7/16 0016.
 */
@Service
@PacketHandlerClass
public class ShopModuleHandler {
    @PacketHandlerMethod(msgId = CommandEnum.Command.COMSUME_REQ, describe = "道具购买", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER,ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void consumeHandler(ConsumeReqMessage message) throws Exception {
        SessionContext context = message.getConnection().getSessionContext();
        ConsumeLog order=ConsumeLog.build(context.getPlayerId(),message.getChannelId(),message.getItemId());
        order.check();
        order.pay();
        order.receipt();
        ConsumeRespMessage.from(message).setItemId(message.getItemId()).setOrderId(order.getOrderId()).send();
    }
}
