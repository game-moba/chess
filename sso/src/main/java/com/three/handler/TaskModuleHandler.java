package com.three.handler;

import com.three.service.TaskService;
import com.three.common.message.quest.GetRewardReqMessage;
import com.three.common.message.quest.GetRewardRespMessage;
import com.three.common.receiver.ReceiverType;
import com.three.netty.core.handler.annotation.PacketHandlerClass;
import com.three.netty.core.handler.annotation.PacketHandlerMethod;
import com.three.protocol.CommandEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mathua on 2017/6/22.
 */
@Service
@PacketHandlerClass
public class TaskModuleHandler {

    @Autowired
    private TaskService taskService;

    @PacketHandlerMethod(msgId = CommandEnum.Command.GET_REWARD, describe = "領取任務奖励", receivers = {ReceiverType.CONNECTION_SERVER_RECEIVER,ReceiverType.WEB_SOCKET_SERVER_RECEIVER})
    public void getRewardHandler(GetRewardReqMessage message) throws Exception {
        taskService.getReward(message.getTaskId());
        //响应
        GetRewardRespMessage.from(message).setTaskId(message.getTaskId()).send();
    }
}
