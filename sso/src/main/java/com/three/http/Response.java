package com.three.http;

/**
 * Created by Mathua on 2017/6/13.
 */
public class Response {
    private String error;
    private String host;
    private int port;

    public static Response build(String error) {
        Response response = new Response();
        response.error = error;
        return response;
    }

    public static Response build(String host, int port) {
        Response response = new Response();
        response.host = host;
        response.port = port;
        return response;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getError() {
        return error;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
