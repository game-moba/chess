package com.three.mahjong.base.battle;

/**
 * 牌局抽象对象
 */
public abstract class AbstractBattle implements Battle{
	/**
	 * 是否已经结束
	 */
	private boolean over;
	
	@Override
	public boolean isOver() {
		return over;
	}

	public void setOver(boolean over) {
		this.over = over;
	}
	
	
}
