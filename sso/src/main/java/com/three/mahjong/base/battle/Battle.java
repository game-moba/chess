package com.three.mahjong.base.battle;


/**
 * 牌局对象
 * 
 *
 *
 */
public interface Battle {
	/**
	 * 获得牌局id
	 * 
	 * @return
	 */
	long getId();

	/**
	 * 是否已经结束
	 * 
	 * @return
	 */
	boolean isOver();

	/**
	 * 初始化
	 */
	void init();

}
