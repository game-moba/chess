package com.three.mahjong.base.comm;

import com.three.mahjong.base.battle.Battle;
import com.three.mahjong.base.model.MJGameType;
import com.three.mahjong.base.model.RoomType;

import java.util.Collection;

/**
 *
 * 通用牌桌对象
 *
 */
public abstract class CardTable implements Battle {

    /**
     * 房间号
     */
    protected long roomId;

    /**
     * 房主id
     */
    protected long roomOwnerId;

    /**
     * 房间类型
     */
    protected RoomType roomType;

    /**
     * 游戏类型
     */
    protected MJGameType gameType;

    /**
     * 局数
     */
    protected int rounds;

    /**
     * 最大局数
     */
    protected int maxRounds;

    /**
     * 房间创建时间
     */
    protected long createTime;


    public abstract Collection<Long> getAllPlayerIds();

    public long getRoomId() {
        return roomId;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public MJGameType getGameType() {
        return gameType;
    }

    public int getRounds() {
        return rounds;
    }

    public int getMaxRounds() {
        return maxRounds;
    }

    public long getCreateTime() {
        return createTime;
    }

    public long getRoomOwnerId() {
        return roomOwnerId;
    }

    @Override
    public long getId() {
        return getRoomId();
    }
}
