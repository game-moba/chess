package com.three.mahjong.base.constants;

/**
 *
 */
public class MJBaseDef {

    /**
     * 玩家人数
     */
    public static final int PLAYER_NUM = 4;

    // 游戏动作
    public enum MJAction {

        MO_PAI(1), // 摸牌
        DA_PAI(2), // 打牌
        PENG_PAI(3), // 碰牌
        GANG_PAI(4), // 杠牌
        CHI_PAI(5);// 吃牌

        private final int value;

        private MJAction(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    // 游戏状态
    public enum MJStatus {

        PREPARE(1), // 准备状态
        RUNNING(2), // 运行状态
        OVERGAME(3),// 结束状态
        PAUSE(4),// 暂停状态
        VOTE_DISSOLVED(5);// 投票解散

        private final int value;

        MJStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

    }


    /**
     * 牌类型定义
     */
    public static final int WAN_PAI_TYPE = 1;//万牌类型

    public static final int TONG_PAI_TYPE = 2;//筒牌类型

    public static final int TIAO_PAI_TYPE = 3;//条牌类型

    public static final int FENG_PAI_TYPE = 4;//风牌类型

    public static final int FLOWER_PAI_TYPE = 5;//花牌类型



    /**
     * 花牌数量
     */
    public static final int FLOWER_PAI_NUM = 7;//花胡条件：花牌的数量

    public static final int MING_LOU_CONDITION = 16;//明楼的条件：


}
