package com.three.mahjong.base.interfaces;

import com.three.mahjong.base.model.Card;
import com.three.mahjong.base.model.CardBigType;

import java.util.List;

/**
 * Created by YW0981 on 2017/6/13.
 */
public interface MJRule {
    /**
     * 碰
     * @param cards 玩家手牌
     * @param card  其他玩家出的牌
     * @return
     */
     boolean Peng(List<Card> cards, Card card);

    /**
     * 杠
     * @param cards
     * @param card
     * @return
     */
     boolean Gang(List<Card> cards, Card card);

    /**
     * 胡
     * @param cards
     * @param card
     * @return
     */
     boolean Hu(List<Card> cards, Card card);

    /**
     * 得到大类型（万，筒，条）
     * @param id
     * @return
     */
     CardBigType getCardBigType(int id);

    /**
     * 得到小类型
     * @param id
     * @return
     */
     Integer getSmallType(int id);
}
