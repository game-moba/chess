package com.three.mahjong.base.model;

/**
 * Created by YW0981 on 2017/6/13.
 */

/**
 * 牌
 */
public class Card {

    /**
     * 牌的id(例子，123：1表示大类型@CardBigType,2表示小类型@CardSmallType,3表示第几张，此牌表示第一张东风)
     */
    private final int id;

    private Card(int id) {
        this.id = id;
    }

    /**
     * 大类型(万、筒、条等)
     */
    private CardBigType cardBigType;

    /**
     * 小类型(万：一万、两万等)
     */
    private CardSmallType cardSmallType;

    public static Card build(int id) {
        Card card = new Card(id);
        int bigType = id/100;
        int smallTypeId = id % 100 / 10;
        card.cardSmallType = CardSmallType.get(CardBigType.get(bigType), smallTypeId);
        card.cardBigType = card.cardSmallType.getCardBigType();
        return card;
    }

    public static Card build(CardSmallType cardSmallType) {
        int id = cardSmallType.getCardBigType().id * 100 + cardSmallType.getId() *10+1;
        return build(id);
    }


    public int getId() {
        return id;
    }

    public CardBigType getCardBigType() {
        return cardBigType;
    }

    public CardSmallType getCardSmallType() {
        return cardSmallType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;
        return cardSmallType == card.cardSmallType;
    }

}
