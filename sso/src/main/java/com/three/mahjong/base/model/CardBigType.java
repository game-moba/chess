package com.three.mahjong.base.model;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by YW0981 on 2017/6/13.
 */
public enum  CardBigType {
    // 字牌(合计28张)
    FENG_PAI(1),// 风牌：东、西、南、北，各4张
//    JIAN_PAI(2),// 箭牌：中、发、白，各4张

    // 序数牌(合计108张)
    WAN_ZI_PAI(3),// 从一万到九万，各4张，共36张
    TONG_ZI_PAI(4),// 从一筒至九筒，各4张，共36张。也有的地方称为饼，从一饼到九饼。
    SUO_ZI_PAI(5),// 从一束至九束，各4张，共36张。也有的地方称为条，从一条到九条。

    // 花牌
    HUA_PAI(6),// 梅、兰、竹、菊、春、夏、秋、冬各1张，共8张。

    ;
    public final int id;
    CardBigType(int id) {
        this.id = id;
    }

    private static Map<Integer, CardBigType> VALUES= Arrays.stream(CardBigType.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public static CardBigType get(int id){
        return VALUES.get(id);
    }
}
