package com.three.mahjong.base.model;

import java.util.HashMap;
import java.util.Map;

/**
 * 小类型
 * Created by mathua on 2017/7/7.
 */
public enum CardSmallType {
    // 风牌
    DONG_FENG(CardBigType.FENG_PAI, 1),// 东
    NAN_FENG(CardBigType.FENG_PAI, 2),// 南
    XI_FENG(CardBigType.FENG_PAI, 3),// 西
    BEI_FENG(CardBigType.FENG_PAI, 4),// 北
    ZHONG(CardBigType.FENG_PAI, 5),// 中
    FA(CardBigType.FENG_PAI, 6),// 发
    BAI(CardBigType.FENG_PAI, 7),// 白

    // 万子牌
    YI_WAN(CardBigType.WAN_ZI_PAI, 1),// 一万
    ER_WAN(CardBigType.WAN_ZI_PAI, 2),// 二万
    SAN_WAN(CardBigType.WAN_ZI_PAI, 3),// 三万
    SI_WAN(CardBigType.WAN_ZI_PAI, 4),// 四万
    WU_WAN(CardBigType.WAN_ZI_PAI, 5),// 五万
    LIU_WAN(CardBigType.WAN_ZI_PAI, 6),// 六万
    QI_WAN(CardBigType.WAN_ZI_PAI, 7),// 七万
    BA_WAN(CardBigType.WAN_ZI_PAI, 8),// 八万
    JIU_WAN(CardBigType.WAN_ZI_PAI, 9),// 九万

    // 筒子牌
    YI_TONG(CardBigType.TONG_ZI_PAI, 1),// 一筒
    ER_TONG(CardBigType.TONG_ZI_PAI, 2),// 二筒
    SAN_TONG(CardBigType.TONG_ZI_PAI, 3),// 三筒
    SI_TONG(CardBigType.TONG_ZI_PAI, 4),// 四筒
    WU_TONG(CardBigType.TONG_ZI_PAI, 5),// 五筒
    LIU_TONG(CardBigType.TONG_ZI_PAI, 6),// 六筒
    QI_TONG(CardBigType.TONG_ZI_PAI, 7),// 七筒
    BA_TONG(CardBigType.TONG_ZI_PAI, 8),// 八筒
    JIU_TONG(CardBigType.TONG_ZI_PAI, 9),// 九筒

    // 索子牌
    YI_SUO(CardBigType.SUO_ZI_PAI, 1),// 一索
    ER_SUO(CardBigType.SUO_ZI_PAI, 2),// 二索
    SAN_SUO(CardBigType.SUO_ZI_PAI, 3),// 三索
    SI_SUO(CardBigType.SUO_ZI_PAI, 4),// 四索
    WU_SUO(CardBigType.SUO_ZI_PAI, 5),// 五索
    LIU_SUO(CardBigType.SUO_ZI_PAI, 6),// 六索
    QI_SUO(CardBigType.SUO_ZI_PAI, 7),// 七索
    BA_SUO(CardBigType.SUO_ZI_PAI, 8),// 八索
    JIU_SUO(CardBigType.SUO_ZI_PAI, 9),// 九索

    // 花牌
    MEI(CardBigType.HUA_PAI, 1),// 梅
    LAN(CardBigType.HUA_PAI, 2),// 兰
    ZHU(CardBigType.HUA_PAI, 3),// 竹
    JU(CardBigType.HUA_PAI, 4),// 菊
    CHUN(CardBigType.HUA_PAI, 5),// 春
    XIA(CardBigType.HUA_PAI, 6),// 夏
    QIU(CardBigType.HUA_PAI, 7),// 秋
    DONG(CardBigType.HUA_PAI, 8),// 冬

    ;

    private final CardBigType cardBigType;
    private final int id;

    private static Map<CardBigType, Map<Integer, CardSmallType>> VALUES = new HashMap<>();
    static {
        for(CardBigType bigType : CardBigType.values()) {
            VALUES.putIfAbsent(bigType, new HashMap<>());
        }
        for(CardSmallType smallType : CardSmallType.values()) {
            Map<Integer, CardSmallType> tmpValues = VALUES.get(smallType.cardBigType);
            tmpValues.putIfAbsent(smallType.id, smallType);
        }
    }

    CardSmallType(CardBigType cardBigType, int id) {
        this.cardBigType = cardBigType;
        this.id = id;
    }

    public static CardSmallType get(CardBigType cardBigType, int id){
        if(!VALUES.containsKey(cardBigType))
            return null;
        return VALUES.get(cardBigType).get(id);
    }

    public CardBigType getCardBigType() {
        return cardBigType;
    }

    public int getId() {
        return id;
    }
    public int getCode() {
        return cardBigType.id * 10 + id;
    }

}
