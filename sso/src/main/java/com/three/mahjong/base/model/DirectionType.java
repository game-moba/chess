package com.three.mahjong.base.model;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by mathua on 2017/7/10.
 */
public enum DirectionType {
    EAST(1),// 东
    SOUTH(2),// 南
    WEST(3),// 西
    NORTH(4),// 北
    ;

    private final int id;
    DirectionType(int id) {
        this.id = id;
    }
    private static Map<Integer, DirectionType> VALUES= Arrays.stream(DirectionType.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public static DirectionType get(int id){
        return VALUES.get(id);
    }
    public DirectionType getNextDir() {
        int tmpId = id + 1;
        if(tmpId > VALUES.size())
            tmpId = 1;
        return VALUES.get(tmpId);
    }
}
