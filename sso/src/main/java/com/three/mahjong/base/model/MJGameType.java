package com.three.mahjong.base.model;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 麻将游戏类型
 * Created by mathua on 2017/7/19.
 */
public enum MJGameType {
    GUANG_DONG_MA_JIANG(0),// 广东麻将
    TYPE_1(1)// 待扩展麻将类型
    ;

    private static Map<Integer, MJGameType> VALUES= Arrays.stream(MJGameType.values()).collect(Collectors.toMap(t->t.id, Function.identity()));
    MJGameType(int id){
        this.id=id;
    }
    public int id;
    public static boolean verfy(int id){
        return VALUES.containsKey(id);
    }
    public static MJGameType get(int id){
        return VALUES.get(id);
    }
}
