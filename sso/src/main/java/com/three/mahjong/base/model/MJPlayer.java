package com.three.mahjong.base.model;

import com.three.exception.GameException;
import com.three.protocol.HallModule;
import com.three.protocol.MJModule.OperateInfo;
import com.three.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 麻将玩家对象父类
 */
public class MJPlayer {

    /**
     * 玩家id
     */
    private long playerId;

    /**
     * 玩家昵称
     */
    private String name;

    /**
     * 头像
     */
    private String head;

    /**
     * 性别
     */
    private int gender;

    /**
     * ip
     */
    private String ip;

    /**
     * 玩家当前状态
     */
    private HallModule.PlayerState playerState;
    
    /**
     * 玩家座位
     */
    private int seat;
    
    /**
     * 玩家是否自摸
     */
    private boolean isZiMo;
    
    /**
     * 玩家是否挂机
     */
    private boolean auto;
    
    /**
     * 房间id
     */
    private long roomId;
    
    /**
     * 玩家手牌
     */
    private List<Integer> handPaiList = new ArrayList<>(50);
    
    /**
     * 玩家桌面牌集合（此处指的是玩家碰杠吃操作的牌，不是指弃牌堆）
     */
    private List<ShowPai> showPais = new ArrayList<>(50);
    
    /**
     * 玩家弃牌堆
     */
    private List<Integer> outPaiList = new ArrayList<>(50);
    
    /**
     * 玩家可操作列表
     */
    private List<OperateInfo> canAction = new ArrayList<>();

    public static MJPlayer build(long roomId, long playerId, String name, String head, int gender, String ip) {
        MJPlayer mjPlayer = new MJPlayer();
        mjPlayer.playerId = playerId;
        mjPlayer.name = name;
        mjPlayer.head = head;
        mjPlayer.gender = gender;
        mjPlayer.ip = ip;
        mjPlayer.playerState = HallModule.PlayerState.NOT_READY;
        mjPlayer.auto = false;
        mjPlayer.roomId = roomId;
        return mjPlayer;
    }

    public void readGame() {
        if(playerState != HallModule.PlayerState.NOT_READY)
            throw new GameException("Can Not To Ready Game!");
        playerState = HallModule.PlayerState.READY;
    }

    public void toPlaying() {
        if(playerState != HallModule.PlayerState.READY)
            throw new GameException("Can Not To Playing Game!");
        playerState = HallModule.PlayerState.PLAYING;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public boolean isZiMo() {
        return isZiMo;
    }

    public void setZiMo(boolean isZiMo) {
        this.isZiMo = isZiMo;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public List<Integer> getHandPaiList() {
        return handPaiList;
    }

    public void setHandPaiList(List<Integer> handPaiList) {
        this.handPaiList = handPaiList;
    }

    public List<ShowPai> getShowPais() {
        return showPais;
    }

    public void setShowPais(List<ShowPai> showPais) {
        this.showPais = showPais;
    }

    public List<Integer> getOutPaiList() {
        return outPaiList;
    }

    public void setOutPaiList(List<Integer> outPaiList) {
        this.outPaiList = outPaiList;
    }

    public List<OperateInfo> getCanAction() {
        return canAction;
    }

    public void setCanAction(List<OperateInfo> canAction) {
        this.canAction = canAction;
    }

    public String getName() {
        return name;
    }

    public String getHead() {
        return head;
    }

    public int getGender() {
        return gender;
    }

    public String getIp() {
        return ip;
    }

    public HallModule.PlayerState getPlayerState() {
        return playerState;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPlayerState(HallModule.PlayerState playerState) {
        this.playerState = playerState;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
