package com.three.mahjong.base.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathua on 2017/7/20.
 */
public enum PayType {
    ROOM_OWNER_TREAT(1),// 房主请客
    MEMBERS_PAY(2),// 各自买单
    ;
    private static Map<Integer, PayType> map = new HashMap<>();
    static {
        for(PayType type : values()) {
            map.putIfAbsent(type.id, type);
        }
    }

    public final int id;
    PayType(int id) {
        this.id = id;
    }

    public static PayType valueOf(int id) {
        return map.get(id);
    }
}