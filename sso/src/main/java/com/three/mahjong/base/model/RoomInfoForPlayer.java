package com.three.mahjong.base.model;

/**
 * Created by mathua on 2017/7/22.
 */
public class RoomInfoForPlayer {
    private MJGameType gameType;
    private RoomType roomType;
    private long roomId;

    public static RoomInfoForPlayer build(MJGameType gameType, RoomType roomType, long roomId) {
        RoomInfoForPlayer roomInfoForPlayer = new RoomInfoForPlayer();
        roomInfoForPlayer.gameType = gameType;
        roomInfoForPlayer.roomType = roomType;
        roomInfoForPlayer.roomId = roomId;
        return roomInfoForPlayer;
    }

    public MJGameType getGameType() {
        return gameType;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public long getRoomId() {
        return roomId;
    }
}
