package com.three.mahjong.base.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathua on 2017/7/19.
 */
public enum RoomType {
    FRIEND_ROOM(1),//好友房
    MACTH_ROOM(2),//匹配房
    ;

    public final int id;
    public final static Map<Integer, RoomType> map = new HashMap<>();
    static {
        for(RoomType type : values()) {
            map.putIfAbsent(type.id, type);
        }
    }

    RoomType(int id) {
        this.id = id;
    }

    public static RoomType valueOf(int id) {
        return map.get(id);
    }

}
