package com.three.mahjong.base.model;

import com.three.config.MJCreateRoomRuleConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mathua on 2017/7/20.
 */
public enum RuleType {
    PAY_TYPE(1) {// 支付类型
        @Override
        public boolean checkConfig(MJCreateRoomRuleConfig config) {
            List<String> params = config.getRuleParams();
            if(params.size() != 1) {
                LOGGER.error("RuleType={}, Config={}, Error Params Size={}.", this, config, params.size());
                return false;
            }
            for(String param : params) {
                try {
                    PayType payType = PayType.valueOf(Integer.valueOf(param));
                    if(payType == null) {
                        LOGGER.error("RuleType={}, Config={}, param={}.", this, config, params);
                        return false;
                    }
                }catch(Exception e) {
                    LOGGER.error("RuleType={}, Config={}, param={}.", this, config, params);
                    return false;
                }
            }
            return true;
        }
    },
    CEILING_SCORE(2) {// 封顶
        @Override
        public boolean checkConfig(MJCreateRoomRuleConfig config) {
            List<String> params = config.getRuleParams();
            if(params.size() != 1) {
                LOGGER.error("RuleType={}, Config={}, Error Params Size={}.", this, config, params.size());
                return false;
            }
            for(String param : params) {
                try {
                    Integer.valueOf(param);
                }catch(Exception e) {
                    LOGGER.error("RuleType={}, Config={}, param={}.", this, config, params);
                    return false;
                }
            }
            return true;
        }
    },
    INNING(3) {// 局数
        @Override
        public boolean checkConfig(MJCreateRoomRuleConfig config) {
            List<String> params = config.getRuleParams();
            if(params.size() != 1) {
                LOGGER.error("RuleType={}, Config={}, Error Params Size={}.", this, config, params.size());
                return false;
            }
            for(String param : params) {
                try {
                    Integer.valueOf(param);
                }catch(Exception e) {
                    LOGGER.error("RuleType={}, Config={}, param={}.", this, config, params);
                    return false;
                }
            }
            return true;
        }
    },
    BASE_SCORE(4) {// 底分
        @Override
        public boolean checkConfig(MJCreateRoomRuleConfig config) {
            List<String> params = config.getRuleParams();
            if(params.size() != 1) {
                LOGGER.error("RuleType={}, Config={}, Error Params Size={}.", this, config, params.size());
                return false;
            }
            for(String param : params) {
                try {
                    Integer.valueOf(param);
                }catch(Exception e) {
                    LOGGER.error("RuleType={}, Config={}, param={}.", this, config, params);
                    return false;
                }
            }
            return true;
        }
    }
    ;
    private static Map<Integer, RuleType> map = new HashMap<>();
    static {
        for(RuleType type : values()) {
            map.putIfAbsent(type.id, type);
        }
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RuleType.class);

    public abstract boolean checkConfig(MJCreateRoomRuleConfig config);

    public final int id;
    RuleType(int id) {
        this.id = id;
    }

    public static RuleType valueOf(int id) {
        return map.get(id);
    }

}
