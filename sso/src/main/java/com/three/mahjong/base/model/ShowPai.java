package com.three.mahjong.base.model;

import com.three.protocol.MJModule.OptionsType;
import java.util.Vector;

/**
 * 
 * 玩家桌面牌对象(已经显示出来的碰杠吃牌集合)
 */
public class ShowPai {

    /**
     * 牌操作类型
     */
    private OptionsType type;
    
    /**
     * 玩家座位
     */
    private int seat;
    
    /**
     * 目标牌
     */
    private int targetPai;
    
    
    /**
     * 玩家操作牌集合
     */
    private Vector<Integer> paiVector = new Vector<>(50);
    

    public OptionsType getType() {
        return type;
    }

    public void setType(OptionsType type) {
        this.type = type;
    }

    public Vector<Integer> getPaiVector() {
        return paiVector;
    }

    public void setPaiVector(Vector<Integer> paiVector) {
        this.paiVector = paiVector;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getTargetPai() {
        return targetPai;
    }

    public void setTargetPai(int targetPai) {
        this.targetPai = targetPai;
    }

}
