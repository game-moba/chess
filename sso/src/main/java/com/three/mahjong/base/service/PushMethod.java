package com.three.mahjong.base.service;

import com.three.mahjong.base.model.MJPlayer;
import com.three.mahjong.base.model.MJTable;
import com.three.function.PromptFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.three.protocol.CommandEnum.*;
import static com.three.protocol.MJModule.*;

/**
 * Created by pzt on 2017/7/18.
 */
public class PushMethod {

    @Autowired
    private PromptFunction promptFunction;

    /**
     * 发送可操作的信息给客户端
     * @param mjPlayer 玩家对象
     * @param operateInfos 操作信息
     */
    public void pushOperatePaiInfo(MJPlayer mjPlayer, List<OperateInfo> operateInfos){
        CanOperateResp.Builder canOperate = CanOperateResp.newBuilder();
        canOperate.setPlayerId(mjPlayer.getPlayerId());
        canOperate.setSeat(mjPlayer.getSeat());
        canOperate.addAllOperateInfo(operateInfos);
        promptFunction.sendMessage(Command.CAN_OPERATE, canOperate.build(), mjPlayer.getPlayerId());
    }

    /**
     * 推送玩家操作结果信息
     * @param operateResult
     * @param mahjongPlayer
     * @param mahjongTable
     */
    public void pushOperateResultResp(OperateResult operateResult,MJPlayer mjPlayer,MJTable mjTable){
        OperateResultResp.Builder operateResultResp = OperateResultResp.newBuilder();
        operateResultResp.setOperateResult(operateResult);
        operateResultResp.setPlayerId(mjPlayer.getPlayerId());
        operateResultResp.setSeat(mjPlayer.getSeat());
        promptFunction.sendMessage(Command.OPERATE_RESULT, operateResultResp.build(), mjTable.getAllPlayerIds());
    }

    /**
     * 推送摸牌信息
     * @param targetPai 目标牌
     * @param mjPlayer
     * @param mjTable
     */
    public void pushMoPaiInfo(Integer targetPai, MJPlayer mjPlayer, MJTable mjTable) {
        System.out.println("推送摸牌信息");
        MopaiResp.Builder mopaiResp = MopaiResp.newBuilder();
        mopaiResp.setBalancePaiNum(mjTable.getTablePais().size());
        mopaiResp.setPlayerId(mjPlayer.getPlayerId());
        mopaiResp.setSeat(mjPlayer.getSeat());

        List<Long> playerIdList = new ArrayList<>();
        for (long playerId : mjTable.getPlayers().keySet()) {
            if (playerId == mjPlayer.getPlayerId()) {
                mopaiResp.setPai(targetPai);
                mopaiResp.addAllShoupai(mjPlayer.getHandPaiList());
            }
            playerIdList.add(playerId);
        }
        promptFunction.sendMessage(Command.MO_PAI, mopaiResp.build(), playerIdList);
    }


    /**
     * 推送开局信息
     *
     * @param mjTable
     */
    public void pushTableOpenInfo(MJTable mjTable) {
        System.out.println("推送玩家手牌信息");
        for (long playerId : mjTable.getPlayers().keySet()) {
            FapaiResp.Builder fapaiResp = FapaiResp.newBuilder();
            fapaiResp.setBalancePaiNum(mjTable.getTablePais().size());
            fapaiResp.setDicePoint1(mjTable.getDiceOneNum());
            fapaiResp.setDicePoint2(mjTable.getDiceTwoNum());
            fapaiResp.setHostSeat(mjTable.getBankerSeat());
            fapaiResp.setPlayerId(playerId);
            MJPlayer mjPlayer = mjTable.getPlayers().get(playerId);
            fapaiResp.setSeat(mjPlayer.getSeat());
            fapaiResp.addAllShoupai(mjPlayer.getHandPaiList());
            fapaiResp.setHostSeat(mjTable.getBankerSeat());
            promptFunction.sendMessage(Command.FA_PAI, fapaiResp.build(), playerId);
        }
    }

    /**
     * 主动推送可进行的操作
     * @param mjPlayer
     * @param optionsType
     */
    public void pushCanOperateValue(MJPlayer mjPlayer,OptionsType optionsType){
        CanOperateResp.Builder canOperateResp = CanOperateResp.newBuilder();
        canOperateResp.setPlayerId(mjPlayer.getPlayerId());
        canOperateResp.setSeat(mjPlayer.getSeat());
        OperateInfo.Builder operateInfo = OperateInfo.newBuilder();
        operateInfo.setType(optionsType);
        canOperateResp.addOperateInfo(operateInfo);
        promptFunction.sendMessage(Command.CAN_OPERATE, canOperateResp.build(), mjPlayer.getPlayerId());
    }
}
