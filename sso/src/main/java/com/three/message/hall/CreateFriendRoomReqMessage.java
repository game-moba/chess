package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.mahjong.base.model.MJGameType;
import com.three.mahjong.base.model.PayType;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mathua on 2017/6/25.
 */
public final class CreateFriendRoomReqMessage extends BaseMessage {
    public int gameType;// 游戏类型
    public int payType;// 支付类型
    private int maxRounds;// 局数
    private int baseScore;// 底分
    private int ceilingScore;// 封顶分数
    private List<Integer> playRules = new ArrayList<>();// 玩法

    public CreateFriendRoomReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static CreateFriendRoomReqMessage from(Connection conn) {
        return new CreateFriendRoomReqMessage(new Packet(CommandEnum.Command.CREATE_FRIEND_ROOM), conn);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.CreateFriendRoomReq_1000 bodyData = HallModule.CreateFriendRoomReq_1000.parseFrom(body);
        gameType = bodyData.getGameType();
        payType = bodyData.getPayType();
        maxRounds = bodyData.getMaxRounds();
        baseScore = bodyData.getBaseScore();
        ceilingScore = bodyData.getCeilingScore();
        for(int playRule : bodyData.getPlayRulesList()) {
            playRules.add(playRule);
        }
    }

    @Override
    public byte[] encode() {
        HallModule.CreateFriendRoomReq_1000.Builder builder = HallModule.CreateFriendRoomReq_1000.newBuilder();
        builder.setGameType(gameType);
        builder.setPayType(payType);
        builder.setMaxRounds(maxRounds);
        builder.setBaseScore(baseScore);
        builder.setCeilingScore(ceilingScore);
        for(int playRule : playRules) {
            builder.addPlayRules(playRule);
        }
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public MJGameType getGameType() {
        return MJGameType.get(gameType);
    }

    public PayType getPayType() {
        return PayType.valueOf(payType);
    }

    public int getMaxRounds() {
        return maxRounds;
    }

    public int getBaseScore() {
        return baseScore;
    }

    public int getCeilingScore() {
        return ceilingScore;
    }

    public List<Integer> getPlayRules() {
        return playRules;
    }

    public CreateFriendRoomReqMessage setGameType(int gameType) {
        this.gameType = gameType;
        return this;
    }

    public CreateFriendRoomReqMessage setPayType(int payType) {
        this.payType = payType;
        return this;
    }

    public CreateFriendRoomReqMessage setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
        return this;
    }

    public CreateFriendRoomReqMessage setBaseScore(int baseScore) {
        this.baseScore = baseScore;
        return this;
    }

    public CreateFriendRoomReqMessage setCeilingScore(int ceilingScore) {
        this.ceilingScore = ceilingScore;
        return this;
    }

    public CreateFriendRoomReqMessage setPlayRules(List<Integer> playRules) {
        this.playRules = playRules;
        return this;
    }
}
