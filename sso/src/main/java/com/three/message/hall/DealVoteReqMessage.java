package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class DealVoteReqMessage extends BaseMessage {
    private boolean isAgree;

    public DealVoteReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.DealVoteReq_1010 bodyData = HallModule.DealVoteReq_1010.parseFrom(body);
        isAgree = bodyData.getIsAgree();
    }

    @Override
    public byte[] encode() {
        HallModule.DealVoteReq_1010.Builder builder = HallModule.DealVoteReq_1010.newBuilder();
        builder.setIsAgree(isAgree);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public DealVoteReqMessage setAgree(boolean agree) {
        isAgree = agree;
        return this;
    }

    public boolean isAgree() {
        return isAgree;
    }
}
