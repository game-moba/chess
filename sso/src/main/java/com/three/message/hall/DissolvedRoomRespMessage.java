package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

import static com.three.protocol.CommandEnum.Command.DISSOLVED_ROOM;

/**
 * Created by mathua on 2017/7/2.
 */
public final class DissolvedRoomRespMessage extends BaseMessage {
    public DissolvedRoomRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static DissolvedRoomRespMessage from(BaseMessage src) {
        return new DissolvedRoomRespMessage(src.getPacket().response(DISSOLVED_ROOM), src.getConnection());
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {

    }

    @Override
    public byte[] encode() {
        HallModule.DissolvedRoomReq_1007.Builder builder = HallModule.DissolvedRoomReq_1007.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
