package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.mahjong.base.model.MJGameType;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.CommandEnum;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/1.
 */
public final class JoinRoomReqMessage extends BaseMessage {

    private int roomId;// 房间id
    public int gameType;// 游戏类型

    public JoinRoomReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static JoinRoomReqMessage from(Connection conn) {
        return new JoinRoomReqMessage(new Packet(CommandEnum.Command.JOIN_ROOM), conn);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.JoinRoomReq_1002 bodyData = HallModule.JoinRoomReq_1002.parseFrom(body);
        roomId = bodyData.getRoomId();
        gameType = bodyData.getGameType();
    }

    @Override
    public byte[] encode() {
        HallModule.JoinRoomReq_1002.Builder builder = HallModule.JoinRoomReq_1002.newBuilder();
        builder.setRoomId(roomId);
        builder.setGameType(gameType);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public JoinRoomReqMessage setRoomId(int roomId) {
        this.roomId = roomId;
        return this;
    }

    public MJGameType getGameType() {
        return MJGameType.get(gameType);
    }

    public int getRoomId() {
        return roomId;
    }

    public JoinRoomReqMessage setGameType(int gameType) {
        this.gameType = gameType;
        return this;
    }
}
