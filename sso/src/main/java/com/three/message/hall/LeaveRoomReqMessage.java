package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class LeaveRoomReqMessage extends BaseMessage{
    public LeaveRoomReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.LeaveRoomReq_1005 bodyData = HallModule.LeaveRoomReq_1005.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        HallModule.LeaveRoomReq_1005.Builder builder = HallModule.LeaveRoomReq_1005.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
