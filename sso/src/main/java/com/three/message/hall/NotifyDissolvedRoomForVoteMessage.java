package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class NotifyDissolvedRoomForVoteMessage extends BaseMessage {

    private int sponsorSeatId;
    private long voteEndTime;

    public NotifyDissolvedRoomForVoteMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.NotifyDissolvedRoomForVote_1009 bodyData = HallModule.NotifyDissolvedRoomForVote_1009.parseFrom(body);
        sponsorSeatId = bodyData.getSponsorSeatId();
        voteEndTime = bodyData.getVoteEndTime();
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyDissolvedRoomForVote_1009.Builder builder = HallModule.NotifyDissolvedRoomForVote_1009.newBuilder();
        builder.setSponsorSeatId(sponsorSeatId);
        builder.setVoteEndTime(voteEndTime);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public NotifyDissolvedRoomForVoteMessage setSponsorSeatId(int sponsorSeatId) {
        this.sponsorSeatId = sponsorSeatId;
        return this;
    }

    public NotifyDissolvedRoomForVoteMessage setVoteEndTime(long voteEndTime) {
        this.voteEndTime = voteEndTime;
        return this;
    }
}
