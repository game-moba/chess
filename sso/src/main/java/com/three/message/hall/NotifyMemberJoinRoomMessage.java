package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.mahjong.base.model.MJPlayer;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/1.
 */
public final class NotifyMemberJoinRoomMessage extends BaseMessage {

    private MJPlayer mjPlayer;

    public NotifyMemberJoinRoomMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        mjPlayer = new MJPlayer();
        HallModule.NotifyMemberJoinRoom_1004 bodyData = HallModule.NotifyMemberJoinRoom_1004.parseFrom(body);
        mjPlayer.setPlayerId(bodyData.getMember().getMemberId());
        mjPlayer.setName(bodyData.getMember().getMemberName());
        mjPlayer.setSeat(bodyData.getMember().getSeatId());
        mjPlayer.setHead(bodyData.getMember().getHead());
        mjPlayer.setGender(bodyData.getMember().getGender());
        mjPlayer.setIp(bodyData.getMember().getIp());
        mjPlayer.setPlayerState(bodyData.getMember().getPlayerState());
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyMemberJoinRoom_1004.Builder builder = HallModule.NotifyMemberJoinRoom_1004.newBuilder();
        HallModule.RoomMemberInfo.Builder roomMemberInfo = HallModule.RoomMemberInfo.newBuilder();
        roomMemberInfo.setMemberId(mjPlayer.getPlayerId());
        roomMemberInfo.setMemberName(mjPlayer.getName());
        roomMemberInfo.setSeatId(mjPlayer.getSeat());
        roomMemberInfo.setHead(mjPlayer.getHead());
        roomMemberInfo.setGender(mjPlayer.getGender());
        roomMemberInfo.setIp(mjPlayer.getIp());
        roomMemberInfo.setPlayerState(mjPlayer.getPlayerState());
        builder.setMember(roomMemberInfo);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public NotifyMemberJoinRoomMessage setJoinRole(MJPlayer mjPlayer) {
        this.mjPlayer = mjPlayer;
        return this;
    }
}
