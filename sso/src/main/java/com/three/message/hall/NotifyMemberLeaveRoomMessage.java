package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class NotifyMemberLeaveRoomMessage extends BaseMessage {

    private int seatId;
    private HallModule.LeaveRoomType leaveRoomType;

    public NotifyMemberLeaveRoomMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.NotifyMemberLeaveRoom_1006 bodyData = HallModule.NotifyMemberLeaveRoom_1006.parseFrom(body);
        seatId = bodyData.getSeatId();
        leaveRoomType = bodyData.getLeaveRoomType();
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyMemberLeaveRoom_1006.Builder builder = HallModule.NotifyMemberLeaveRoom_1006.newBuilder();
        builder.setSeatId(seatId);
        builder.setLeaveRoomType(leaveRoomType);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public NotifyMemberLeaveRoomMessage setSeatId(int seatId) {
        this.seatId = seatId;
        return this;
    }

    public NotifyMemberLeaveRoomMessage setLeaveRoomType(HallModule.LeaveRoomType leaveRoomType) {
        this.leaveRoomType = leaveRoomType;
        return this;
    }

    public int getSeatId() {
        return seatId;
    }

    public HallModule.LeaveRoomType getLeaveRoomType() {
        return leaveRoomType;
    }
}
