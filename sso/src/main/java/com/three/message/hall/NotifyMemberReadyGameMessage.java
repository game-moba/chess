package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class NotifyMemberReadyGameMessage extends BaseMessage {
    private int seatId;

    public NotifyMemberReadyGameMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.NotifyMemberReadyGame_1013 bodyData = HallModule.NotifyMemberReadyGame_1013.parseFrom(body);
        seatId = bodyData.getSeatId();
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyMemberReadyGame_1013.Builder builder = HallModule.NotifyMemberReadyGame_1013.newBuilder();
        builder.setSeatId(seatId);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public NotifyMemberReadyGameMessage setSeatId(int seatId) {
        this.seatId = seatId;
        return this;
    }

    public int getSeatId() {
        return seatId;
    }
}
