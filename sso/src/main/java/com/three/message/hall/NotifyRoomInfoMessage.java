package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.mahjong.base.model.MJPlayer;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.three.protocol.CommandEnum.Command.NOTIFY_ROOM_INFO;

/**
 * Created by Mathua on 2017/6/29.
 */
public final class NotifyRoomInfoMessage extends BaseMessage {
    private int roomId;// 房号
    private int gameType;// 游戏类型
    private int payType;// 支付类型
    private int maxRounds;//局数
    private int currentMemberCount;// 当前人数
    private long createTime;// 创建时间
    private List<Integer> playRules;// 玩法
    private int baseScore;// 底分
    private int ceilingScore;// 封顶分数
    private long roomOwnerId;// 房主id
    private List<MJPlayer> players = new ArrayList<>();// 成员信息

    public NotifyRoomInfoMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static NotifyRoomInfoMessage from(BaseMessage src) {
        return new NotifyRoomInfoMessage(src.getPacket().response(NOTIFY_ROOM_INFO), src.getConnection());
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.NotifyRoomInfo_1001 bodyData = HallModule.NotifyRoomInfo_1001.parseFrom(body);
        roomId = bodyData.getRoomId();
        gameType = bodyData.getGameType();
        payType = bodyData.getPayType();
        maxRounds = bodyData.getMaxRounds();
        currentMemberCount = bodyData.getCurrentMemberCount();
        createTime = bodyData.getCreateTime();
        playRules = new ArrayList<>();
        for(int playRule : bodyData.getPlayRulesList()) {
            playRules.add(playRule);
        }
        baseScore = bodyData.getBaseScore();
        ceilingScore = bodyData.getCeilingScore();
        roomOwnerId = bodyData.getRoomOwnerId();
        for(HallModule.RoomMemberInfo roomMemberInfo : bodyData.getMembersList()) {
            MJPlayer mjPlayer = new MJPlayer();
            mjPlayer.setPlayerId(roomMemberInfo.getMemberId());
            mjPlayer.setName(roomMemberInfo.getMemberName());
            mjPlayer.setSeat(roomMemberInfo.getSeatId());
            mjPlayer.setHead(roomMemberInfo.getHead());
            mjPlayer.setGender(roomMemberInfo.getGender());
            mjPlayer.setIp(roomMemberInfo.getIp());
            mjPlayer.setPlayerState(roomMemberInfo.getPlayerState());
            players.add(mjPlayer);
        }
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyRoomInfo_1001.Builder builder = HallModule.NotifyRoomInfo_1001.newBuilder();
        builder.setRoomId(roomId);
        builder.setGameType(gameType);
        builder.setPayType(payType);
        builder.setMaxRounds(maxRounds);
        builder.setCurrentMemberCount(currentMemberCount);
        builder.setCreateTime(createTime);
        for(int playRule : playRules) {
            builder.addPlayRules(playRule);
        }
        builder.setBaseScore(baseScore);
        builder.setCeilingScore(ceilingScore);
        builder.setRoomOwnerId(roomOwnerId);
        if(players != null && !players.isEmpty()) {
            for(int i = 0; i < players.size(); ++i) {
                MJPlayer mjPlayer = players.get(i);
                HallModule.RoomMemberInfo.Builder memberInfo = HallModule.RoomMemberInfo.newBuilder();
                memberInfo.setMemberId(mjPlayer.getPlayerId());
                memberInfo.setMemberName(mjPlayer.getName());
                memberInfo.setSeatId(mjPlayer.getSeat());
                memberInfo.setHead(mjPlayer.getHead());
                memberInfo.setGender(mjPlayer.getGender());
                memberInfo.setIp(mjPlayer.getIp());
                memberInfo.setPlayerState(mjPlayer.getPlayerState());
                builder.addMembers(memberInfo);
            }
        }
        return builder.build().toByteArray();
    }

    public NotifyRoomInfoMessage setRoomId(int roomId) {
        this.roomId = roomId;
        return this;
    }

    public NotifyRoomInfoMessage setGameType(int gameType) {
        this.gameType = gameType;
        return this;
    }

    public NotifyRoomInfoMessage setPayType(int payType) {
        this.payType = payType;
        return this;
    }

    public NotifyRoomInfoMessage setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
        return this;
    }

    public NotifyRoomInfoMessage addPlayers(Collection<MJPlayer> players) {
        this.players.addAll(players);
        return this;
    }

    public NotifyRoomInfoMessage setCurrentMemberCount(int currentMemberCount) {
        this.currentMemberCount = currentMemberCount;
        return this;
    }

    public NotifyRoomInfoMessage setCreateTime(long createTime) {
        this.createTime = createTime;
        return this;
    }

    public NotifyRoomInfoMessage setPlayRules(List<Integer> playRules) {
        this.playRules = playRules;
        return this;
    }

    public NotifyRoomInfoMessage setBaseScore(int baseScore) {
        this.baseScore = baseScore;
        return this;
    }

    public NotifyRoomInfoMessage setCeilingScore(int ceilingScore) {
        this.ceilingScore = ceilingScore;
        return this;
    }

    public NotifyRoomInfoMessage setRoomOwnerId(long roomOwnerId) {
        this.roomOwnerId = roomOwnerId;
        return this;
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
