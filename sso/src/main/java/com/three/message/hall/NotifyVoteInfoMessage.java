package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class NotifyVoteInfoMessage extends BaseMessage {

    private int seatId;
    private boolean isAgree;

    public NotifyVoteInfoMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        HallModule.NotifyVoteInfo_1011 bodyData = HallModule.NotifyVoteInfo_1011.parseFrom(body);
        seatId = bodyData.getSeatId();
        isAgree = bodyData.getIsAgree();
    }

    @Override
    public byte[] encode() {
        HallModule.NotifyVoteInfo_1011.Builder builder = HallModule.NotifyVoteInfo_1011.newBuilder();
        builder.setIsAgree(isAgree);
        builder.setSeatId(seatId);
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }

    public int getSeatId() {
        return seatId;
    }

    public boolean isAgree() {
        return isAgree;
    }

    public NotifyVoteInfoMessage setSeatId(int seatId) {
        this.seatId = seatId;
        return this;
    }

    public NotifyVoteInfoMessage setAgree(boolean agree) {
        isAgree = agree;
        return this;
    }
}
