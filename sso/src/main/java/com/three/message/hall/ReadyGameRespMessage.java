package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

import static com.three.protocol.CommandEnum.Command.READY_GAME;

/**
 * Created by mathua on 2017/7/2.
 */
public final class ReadyGameRespMessage extends BaseMessage {
    public ReadyGameRespMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    public static ReadyGameRespMessage from(BaseMessage src) {
        return new ReadyGameRespMessage(src.getPacket().response(READY_GAME), src.getConnection());
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {

    }

    @Override
    public byte[] encode() {
        HallModule.ReadyGameResp_1012.Builder builder = HallModule.ReadyGameResp_1012.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
