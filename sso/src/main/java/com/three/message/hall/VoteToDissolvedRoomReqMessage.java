package com.three.message.hall;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.HallModule;
import com.three.utils.JsonUtils;

/**
 * Created by mathua on 2017/7/2.
 */
public final class VoteToDissolvedRoomReqMessage extends BaseMessage {
    public VoteToDissolvedRoomReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {

    }

    @Override
    public byte[] encode() {
        HallModule.VoteToDissolvedRoomReq_1008.Builder builder = HallModule.VoteToDissolvedRoomReq_1008.newBuilder();
        return builder.build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
