package com.three.message.item;

import com.google.protobuf.InvalidProtocolBufferException;
import com.three.api.connection.Connection;
import com.three.api.protocol.Packet;
import com.three.common.message.base.BaseMessage;
import com.three.protocol.ItemModule;
import com.three.utils.JsonUtils;

/**
 * Created by Mathua on 2017/7/17.
 */
public class GetPackItemsReqMessage extends BaseMessage {
    public GetPackItemsReqMessage(Packet packet, Connection connection) {
        super(packet, connection);
    }

    @Override
    public void decode(byte[] body) throws InvalidProtocolBufferException {
        ItemModule.GetPackItemsReq_500 bodyData = ItemModule.GetPackItemsReq_500.parseFrom(body);
    }

    @Override
    public byte[] encode() {
        return ItemModule.GetPackItemsReq_500.newBuilder().build().toByteArray();
    }

    @Override
    public String toString() {
        return JsonUtils.toJson(this);
    }
}
