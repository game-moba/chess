package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.account.AccountPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>通过注解标志使用哪个数据库</p>
 * Created by mathua on 2017/6/11.
 */
@ChessDbRepo
@Component
public interface AccountRepository extends JpaRepository<AccountPo,Long> {

    List<AccountPo> findByOpenId(String openId);

}