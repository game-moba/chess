package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.shop.ConsumeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Created by wangziqing on 2017/7/16 0016.
 */
@ChessDbRepo
@Component
public interface ConsumeLogRepository extends JpaRepository<ConsumeLog,Long> {
}
