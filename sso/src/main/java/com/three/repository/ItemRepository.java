package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.item.ItemPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Mathua on 2017/7/17.
 */
@ChessDbRepo
@Component
public interface ItemRepository extends JpaRepository<ItemPo, Long> {
    List<ItemPo> findByOwnerId(long ownerId);
}
