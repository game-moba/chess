package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.mail.MailPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Mathua on 2017/6/23.
 */
@ChessDbRepo
@Component
public interface MailRepository extends JpaRepository<MailPo, Long> {
    List<MailPo> findByReceiverId(long receiverId);
}
