package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.shop.PayLog;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@ChessDbRepo
@Component
public interface PayLogRepository extends JpaRepository<PayLog,Long> {
    @Query("SELECT COUNT(*) FROM PayLog WHERE trade_no=:tradeNo")
    int countByTradeNo(String tradeNo);
}
