package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.player.PlayerPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Created by wangziqing on 2017/5/17 0017.
 */
@ChessDbRepo
@Component
public interface PlayerRepository extends JpaRepository<PlayerPo,Long> {

}
