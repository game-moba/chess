package com.three.repository;

import com.three.db.annotation.ChessDbRepo;
import com.three.domain.quest.TaskPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@ChessDbRepo
@Component
public interface TaskRepository extends JpaRepository<TaskPO, Long> {
    List<TaskPO> findByPlayerId(long playerId);
}
