package com.three.reward;

/**
 * 奖励项信息
 * Created by mathua on 2017/6/23.
 */
public class RewardItem {
    private RewardType type;// 奖励类型
    private long param;// 具体奖励参数
    private long amount;// 奖励数量

    public static RewardItem build(RewardType type, long param, long amount) {
        RewardItem item = new RewardItem();
        item.amount = amount;
        item.param = param;
        item.type = type;
        return item;
    }

    public RewardType getType() {
        return type;
    }

    public long getParam() {
        return param;
    }

    public long getAmount() {
        return amount;
    }
}
