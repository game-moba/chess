package com.three.reward;

import com.three.api.protocol.Packet;
import com.three.api.push.PushSender;
import com.three.config.LanguageConfig;
import com.three.constant.LanguageId;
import com.three.exception.GameException;
import com.three.protocol.BaseModule;
import com.three.protocol.CommandEnum;
import com.three.service.MailModuleService;
import com.three.utils.SpringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Mathua on 2017/6/23.
 */
public class RewardList {
    private List<RewardBase> rewards = new ArrayList<>();
    protected volatile boolean hadHandOut = false;// 是否已经发放
    private final long holderId;

    public RewardList(long holderId) {
        this.holderId = holderId;
    }

    /**
     * 构造一组奖励
     *
     * @param items    奖励项
     * @param holderId 奖励持有者
     * @return
     */
    public static RewardList valueOf(List<RewardItem> items, long holderId) {
        RewardList rewards = new RewardList(holderId);
        for(RewardItem item : items){
            RewardBase rewardBase = RewardBase.valueOf(item, holderId);
            if(rewardBase != null)
                rewards.addReward(rewardBase);
        }
        return rewards;
    }

    /**
     * 添加一个奖励
     * @param rewardBase 奖励信息
     */
    private void addReward(RewardBase rewardBase) {
        for(RewardBase reward : rewards) {
            if(reward.equals(rewardBase)) {
                reward.addAmount(rewardBase.amount);
                return;
            }
        }
        rewards.add(rewardBase);
    }

    /**
     * 发放奖励
     * @param overflowToMail 溢出发邮件
     */
    public void handOutReward(boolean overflowToMail) throws GameException {
        if(hadHandOut) {
            return;
        }
        // 奖励溢出如果不发邮件，就要检查是否会溢出
        if(!overflowToMail) {
            for (RewardBase reward : rewards) {
                try {
                    reward.canHandOut();
                } catch (GameException e) {
                    BaseModule.Error_8.Builder builder = BaseModule.Error_8.newBuilder();
                    if(e.getErrorMsg() != null)
                        builder.setReason(e.getErrorMsg());
                    else {
                        builder.setCode(e.getErrorId());
                        builder.addAllData(Arrays.asList(e.getParams()));
                    }
                    Packet packet = Packet.build(CommandEnum.Command.ERROR, builder.build().toByteArray());
                    PushSender sender = PushSender.get();
                    sender.send(packet, reward.holderId);
                    throw new GameException(LanguageConfig.getText(LanguageId.FAIL_TO_OBTAIN_ITEMS));
                }
            }
        }
        // 发放奖励
        List<RewardItem> leftRewards = new ArrayList<>();
        for(RewardBase reward : rewards) {
            RewardItem item = reward.handOutReward(overflowToMail);
            if(item != null)
                leftRewards.add(item);
        }
        // 剩余的发邮件
        if(overflowToMail && !leftRewards.isEmpty()) {
            SpringUtils.getBean(MailModuleService.class).sendMail(
                    holderId, // holderId
                    LanguageConfig.getText(LanguageId.MAIL_SUBJECT_FOR_NO_ITEMS_SPACE),// subject
                    LanguageConfig.getText(LanguageId.MAIL_CONTENT_FOR_NO_ITEMS_SPACE),// content
                    leftRewards,// items
                    false, // 是否推荐
                    new Date(),// 发送时间
                    null// 删除时间
            );
        }
        hadHandOut = true;
    }
}
