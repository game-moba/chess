package com.three.reward;

import com.three.reward.impl.PlayerAttrReward;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mathua on 2017/6/23.
 */
public enum RewardType {
    PLAYER_ATTR(1, PlayerAttrReward.class),// 玩家属性奖励
    ;

    private final int id;
    private final Class<? extends RewardBase> classOfT;

    private static Map<Integer, RewardType> map = new HashMap<>();
    static {
        for(RewardType type : values()) {
            map.putIfAbsent(type.id, type);
        }
    }

    RewardType(int id, Class<? extends RewardBase> classOfT) {
        this.id = id;
        this.classOfT = classOfT;
    }

    public static boolean verfy(int id){
        return map.containsKey(id);
    }


    public static RewardType valueOf(int id) {
        return map.get(id);
    }

    public int getId() {
        return id;
    }

    public Class<? extends RewardBase> getClassOfT() {
        return classOfT;
    }
}
