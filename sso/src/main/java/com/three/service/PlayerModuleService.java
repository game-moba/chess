package com.three.service;

import com.three.api.connection.SessionContext;
import com.three.common.message.base.ErrorMessage;
import com.three.common.message.base.OkMessage;
import com.three.common.message.player.ChangePlayerInfoMessage;
import com.three.config.LanguageConfig;
import com.three.constant.LanguageId;
import com.three.domain.player.PlayerPo;
import com.three.exception.GameException;
import com.three.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Mathua on 2017/6/22.
 */
@Service
public class PlayerModuleService {

    @Autowired
    private PlayerRepository playerRepository;

    public void changePlayerInfo(ChangePlayerInfoMessage message) {
        SessionContext context = message.getConnection().getSessionContext();
        long playerId = context.getPlayerId();
        PlayerPo player = playerRepository.getOne(playerId);

        try {
            player.changePlayerInfo(playerId,message.getNickName(), message.getHead(), message.getGender(), message.getProvinceID(), message.getCity(), message.getPhone());
            OkMessage.from(message).setMsg(LanguageConfig.getText(LanguageId.CHANGE_PLAYER_INFO_SUCCESS)).send();
        } catch (GameException e) {
            ErrorMessage.from(message).setCode(e.getErrorId()).addData(e.getParams()).send();
        }
    }
}
