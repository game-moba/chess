package com.three.service;

import com.three.config.LanguageConfig;
import com.three.config.ShopConfig;
import com.three.constant.LanguageId;
import com.three.constant.ShopConstants;
import com.three.domain.player.PlayerAttrType;
import com.three.domain.player.PlayerPo;
import com.three.domain.shop.ConsumeLog;
import com.three.domain.shop.PayLog;
import com.three.exception.GameException;
import com.three.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ShopService {
    @Autowired
    private PlayerRepository playerRepository;

    /**
     * 充值——开收据
     *
     * @param order
     */
    public boolean receipt(PayLog order) {
        PlayerPo player = playerRepository.getOne(order.getPlayerId());
        switch (ShopConfig.ShopType.get(order.getShopType())) {
            case GOLD://金币
                player.addAttrValue(PlayerAttrType.GOLD, order.getNumber() + order.getNumber() * order.getGift());
                break;
            case DIAMOND://钻石
                player.addAttrValue(PlayerAttrType.DIAMOND, order.getNumber() + order.getNumber() * order.getGift());
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * 充值——支付
     *
     * @param order
     * @return
     */
    public boolean pay(PayLog order) {
        return order.getStatus() == ShopConstants.ORDER_STATUS_FAILURE &&
                (order.getTradeStatus() == ShopConstants.TRADE_SUCCESS || order.getTradeStatus() == ShopConstants.TRADE_FINISHED);
    }

    /**
     * 消费——开收据
     *
     * @return
     */
    public boolean receipt(ConsumeLog order) {
        PlayerPo player = playerRepository.getOne(order.getPlayerId());

        return true;
    }

    /**
     * 消费——支付
     *
     * @param order
     * @return
     */
    public boolean pay(ConsumeLog order) {
        PlayerPo player = playerRepository.getOne(order.getPlayerId());
        PlayerAttrType attrType=PlayerAttrType.valueOf(order.getPriceUnit());
        // todo  同步
        long own=player.getAttrValue(attrType);
        long need=order.getItemPrice()*order.getNumber();
        if(own<need){
            //余额不足
            throw new GameException(LanguageConfig.getText(LanguageId.NOT_ENOUGH_MONEY));
        }
        return player.subAttrValue(attrType,need);
    }

}
