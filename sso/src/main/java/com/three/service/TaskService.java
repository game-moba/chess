package com.three.service;

import com.three.api.protocol.Packet;
import com.three.api.push.PushSender;
import com.three.config.MissionConfig;
import com.three.config.LanguageConfig;
import com.three.constant.LanguageId;
import com.three.domain.quest.TaskType;
import com.three.domain.quest.TaskPO;
import com.three.exception.GameException;
import com.three.protocol.CommandEnum;
import com.three.protocol.TaskModule;
import com.three.repository.TaskRepository;
import com.three.utils.JsonUtils;
import com.three.utils.LogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    /**
     * 获取任务列表
     * @param playerId
     * @return
     */
    public List<TaskPO> getTasksByPlayer(long playerId){
        List<TaskPO> tasks=taskRepository.findByPlayerId(playerId);
        if(CollectionUtils.isEmpty(tasks)){
            tasks=MissionConfig.getConfigs().values().stream()
                    .map(config->{
                        return TaskPO.build(playerId,config.getId(),0);
                    })
                    .collect(Collectors.toList());
            taskRepository.save(tasks);
        }
        return tasks;
    }

    /**
     * 下发任务列表
     * @param playerId
     * @param tasks
     */
    public void nitifyTasks(long playerId,List<TaskPO> tasks){
        if(!CollectionUtils.isEmpty(tasks)){//下发任务列表
            TaskModule.NotifyTaskList_300 data=TaskModule.NotifyTaskList_300.newBuilder().addAllTasks(
                    tasks.stream()
                            .filter(Objects::nonNull)
                            .map(taskPO -> {
                                return TaskModule.Task.newBuilder().setTaskId(taskPO.getId()).setMissionId(taskPO.getMissionId()).setProgress(taskPO.getProgress()).build();
                            })
                            .collect(Collectors.toList())
            ).build();
            LogUtils.Console.info(JsonUtils.toJson(tasks));
            Packet packet = Packet.build(CommandEnum.Command.NOTIFY_TASK_LIST, data.toByteArray());
            PushSender sender = PushSender.get();
            sender.send(packet, playerId);
        }
    }
    /**
     * 领取奖励
     * @param taskId
     * @throws GameException
     */
    public void getReward(long taskId) throws GameException{
        TaskPO task=taskRepository.findOne(taskId);
        if(Objects.isNull(task)){
            throw new GameException(LanguageConfig.getText(LanguageId.TASK_NOT_EXIST));
        }
        task.reward();
    }

    /**
     * 重置日常任务
     * @param playerId
     */
    public void resetDailyTask(long playerId) {
        List<TaskPO> tasks = taskRepository.findByPlayerId(playerId);
        if (!CollectionUtils.isEmpty(tasks)) {
            tasks = tasks.stream()
                    .filter(task -> {
                        MissionConfig taskConfig=MissionConfig.getConfig(task.getMissionId());
                        return taskConfig.getTaskType() == TaskType.DAILY.id&&
                                taskConfig.getFrontTask()==0;
                    })
                    .peek(task -> task.reset())
                    .collect(Collectors.toList());
            taskRepository.save(tasks);
        }
    }
}
