package com.three.spi;

import com.three.api.spi.Spi;
import com.three.api.spi.common.CacheManager;
import com.three.api.spi.common.CacheManagerFactory;

/**
 * Created by mathua on 2017/5/30.
 */
@Spi(order = 2)
public final class SimpleCacheMangerFactory implements CacheManagerFactory {
    @Override
    public CacheManager get() {
        return FileCacheManger.I;
//        return RedisManager.I;
    }
}
