package com.three.spi;

import com.three.api.spi.Spi;
import com.three.api.spi.common.ServiceRegistryFactory;
import com.three.api.srd.ServiceRegistry;

/**
 * Created by mathua on 2017/5/30.
 */
@Spi(order = 2)
public final class SimpleRegistryFactory implements ServiceRegistryFactory {
    @Override
    public ServiceRegistry get() {
        return FileSrd.I;
    }
}
